package request;

import enums.Crop;
import enums.RequestType;

public class CropDataUpdateRequest {

    private Crop crop;
    private String cropName;
    private Float cropPrice, cropYield, cropAcres;
    private String cropId;
    private RequestType requestType = RequestType.UPDATE;

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Crop getCrop() {
        return crop;
    }

    public void setCrop(Crop crop) {
        this.crop = crop;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public Float getCropPrice() {
        return cropPrice;
    }

    public void setCropPrice(Float cropPrice) {
        this.cropPrice = cropPrice;
    }

    public Float getCropYield() {
        return cropYield;
    }

    public void setCropYield(Float cropYield) {
        this.cropYield = cropYield;
    }

    public Float getCropAcres() {
        return cropAcres;
    }

    public void setCropAcres(Float cropAcres) {
        this.cropAcres = cropAcres;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

}

