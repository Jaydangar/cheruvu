package response;

import pojo.Farmer;

public class UpdateProfileReponse {

    private Farmer farmer;

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }
}