package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.Register;
import response.DistrictResponse;
import response.SubDistrictResponse;
import response.VillageResponse;

public class SubDistrictAdapter extends BaseAdapter {

    private List<SubDistrictResponse> searchList;
    private ArrayList<SubDistrictResponse> subDistrictResponseArrayList;
    private Context context;
    private LayoutInflater inflater;
    private MutableLiveData<SubDistrictResponse> subDistrictResponseMutableLiveData;

    static class ViewHolder {
        TextView eachItem;
    }

    public SubDistrictAdapter(Context context, ArrayList<SubDistrictResponse> arrayAdapter) {
        this.context = context;
        inflater = LayoutInflater.from( context );
        this.subDistrictResponseArrayList = arrayAdapter;
        subDistrictResponseMutableLiveData = Register.getSubDistrictResponse();
        searchList = new ArrayList<>();
        searchList.addAll( subDistrictResponseArrayList );
    }

    @Override
    public int getCount() {
        return subDistrictResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return subDistrictResponseArrayList.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate( R.layout.item_selection, null );
            viewHolder.eachItem = convertView.findViewById( R.id.listItem );
            convertView.setTag( viewHolder );
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //  setting text on items...
        viewHolder.eachItem.setText( subDistrictResponseArrayList.get( position ).getMandalName() );

        //  setting onclicklistener...
        View finalConvertView = convertView;
        convertView.setOnClickListener( v -> {
            SubDistrictResponse subDistrictResponse = subDistrictResponseArrayList.get( position );
            subDistrictResponseMutableLiveData.postValue( subDistrictResponse );
            //  when we update State we need to make sure user updates District, SubDistrict, Village as well...
            updateV( finalConvertView );
            ((Activity) context).finish();
        } );

        return convertView;
    }

    //  we need to update Village to default one, whenever subDistrict changes..
    private void updateV(View view) {
        Context context = view.getContext();
        VillageResponse villageResponse = new VillageResponse();
        villageResponse.setVillageName( context.getString( R.string.select_village ) );
        villageResponse.setId( "0" );
        Register.getVillageResponse().setValue( villageResponse );
    }

    //  filter
    public void filter(String charText) {
        charText = charText.toLowerCase( Locale.getDefault() );
        subDistrictResponseArrayList.clear();
        if (charText.length() == 0) {
            subDistrictResponseArrayList.addAll( searchList );
        } else {
            for (SubDistrictResponse model : searchList) {
                if (model.getMandalName().toLowerCase( Locale.getDefault() )
                        .contains( charText )) {
                    subDistrictResponseArrayList.add( model );
                }
            }
        }
        notifyDataSetChanged();
    }
}
