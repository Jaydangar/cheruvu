package enums;

//  For temperature in Fahrenheit use units=imperial
//  For temperature in Celsius use units=metric
//  Temperature in Kelvin is used by default, no need to use units parameter in API call
public enum Units {
    metric,imperial
}
