package in.cheruvu.cheruvu.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import enums.Crop;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivityCropDetailsBinding;
import io.realm.Realm;
import network.NetworkResource;
import pojo.CropData;
import pojo.Farmer;
import request.CropDataUpdateRequest;
import utility.PortraitHelper;
import utility.QueryUtils;

public class CropDetails extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "CropDetails";

    private ActivityCropDetailsBinding activityCropDetailsBinding;

    private Spinner spinner;
    private TextInputEditText yieldInput, acresInput, sellingRateInput;

    private String yield, acres, sellingPrice;
    private Crop crop;

    //  create Realm
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        PortraitHelper.initialize( this );

        realm = Realm.getDefaultInstance();

        activityCropDetailsBinding = DataBindingUtil.setContentView( this, R.layout.activity_crop_details );
        spinner = activityCropDetailsBinding.IdCrop;
        yieldInput = activityCropDetailsBinding.IdTotalYields;
        acresInput = activityCropDetailsBinding.idAcreage;
        sellingRateInput = activityCropDetailsBinding.IdTotalSellingRate;

        fetchAndSetData( activityCropDetailsBinding );

        setupSpinner( spinner );
    }

    private void setupSpinner(Spinner spinner) {

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<Crop> cropArrayAdapter = new ArrayAdapter<>( this, android.R.layout.simple_spinner_dropdown_item, Crop.values() );

        // Apply the adapter to the spinner
        spinner.setAdapter( cropArrayAdapter );

        spinner.setOnItemSelectedListener( this );
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        crop = (Crop) parent.getSelectedItem();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onComplete(View view) {

        acres = Objects.requireNonNull( acresInput.getText() ).toString();
        yield = Objects.requireNonNull( yieldInput.getText() ).toString();
        sellingPrice = Objects.requireNonNull( sellingRateInput.getText() ).toString();

        List<String> errorList = verify();
        if (errorList.isEmpty()) {
            CropDataUpdateRequest cropDataUpdateRequest = new CropDataUpdateRequest();
            cropDataUpdateRequest.setCrop( crop );
            cropDataUpdateRequest.setCropAcres( Float.valueOf( acres ) );
            cropDataUpdateRequest.setCropName( crop.name() );
            cropDataUpdateRequest.setCropPrice( Float.valueOf( sellingPrice ) );
            cropDataUpdateRequest.setCropYield( Float.valueOf( yield ) );

            HashMap<String, String> map = QueryUtils.getCredentials( realm );
            cropDataUpdateRequest.setCropId( map.get( "cropId" ) );

            String authToken = map.get( "authToken" );
            String farmerId = map.get( "farmerId" );

            NetworkResource.getInstance().updateCrop( authToken, farmerId, cropDataUpdateRequest, view.getContext() );
        } else {
            Toast.makeText( this, errorList.get( 0 ), Toast.LENGTH_SHORT ).show();
        }
    }

    List<String> verify() {
        List<String> errorList = new ArrayList<>();
        if (crop.name().equals( this.getResources().getString( R.string.crop ) )) {
            errorList.add( getResources().getString( R.string.select_crop_error ) );
        } else if (yield.isEmpty()) {
            errorList.add( getResources().getString( R.string.select_yield_error ) );
        } else if (acres.isEmpty()) {
            errorList.add( getResources().getString( R.string.select_acre_error ) );
        } else if (sellingPrice.isEmpty()) {
            errorList.add( getResources().getString( R.string.select_income_error ) );
        }
        return errorList;
    }

    private void fetchAndSetData(ActivityCropDetailsBinding activityCropDetailsBinding) {

        //  if there's update/View data Request then fetch and load data on view...
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String msg = bundle.getString( "fragmentMsg" );
            assert msg != null;
            if (msg.equals( "cropDetails" )) {

                HashMap<String, String> credentials = QueryUtils.getCredentials( realm );
                String farmerId = credentials.get( "farmerId" );
                Farmer farmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();
                CropData cropData = farmer.getCropData().get( 0 );
                if (cropData != null) {
                    activityCropDetailsBinding.idAcreage.setText( String.valueOf( cropData.getCropAcres() ) );
                    activityCropDetailsBinding.IdTotalSellingRate.setText( String.valueOf( cropData.getCropPrice() ) );
                    activityCropDetailsBinding.IdTotalYields.setText( String.valueOf( cropData.getCropYield() ) );
                    activityCropDetailsBinding.IdCrop.setPrompt( Crop.valueOf( cropData.getCrop() ).name() );

                    activityCropDetailsBinding.buttonContinue.setText( getResources().getString( R.string.update_personal_details ) );
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
