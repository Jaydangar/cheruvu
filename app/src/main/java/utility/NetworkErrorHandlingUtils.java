package utility;

import java.util.ArrayList;

import application.ApplicationClass;
import enums.ErrorCode;
import in.cheruvu.cheruvu.R;

//  This class is specially created for error checking for retrofit API error.
public class NetworkErrorHandlingUtils {

    //  check various types of errors and send specified message on AlertDialog...
    //  use error UI, for particular API Error
    //  tip to use Context getApplication Context.... via ApplicationClass.getContext() look into ApplicationClass
    public static ArrayList<String> ErrorCheck(ErrorCode errorCode) {
        ArrayList<String> errors = new ArrayList<>();
        if (errorCode != null) {
            switch (errorCode) {
                case INTERNAL_SERVER_ERROR:
                    errors.add( ApplicationClass.getContext().getString(R.string.server_error_try_after_some_time));
                    break;
                case BAD_CREDENTIALS:
                    errors.add(ApplicationClass.getContext().getString(R.string.bad_credentials));
                    break;
                case USER_ALREADY_LOGGED_IN:
                    errors.add(ApplicationClass.getContext().getString(R.string.you_are_already_logged_in_on_another_device));
                    break;
                case FORBIDDEN_ERROR:
                    errors.add( ApplicationClass.getContext().getString(R.string.you_are_already_logged_in_on_another_device) );
                    break;
                case SESSION_EXPIRED:
                    errors.add(ApplicationClass.getContext().getString(R.string.session_expired_try_after_some_time));
                    break;
                case INVALID_EMAIL_ID:
                    errors.add(ApplicationClass.getContext().getString(R.string.invalid_email));
                    break;
                case BAD_REQUEST_ERROR:
                    errors.add(ApplicationClass.getContext().getString(R.string.try_again));
                    break;
                case NO_USER_LOGGED_IN:
                    errors.add( ApplicationClass.getContext().getString(R.string.try_again) );
                    break;
                case UPDATE_FAILED_ERROR:
                    errors.add(ApplicationClass.getContext().getString(R.string.failed_updating));
                    break;
                case USER_NOT_REGISTERED:
                    errors.add(ApplicationClass.getContext().getString(R.string.you_are_not_registered));
                    break;
                case USER_ALREADY_REGISTERED:
                    errors.add(ApplicationClass.getContext().getString(R.string.you_are_already_registered));
                    break;
                case INVALID_OTP:
                    errors.add(ApplicationClass.getContext().getString(R.string.otp_is_invalid_please_try_again));
                    break;
            }
        }
        return errors;
    }
}
