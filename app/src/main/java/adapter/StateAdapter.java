package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.Register;
import response.DistrictResponse;
import response.StatesResponse;
import response.SubDistrictResponse;
import response.VillageResponse;

public class StateAdapter extends BaseAdapter {

    private List<StatesResponse> searchList;
    private ArrayList<StatesResponse> statesResponseArrayList;
    private Context context;
    private LayoutInflater inflater;
    private MutableLiveData<StatesResponse> statesResponseMutableLiveData;

    static class ViewHolder {
        TextView eachState;
    }

    public StateAdapter(Context context, ArrayList<StatesResponse> statesList) {
        this.context = context;
        inflater = LayoutInflater.from( context );
        this.statesResponseArrayList = statesList;
        statesResponseMutableLiveData = Register.getStatesResponse();
        searchList = new ArrayList<>();
        searchList.addAll( statesList );
    }

    @Override
    public int getCount() {
        return statesResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return statesResponseArrayList.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate( R.layout.item_selection, null );
            viewHolder.eachState = convertView.findViewById( R.id.listItem );
            convertView.setTag( viewHolder );
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //  setting text on items...
        viewHolder.eachState.setText( statesResponseArrayList.get( position ).getStateName() );

        //  setting onclicklistener...
        View finalConvertView = convertView;
        convertView.setOnClickListener( v -> {
            StatesResponse statesResponse = statesResponseArrayList.get( position );
            statesResponseMutableLiveData.postValue( statesResponse );
            //  when we update State we need to make sure user updates District, SubDistrict, Village as well...
            updateDSV( finalConvertView );
            ((Activity) context).finish();
        } );

        return convertView;
    }

    //  we need to update District, SubDistrict and Village to default one, whenever state changes..
    private void updateDSV(View view) {
        Context context = view.getContext();
        DistrictResponse districtResponse = new DistrictResponse();
        districtResponse.setDistrictName( context.getString( R.string.select_district ) );
        districtResponse.setId( "0" );
        Register.getDistrictResponse().setValue( districtResponse );

        SubDistrictResponse subDistrictResponse = new SubDistrictResponse();
        subDistrictResponse.setMandalName( context.getString( R.string.select_sub_district ) );
        subDistrictResponse.setId( "0" );
        Register.getSubDistrictResponse().setValue( subDistrictResponse );

        VillageResponse villageResponse = new VillageResponse();
        villageResponse.setVillageName( context.getString( R.string.select_village ) );
        villageResponse.setId( "0" );
        Register.getVillageResponse().setValue( villageResponse );
    }

    //  filter
    public void filter(String charText) {
        charText = charText.toLowerCase( Locale.getDefault() );
        statesResponseArrayList.clear();
        if (charText.length() == 0) {
            statesResponseArrayList.addAll( searchList );
        } else {
            for (StatesResponse model : searchList) {
                if (model.getStateName().toLowerCase( Locale.getDefault() )
                        .contains( charText )) {
                    statesResponseArrayList.add( model );
                }
            }
        }
        notifyDataSetChanged();
    }

}