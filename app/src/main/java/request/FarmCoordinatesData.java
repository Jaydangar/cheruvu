package request;

import java.util.List;

import pojo.LatLng;

public class FarmCoordinatesData {

    private String farmerId;
    private String farmerReferenceId;
    private List<LatLng> path;

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFarmerReferenceId() {
        return farmerReferenceId;
    }

    public void setFarmerReferenceId(String farmerReferenceId) {
        this.farmerReferenceId = farmerReferenceId;
    }

    public List<LatLng> getPath() {
        return path;
    }

    public void setPath(List<LatLng> path) {
        this.path = path;
    }
}
