package singleton;

import android.content.Context;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import application.ApplicationClass;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utility.NetworkConnectionInterceptor;

public class RetrofitInstance {

    private static Retrofit retrofitInstance;
    private static Retrofit retrofitWeatherInstance;
    private static final Object LOCK = new Object();

    //  defining TimeOuts...
    private static final long CONNECTION_TIMEOUT = 5;
    private static final long READ_TIMEOUT = 5;
    private static final long WRITE_TIMEOUT = 5;

    private static final String BASE_URL = "http://54.218.116.181:8081/cheruvu-spring/mobile/";
    //  private static final String BASE_URL = "http://192.168.0.108:7890/cheruvu-spring/mobile/";

    //  this is the weather URL...
    private static final String WEATHER_BASE_URL = "http://api.apixu.com/v1/";

    public static Retrofit getRetrofitInstance() {
        if (retrofitInstance == null) {
            synchronized (LOCK) {
                retrofitInstance = new Retrofit.Builder()
                        .baseUrl( BASE_URL )
                        .addConverterFactory( GsonConverterFactory.create( new Gson() ) )
                        .client( provideOkHttpClientDebug( ApplicationClass.getContext() ) )
                        .build();
            }
        }
        return retrofitInstance;
    }

    public static Retrofit getRetrofitWeatherInstance() {
        if (retrofitWeatherInstance == null) {
            synchronized (LOCK) {
                retrofitWeatherInstance = new Retrofit.Builder()
                        .baseUrl( WEATHER_BASE_URL )
                        .addConverterFactory( GsonConverterFactory.create( new Gson() ) )
                        .client( provideOkHttpClientDebug( ApplicationClass.getContext() ) )
                        .build();
            }
        }
        return retrofitWeatherInstance;
    }

    //  this one contains debug mode...
    private static OkHttpClient provideOkHttpClientDebug(Context context) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient.addInterceptor(httpLoggingInterceptor).addInterceptor(new NetworkConnectionInterceptor(context));
        okHttpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MINUTES);
        okHttpClient.readTimeout(READ_TIMEOUT, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(WRITE_TIMEOUT, TimeUnit.MINUTES);
        return okHttpClient.build();
    }

    private static OkHttpClient provideOkHttpClient(Context context) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.addInterceptor( new NetworkConnectionInterceptor( context ) );
        okHttpClient.connectTimeout( CONNECTION_TIMEOUT, TimeUnit.MINUTES );
        okHttpClient.readTimeout( READ_TIMEOUT, TimeUnit.MINUTES );
        okHttpClient.writeTimeout( WRITE_TIMEOUT, TimeUnit.MINUTES );
        return okHttpClient.build();
    }
}
