package in.cheruvu.cheruvu.activities;


import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashMap;
import java.util.Locale;

import fragments.HomeFragment;
import fragments.NutrientManagementFragment;
import fragments.ProfileFragment;
import fragments.SoilTestFragment;
import fragments.WeatherFragment;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivityHomeScreenBinding;
import io.realm.Realm;
import io.realm.RealmList;
import pojo.CropData;
import pojo.Farmer;
import utility.CustomDialogUtils;
import utility.IntentUtils;
import utility.PortraitHelper;
import utility.QueryUtils;
import utility.SharedPreferencesUtils;

public class HomeScreenActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    // get shared prefs utils
    SharedPreferencesUtils localStorage;

    // language locale
    //Locale
    Locale locale;

    private static final String TAG = "HomeScreenActivity";
    private ActivityHomeScreenBinding activityHomeScreenBinding;
    private Realm realm;
    private Farmer farmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        PortraitHelper.initialize( this );

        localStorage = SharedPreferencesUtils.getInstance( HomeScreenActivity.this );

        activityHomeScreenBinding = DataBindingUtil.setContentView( this, R.layout.activity_home_screen );
        realm = Realm.getDefaultInstance();

        HashMap<String, String> credentials = QueryUtils.getCredentials( realm );
        String farmerId = credentials.get( "farmerId" );

        farmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();
        activityHomeScreenBinding.helloName.setText( getResources().getText( R.string.hey ) + " " + farmer.getFirstName() );

        //init bottom navigation bar
        activityHomeScreenBinding.navigation.setOnNavigationItemSelectedListener( this );

        if (localStorage.getLanguagePref( "language" ) != null) {
            String languageSelected = localStorage.getLanguagePref( "language" );
            SetText( languageSelected );
        }

        RealmList<CropData> farmerCropData = farmer.getCropData();
        if (!farmerCropData.isEmpty()) {
            Fragment fragment = new HomeFragment();
            replaceFragment( fragment );
        } else {
            IntentUtils.PassIntent( this, CropDetails.class );
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations( R.anim.slide_in_left, R.anim.slide_out_right );
        ft.replace( R.id.main_fragment_placeholder, fragment );
        ft.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                fragment = new HomeFragment();
                replaceFragment( fragment );
                return true;
            case R.id.navigation_advice:
                fragment = new NutrientManagementFragment();
                replaceFragment( fragment );
                return true;
            case R.id.navigation_soil_test:
                fragment = new SoilTestFragment();
                replaceFragment( fragment );
                return true;
            case R.id.navigation_weather:
                fragment = new WeatherFragment();
                replaceFragment( fragment );
                return true;
            case R.id.navigation_profile:
                fragment = new ProfileFragment();
                replaceFragment( fragment );
                return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onBackPressed() {
        CustomDialogUtils customDialogUtils = new CustomDialogUtils( this );
        customDialogUtils.setAlertTitle( getResources().getString( R.string.dialog_close_application ) );

        customDialogUtils.setAlertDesciption( getResources().getString( R.string.description_close_application ) );
        customDialogUtils.setLeftButtonText( getResources().getString( R.string.button_yes ) );
        customDialogUtils.setRightButtonText( getResources().getString( R.string.button_no ) );

        customDialogUtils.setOnActionListener( viewId -> {
            switch (viewId.getId()) {
                case R.id.btn_left:
                    finish();
                    break;
                case R.id.btn_right:
                    customDialogUtils.dismiss();
                    break;
            }
        } );
        customDialogUtils.show();
    }

    private void SetText(String LanguageLocale) {
        locale = new Locale( LanguageLocale );
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration( conf, dm );
    }
}
