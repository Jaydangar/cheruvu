package fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import io.realm.Realm;
import network.NetworkResource;
import request.PlantingDateRequest;
import utility.QueryUtils;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private Realm realm;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get( Calendar.YEAR );
        int month = calendar.get( Calendar.MONTH );
        int day = calendar.get( Calendar.DAY_OF_MONTH );

        realm = Realm.getDefaultInstance();

        DatePickerDialog dpd = new DatePickerDialog( getActivity(), this, year, month, day );
        return dpd;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // Do something with the chosen date

        // Create a Date variable/object with user chosen date
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis( 0 );
        cal.set( year, month, dayOfMonth, 0, 0, 0 );
        Date chosenDate = cal.getTime();

        // Format the date using style and locale
        DateFormat df = DateFormat.getDateInstance( DateFormat.MEDIUM, Locale.US);
        String formattedDate = df.format( chosenDate );

        HashMap<String,String> credentials = QueryUtils.getCredentials(realm);
        String farmerId = credentials.get( "farmerId" );
        String authToken = credentials.get( "authToken" );

        PlantingDateRequest plantingDateRequest = new PlantingDateRequest();
        plantingDateRequest.setPlantingDate( formattedDate );

        NetworkResource.getInstance().setPlantingDate(authToken,farmerId,plantingDateRequest,getContext());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
}