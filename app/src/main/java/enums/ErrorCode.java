package enums;

public enum ErrorCode {
    BAD_CREDENTIALS, USER_ALREADY_LOGGED_IN, SESSION_EXPIRED, NO_USER_LOGGED_IN, INVALID_EMAIL_ID, BAD_REQUEST_ERROR, UPDATE_FAILED_ERROR, USER_ALREADY_REGISTERED, USER_NOT_REGISTERED, FORBIDDEN_ERROR, INTERNAL_SERVER_ERROR,INVALID_OTP
}