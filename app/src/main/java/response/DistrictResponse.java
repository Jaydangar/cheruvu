package response;

import com.google.gson.annotations.SerializedName;

public class DistrictResponse{

	@SerializedName("lastUpdated")
	private long lastUpdated;

	@SerializedName("districtName")
	private String districtName;

	@SerializedName("creationTime")
	private long creationTime;

	@SerializedName("createdBy")
	private String createdBy;

	@SerializedName("stateId")
	private String stateId;

	@SerializedName("id")
	private String id;

	@SerializedName("state")
	private String state;

	public void setLastUpdated(long lastUpdated){
		this.lastUpdated = lastUpdated;
	}

	public long getLastUpdated(){
		return lastUpdated;
	}

	public void setDistrictName(String districtName){
		this.districtName = districtName;
	}

	public String getDistrictName(){
		if (this.districtName == null)
			return "";
		else
			return districtName;
	}

	public void setCreationTime(long creationTime){
		this.creationTime = creationTime;
	}

	public long getCreationTime(){
		return creationTime;
	}

	public void setCreatedBy(String createdBy){
		this.createdBy = createdBy;
	}

	public String getCreatedBy(){
		return createdBy;
	}

	public void setStateId(String stateId){
		this.stateId = stateId;
	}

	public String getStateId(){
		if (this.stateId == null)
			return "";
		else
			return stateId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		if (this.id == null)
			return "";
		else
			return id;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		if (this.state == null)
			return "";
		else
			return state;
	}

	@Override
 	public String toString(){
		return 
			"DistrictResponse{" + 
			"lastUpdated = '" + lastUpdated + '\'' + 
			",districtName = '" + districtName + '\'' + 
			",creationTime = '" + creationTime + '\'' + 
			",createdBy = '" + createdBy + '\'' + 
			",stateId = '" + stateId + '\'' + 
			",id = '" + id + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}