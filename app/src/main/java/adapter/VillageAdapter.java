package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.Register;
import response.VillageResponse;

public class VillageAdapter extends BaseAdapter {

    List<VillageResponse> searchList;
    private ArrayList<VillageResponse> villageResponseArrayList;
    private Context context;
    private LayoutInflater inflater;
    private MutableLiveData<VillageResponse> villageResponseMutableLiveData;

    static class ViewHolder {
        TextView eachItem;
    }

    public VillageAdapter(Context context, ArrayList<VillageResponse> arrayAdapter) {
        this.context = context;
        inflater = LayoutInflater.from( context );
        this.villageResponseArrayList = arrayAdapter;
        villageResponseMutableLiveData = Register.getVillageResponse();
        searchList = new ArrayList<>();
        searchList.addAll( villageResponseArrayList );
    }

    @Override
    public int getCount() {
        return villageResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return villageResponseArrayList.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate( R.layout.item_selection, null );
            viewHolder.eachItem = convertView.findViewById( R.id.listItem );
            convertView.setTag( viewHolder );
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //  setting text on items...
        viewHolder.eachItem.setText( villageResponseArrayList.get( position ).getVillageName() );

        //  setting onclicklistener...
        convertView.setOnClickListener( v -> {
            VillageResponse villageResponse = villageResponseArrayList.get( position );
            villageResponseMutableLiveData.postValue( villageResponse );
            ((Activity) context).finish();
        } );

        return convertView;
    }

    //  filter
    public void filter(String charText) {
        charText = charText.toLowerCase( Locale.getDefault() );
        villageResponseArrayList.clear();
        if (charText.length() == 0) {
            villageResponseArrayList.addAll( searchList );
        } else {
            for (VillageResponse model : searchList) {
                if (model.getVillageName().toLowerCase( Locale.getDefault() )
                        .contains( charText )) {
                    villageResponseArrayList.add( model );
                }
            }
        }
        notifyDataSetChanged();
    }
}
