package fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import adapter.WeatherAdapter;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.PlotActivity;
import in.cheruvu.cheruvu.activities.SingleDayWeather;
import in.cheruvu.cheruvu.databinding.FragmentWeatherBinding;
import io.realm.Realm;
import io.realm.RealmList;
import listeners.OnAlertButtonClickListener;
import pojo.ForecastdayItem;
import response.ForecastResponse;
import utility.AlertUtils;
import utility.ConstantUtils;
import utility.IntentUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = "WeatherFragment";

    private Realm realm;
    private String location;
    private RealmList<ForecastdayItem> initializeList;
    private OnAlertButtonClickListener onAlertButtonClickListener;

    private Context context;

    private WeatherAdapter weatherAdapter;

    public WeatherFragment() {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentWeatherBinding fragmentWeatherBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false);

        //  listView
        ListView listView = fragmentWeatherBinding.weatherList;

        realm = Realm.getDefaultInstance();

        initializeList = new RealmList<>();

        weatherAdapter = new WeatherAdapter(context, initializeList);
        listView.setAdapter(weatherAdapter);

        onAlertButtonClickListener = from -> {
            switch (from) {
                case DialogInterface.BUTTON_POSITIVE:
                    IntentUtils.PassIntent(context, PlotActivity.class);
                    break;
            }
        };

        ForecastResponse forecastResponse = fetchWeatherData();
        if (forecastResponse != null) {
            initializeList.addAll(forecastResponse.getForecast().getForecastday());
            this.location = forecastResponse.getLocation().getName();
            weatherAdapter.notifyDataSetChanged();
        } else {
            AlertUtils alertUtils = AlertUtils.getInstance(onAlertButtonClickListener);
            alertUtils.showDialogWithPositiveAndNegativeBtn(context, context.getString(R.string.plotNoticeTitle), context.getString(R.string.plotNoticeQuestion), context.getString(R.string.button_yes), context.getString(R.string.button_no));
        }

        listView.setOnItemClickListener(this);
        return fragmentWeatherBinding.getRoot();
    }

    private ForecastResponse fetchWeatherData() {
        return realm.where(ForecastResponse.class).equalTo(ForecastResponse.Constants.ID, ConstantUtils.getForecastId()).findFirst();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ForecastdayItem listItem = initializeList.get(position);

        //  update Location
        SingleDayWeather.getWeatherData().postValue(listItem);
        SingleDayWeather.getLocation().postValue(this.location);

        IntentUtils.PassIntent(getContext(), SingleDayWeather.class);
    }
}
