package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentFertilizerRecommendationDayThirtyBinding;
import network.NetworkResource;
import utility.ImageSetterUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FertilizerRecommendationDayThirty extends Fragment {

    public FertilizerRecommendationDayThirty() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        FragmentFertilizerRecommendationDayThirtyBinding inflate = DataBindingUtil.inflate( inflater, R.layout.fragment_fertilizer_recommendation_day_thirty, container, false );

        NetworkResource.getInstance().getRecommendationSheetData().observe( this, recommendationSheetResponse -> {
            double mop = recommendationSheetResponse.getMopByDay().getDay30();
            double ssp = recommendationSheetResponse.getSspByDay().getDay30();
            double urea = recommendationSheetResponse.getUreaByDay().getDay30();

            ImageSetterUtils.setImageAndText( getContext(), inflate.dapBagsImg, inflate.dapUsage, mop, "mop" );
            ImageSetterUtils.setImageAndText( getContext(), inflate.superBagsImg, inflate.superUsage, ssp, "ssp" );
            ImageSetterUtils.setImageAndText( getContext(), inflate.ureaBagsImg, inflate.ureaUsage, urea, "urea" );

            if (mop == 0.0) {
                inflate.dayThirtyPotashGroup.setVisibility( View.GONE );
            }
            if (ssp == 0.0) {
                inflate.dayThirtySuperGroup.setVisibility( View.GONE );
            }
            if (urea == 0.0) {
                inflate.dayThirtyUreaGroup.setVisibility( View.GONE );
            }
        } );

        return inflate.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
