package utility;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Locale;

import application.ApplicationClass;

public class ChangeLanguageUtils {

    public static void setText(String LanguageLocale) {
        Locale locale = new Locale( LanguageLocale );
        Resources res = ApplicationClass.getContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration( conf, dm );
    }

}
