package pojo;

import java.util.ArrayList;
import java.util.List;

public class UpdateFarmerRequest {

    private Farmer farmer;

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<String>();
        if (farmer == null) {
            errors.add( "farmer" );
        }
        return errors;
    }
}
