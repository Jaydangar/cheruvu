package utility;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

//  handles Intent related Tasks...
public class IntentUtils {

    //  passing from one to another activity
    public static void PassIntent(Context From, Class To) {
        Intent intent = new Intent( From, To );
        intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        intent.addFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP);
        From.startActivity( intent );
    }

    //  if you want to pass intent with data
    public static void PassIntentWithBundle(Context From, Class To, Bundle Data) {
        Intent intent = new Intent( From, To );
        intent.putExtras( Data );
        intent.setFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP);
        From.startActivity( intent );
    }
}
