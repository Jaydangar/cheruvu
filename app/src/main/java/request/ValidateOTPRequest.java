package request;

import java.util.ArrayList;
import java.util.List;

import application.ApplicationClass;
import in.cheruvu.cheruvu.R;

public class ValidateOTPRequest {

    private String userId;
    private String phoneNumber;
    private String value;
    private String countryCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    //  verification
    public List<String> verify() {
        List<String> errors = new ArrayList<>();
        if (this.value == null) {
            errors.add( ApplicationClass.getContext().getString(R.string.invalid_otp));
        }
        if (this.countryCode == null) {
            errors.add(ApplicationClass.getContext().getString(R.string.invalid_country_code));
        }
        if (this.phoneNumber == null) {
            errors.add(ApplicationClass.getContext().getString(R.string.invalid_phone_number));
        }
        return errors;
    }
}
