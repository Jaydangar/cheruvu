package pojo;

import enums.CHANNEL;
import enums.INDUSTRY_TYPE;
import enums.WEBSITE;

public class PaymentLogResponse {

    private String merchantId;
    private String orderId;
    private String customerId;
    private String amountPaid;
    private CHANNEL channelId;
    private INDUSTRY_TYPE industry_type;
    private WEBSITE website;
    private boolean paymentDone;
    private String checksumHash;

    public String getChecksumHash() {
        if (this.checksumHash == null)
            return "";
        else
            return checksumHash;
    }

    public void setChecksumHash(String checksumHash) {
        this.checksumHash = checksumHash;
    }

    public String getMerchantId() {
        if (this.merchantId == null)
            return "";
        else
            return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderId() {
        if (this.orderId == null)
            return "";
        else
            return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerId() {
        if (this.customerId == null)
            return "";
        else
            return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAmountPaid() {
        if (this.amountPaid == null)
            return "";
        else
            return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public CHANNEL getChannelId() {
        return channelId;
    }

    public void setChannelId(CHANNEL channelId) {
        this.channelId = channelId;
    }

    public INDUSTRY_TYPE getIndustry_type() {
        return industry_type;
    }

    public void setIndustry_type(INDUSTRY_TYPE industry_type) {
        this.industry_type = industry_type;
    }

    public WEBSITE getWebsite() {
        return website;
    }

    public void setWebsite(WEBSITE website) {
        this.website = website;
    }

    public boolean isPaymentDone() {
        return paymentDone;
    }

    public void setPaymentDone(boolean paymentDone) {
        this.paymentDone = paymentDone;
    }
}
