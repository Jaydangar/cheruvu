package utility;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import in.cheruvu.cheruvu.R;

public class ImageSetterUtils {

    public static void setImageAndText(Context context, ImageView imageview, TextView usage, double usageValue, String fertilizer) {
        switch (fertilizer) {
            case "mop":
                setDAP( imageview, usageValue );
                usage.setText( context.getString( R.string.apply ) + " " + usageValue + " " + context.getString( R.string.apply_potash ) );
                break;
            case "ssp":
                setSSP( imageview, usageValue );
                usage.setText( context.getString( R.string.apply ) + " " + usageValue + " " + context.getString( R.string.apply_super ) );
                break;
            case "urea":
                setUrea( imageview, usageValue );
                usage.setText( context.getString( R.string.apply ) + " " + usageValue + " " + context.getString( R.string.apply_urea ) );
                break;
            case "zn":
                setUrea( imageview, usageValue );
                usage.setText( context.getString( R.string.apply ) + " " + usageValue + " " + context.getString( R.string.apply_zinc ) );
                break;
        }
    }

    private static void setUrea(ImageView imageview, double usageValue) {
        if (usageValue > 0 && usageValue <= 0.5) {
            imageview.setImageResource( R.drawable.ic_urea_a_half_bag );
        }
        else if (usageValue > 0.5 && usageValue <= 1) {
            imageview.setImageResource( R.drawable.ic_urea_one_bag );
        }else if (usageValue > 1 && usageValue <= 1.33) {
            imageview.setImageResource( R.drawable.ic_urea_one_and_a_quater_bag );
        } else if (usageValue > 1.33 && usageValue <= 1.5) {
            imageview.setImageResource( R.drawable.ic_urea_one_and_a_half_bag );
        } else if (usageValue > 1.5 && usageValue <= 1.75) {
            imageview.setImageResource( R.drawable.ic_urea_one_and_three_quater_bag );
        } else if (usageValue > 1.75 && usageValue <= 2) {
            imageview.setImageResource( R.drawable.ic_urea_two_bag );
        } else if (usageValue > 2 && usageValue <= 2.25) {
            imageview.setImageResource( R.drawable.ic_urea_two_and_a_quater_bag );
        } else if (usageValue > 2.25 && usageValue <= 2.33) {
            imageview.setImageResource( R.drawable.ic_urea_two_and_a_quater_bag );
        } else if (usageValue > 2.33 && usageValue <= 2.5) {
            imageview.setImageResource( R.drawable.ic_urea_two_and_a_half_bag );
        } else if (usageValue > 2.5 && usageValue <= 2.75) {
            imageview.setImageResource( R.drawable.ic_urea_two_and_three_quater_bag );
        } else if (usageValue > 2.75 && usageValue <= 3) {
            imageview.setImageResource( R.drawable.ic_urea_three_bags );
        } else if (usageValue > 3 && usageValue <= 3.33) {
            imageview.setImageResource( R.drawable.ic_urea_three_quater_bag );
        } else if (usageValue > 3.33 && usageValue <= 3.5) {
            imageview.setImageResource( R.drawable.ic_urea_three_and_a_half_bags );
        } else if (usageValue > 3.5 && usageValue <= 4) {
            imageview.setImageResource( R.drawable.ic_urea_three_and_three_quaters_bags );
        }
    }

    private static void setSSP(ImageView imageview, double usageValue) {
        if (usageValue > 0 && usageValue <= 0.5) {
            imageview.setImageResource( R.drawable.ic_super_a_half_bags );
        }
        else if (usageValue > 0.5 && usageValue <= 1) {
            imageview.setImageResource( R.drawable.ic_super_one_bag );
        }else if (usageValue > 1 && usageValue <= 1.33) {
            imageview.setImageResource( R.drawable.ic_super_a_quater_bags );
        } else if (usageValue > 1.33 && usageValue <= 1.5) {
            imageview.setImageResource( R.drawable.ic_super_one_and_a_half_bag );
        } else if (usageValue > 1.5 && usageValue <= 1.75) {
            imageview.setImageResource( R.drawable.ic_super_one_and_three_quater_bags );
        } else if (usageValue > 1.75 && usageValue <= 2) {
            imageview.setImageResource( R.drawable.ic_super_two_bags );
        } else if (usageValue > 2 && usageValue <= 2.33) {
            imageview.setImageResource( R.drawable.ic_super_two_and_a_quater_bag );
        } else if (usageValue > 2.33 && usageValue <= 2.5) {
            imageview.setImageResource( R.drawable.ic_super_two_and_a_half_bags );
        } else if (usageValue > 2.5 && usageValue <= 2.75) {
            imageview.setImageResource( R.drawable.ic_super_two_and_three_quater_bags );
        } else if (usageValue > 2.75 && usageValue <= 3) {
            imageview.setImageResource( R.drawable.ic_super_three_bags );
        } else if (usageValue > 3 && usageValue <= 3.33) {
            imageview.setImageResource( R.drawable.ic_super_three_and_a_quater_bag );
        } else if (usageValue > 3.33 && usageValue <= 3.5) {
            imageview.setImageResource( R.drawable.ic_super_three_and_a_half_bag );
        } else if (usageValue > 3.5 && usageValue <= 4) {
            imageview.setImageResource( R.drawable.ic_super_three_and_three_quater_bags );
        }
    }

    private static void setDAP(ImageView imageview, double usageValue) {
        if (usageValue > 0 && usageValue <= 0.5) {
            imageview.setImageResource( R.drawable.ic_potash_a_quater );
        }
        else if (usageValue > 0.5 && usageValue <= 1) {
            imageview.setImageResource( R.drawable.ic_potash_one );
        }
        else if (usageValue > 1 && usageValue <= 1.33) {
            imageview.setImageResource( R.drawable.ic_potash_one_and_a_quater );
        } else if (usageValue > 1.33 && usageValue <= 1.5) {
            imageview.setImageResource( R.drawable.ic_potash_one_and_a_half );
        } else if (usageValue > 1.5 && usageValue <= 1.75) {
            imageview.setImageResource( R.drawable.ic_potash_one_and_three_quaters );
        } else if (usageValue > 1.75 && usageValue <= 2) {
            imageview.setImageResource( R.drawable.ic_potash_two );
        } else if (usageValue > 2 && usageValue <= 2.33) {
            imageview.setImageResource( R.drawable.ic_potash_two_and_a_quater );
        } else if (usageValue > 2.33 && usageValue <= 2.5) {
            imageview.setImageResource( R.drawable.ic_potash_two_and_a_half );
        } else if (usageValue > 2.5 && usageValue <= 2.75) {
            imageview.setImageResource( R.drawable.ic_potash_two_and_three_quaters );
        } else if (usageValue > 2.75 && usageValue <= 3) {
            imageview.setImageResource( R.drawable.ic_potash_three );
        } else if (usageValue > 3 && usageValue <= 3.33) {
            imageview.setImageResource( R.drawable.ic_potash_three_and_a_quater );
        } else if (usageValue > 3.33 && usageValue <= 3.5) {
            imageview.setImageResource( R.drawable.ic_potash_three_and_a_half );
        } else if (usageValue > 3.5 && usageValue <= 4) {
            imageview.setImageResource( R.drawable.ic_potash_three_and_three_quaters );
        }
    }
}
