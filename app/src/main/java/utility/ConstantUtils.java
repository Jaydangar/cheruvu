package utility;

//  this class contains Ids for Entities...
public class ConstantUtils {

    private static final String forecastId = "2a451103-a766-4784-b78e-7b8a2b6240a7";
    private static final String incomeId = "ecb8d87b-7fb5-42ce-bfd5-b57c77a68f55";
    private static final String yieldId = "4450df74-d272-4b83-9c63-4b78f0bff293";
    private static final String recommendationId = "5a209b92-bbf0-4a04-862c-75230ebbbe0e";
    private static final String validationId = "a1c09fb9-ccc1-4392-8a04-0a0cea7d390b";
    private static final String cropId = "8882246a-39a0-4b84-9885-3b31098d0d89";
    private static final String farmCoordinatesId = "43a7da74-773b-11e9-8248-2a86e4085a59";

    //  options for passing the values...
    public static final String STATE_OPTION = "state";
    public static final String DISTRICT_OPTION = "district";
    public static final String SUBDISTRICT_OPTION = "subdistrict";
    public static final String VILLAGE_OPTION = "village";

    //  options for saving Register state...
    private static final String ADDRESS_STATUS = "address";
    private static final String NAME_STATUS = "name";

    //  request Codes
    private static final int READ_SMS_REQUEST = 238;
    private static final int RECEIVE_SMS_REQUEST = 786;

    //  TAG + Work Name
    private static final String PERIODIC_WORK_NAME = "periodic work";

    public static String getFarmCoordinatesId() {
        return farmCoordinatesId;
    }

    public static String getPeriodicWorkName() {
        return PERIODIC_WORK_NAME;
    }

    public static int getReceiveSmsRequest() {
        return RECEIVE_SMS_REQUEST;
    }

    public static int getReadSmsRequest() {
        return READ_SMS_REQUEST;
    }

    public static String getStateOption() {
        return STATE_OPTION;
    }

    public static String getDistrictOption() {
        return DISTRICT_OPTION;
    }

    public static String getSubdistrictOption() {
        return SUBDISTRICT_OPTION;
    }

    public static String getVillageOption() {
        return VILLAGE_OPTION;
    }

    public static String getAddressStatus() {
        return ADDRESS_STATUS;
    }

    public static String getNameStatus() {
        return NAME_STATUS;
    }

    public static String getForecastId() {
        return forecastId;
    }

    public static String getIncomeId() {
        return incomeId;
    }

    public static String getYieldId() {
        return yieldId;
    }

    public static String getRecommendationId() {
        return recommendationId;
    }

    public static String getValidationId() {
        return validationId;
    }

    public static String getCropId() {
        return cropId;
    }
}
