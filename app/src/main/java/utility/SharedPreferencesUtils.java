package utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.Set;

import in.cheruvu.cheruvu.R;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesUtils {

    private static SharedPreferencesUtils sharedPreferencesUtilsInstance;
    private String TAG = SharedPreferencesUtils.class.getName();

    private static SharedPreferences sharedPreferences;

    private SharedPreferencesUtils(Context context) {
        sharedPreferences = context.getSharedPreferences( context.getResources().getString( R.string.cheruvu_default ), MODE_PRIVATE );
    }

    public static synchronized SharedPreferencesUtils getInstance(Context context) {
        if (sharedPreferencesUtilsInstance == null) {
            sharedPreferencesUtilsInstance = new SharedPreferencesUtils( context );
        }
        return sharedPreferencesUtilsInstance;
    }

    //  store Data...
    public void storeData(String key, String json) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString( key, json );
        editor.apply();
    }

    //  will return Data...
    public String getData(String key) {
        return sharedPreferences.getString( key, null );
    }

    //  store Data...
    public void storeBundleData(Bundle bundle) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> keySet = bundle.keySet();
        for (String individualKey : keySet) {
            editor.putString( individualKey, bundle.getString( individualKey ) );
        }
        editor.apply();
    }

    /* set tutorial status  ...*/
    public void setTutorialSeenStatus(String key, Boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean( key, value );
        editor.apply();
    }

    //  get tutorial status
    public boolean getTutorialSeenStatus(String Key) {
        return sharedPreferences.getBoolean( Key, false );
    }

    // set selected language
    public void setLanguagePref(String key, String language) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString( key, language );
        editor.apply();
    }

    // get selected language
    public String getLanguagePref(String key) {
        return sharedPreferences.getString( key, "te" );
    }

    // set agreed or not
    public void setConcenmentPref(String key, boolean concenment) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean( key, concenment );
        editor.apply();
    }

    // get selected language
    public boolean getConcenmentPref(String key) {
        return sharedPreferences.getBoolean( key, false );
    }
}