package request;

import java.util.ArrayList;
import java.util.List;

import application.ApplicationClass;
import in.cheruvu.cheruvu.R;

public class PlantingDateRequest {

	private String plantingDate;

	public String getPlantingDate() {
		return plantingDate;
	}

	public void setPlantingDate(String plantingDate) {
		this.plantingDate = plantingDate;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if(plantingDate.isEmpty()) {
			errors.add( ApplicationClass.getContext().getString(R.string.planting_date_cannot_be_empty));
		}
		return errors;
	}	
	
}
