package response;

import io.realm.annotations.PrimaryKey;
import pojo.SoilTestRecommendation;

public class SoilTestDataResponse {

    private String landSurveyNo;
    @PrimaryKey
    private String farmerId;
    private String villageId;
    private String mandalId;
    private String farmerReferenceId;
    private Double ph;
    private Double ec;
    private Double clay;
    private Double silt;
    private Double sand;
    private String textureClass;
    private Double hue;
    private Double value;
    private Double chroma;
    private String oc;
    private Double n;
    private Double p2o5;
    private Double k2o;
    private Double cu;
    private Double mn;
    private Double fe;
    private Double zn;
    private Double borax;
    private Double yield;
    private Double productivity;
    private SoilTestRecommendation soilTestRecommendation;

    public String getVillageId() {
        if (this.villageId == null)
            return "";
        else
            return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public String getMandalId() {
        if (this.mandalId == null)
            return "";
        else
            return mandalId;
    }

    public void setMandalId(String mandalId) {
        this.mandalId = mandalId;
    }

    public Double getProductivity() {
        if (this.productivity == null)
            return 0.0;
        else
            return productivity;
    }

    public void setProductivity(Double productivity) {
        this.productivity = productivity;
    }

    public String getLandSurveyNo() {
        if (this.landSurveyNo == null)
            return "";
        else
            return landSurveyNo;
    }

    public Double getYield() {
        if (this.yield == null)
            return 0.0;
        else
            return yield;
    }

    public void setYield(Double yield) {
        this.yield = yield;
    }

    public void setLandSurveyNo(String landSurveyNo) {
        this.landSurveyNo = landSurveyNo;
    }

    public String getFarmerId() {
        if (this.farmerId == null)
            return "";
        else
            return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFarmerReferenceId() {
        if (this.farmerReferenceId == null)
            return "";
        else
            return farmerReferenceId;
    }

    public void setFarmerReferenceId(String farmerReferenceId) {
        this.farmerReferenceId = farmerReferenceId;
    }

    public Double getPh() {
        if (this.ph == null)
            return 0.0;
        else
            return ph;
    }

    public void setPh(Double ph) {
        this.ph = ph;
    }

    public Double getEc() {
        if (this.ec == null)
            return 0.0;
        else
            return ec;
    }

    public void setEc(Double ec) {
        this.ec = ec;
    }

    public Double getClay() {
        if (this.clay == null)
            return 0.0;
        else
            return clay;
    }

    public void setClay(Double clay) {
        this.clay = clay;
    }

    public Double getSilt() {
        if (this.silt == null)
            return 0.0;
        else
            return silt;
    }

    public void setSilt(Double silt) {
        this.silt = silt;
    }

    public Double getSand() {
        if (this.sand == null)
            return 0.0;
        else
            return sand;
    }

    public void setSand(Double sand) {
        this.sand = sand;
    }

    public String getTextureClass() {
        if (this.textureClass == null)
            return "";
        else
            return textureClass;
    }

    public void setTextureClass(String textureClass) {
        this.textureClass = textureClass;
    }

    public Double getHue() {
        if (this.hue == null)
            return 0.0;
        else
            return hue;
    }

    public void setHue(Double hue) {
        this.hue = hue;
    }

    public Double getValue() {
        if (this.value == null)
            return 0.0;
        else
            return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getChroma() {
        if (this.chroma == null)
            return 0.0;
        else
            return chroma;
    }

    public void setChroma(Double chroma) {
        this.chroma = chroma;
    }

    public String getOc() {
        if (this.oc == null)
            return "";
        else
            return oc;
    }

    public void setOc(String oc) {
        this.oc = oc;
    }

    public Double getN() {
        if (this.n == null)
            return 0.0;
        else
            return n;
    }

    public void setN(Double n) {
        this.n = n;
    }

    public Double getP2o5() {
        if (this.p2o5 == null)
            return 0.0;
        else
            return p2o5;
    }

    public void setP2o5(Double p2o5) {
        this.p2o5 = p2o5;
    }

    public Double getK2o() {
        if (this.k2o == null)
            return 0.0;
        else
            return k2o;
    }

    public void setK2o(Double k2o) {
        this.k2o = k2o;
    }

    public Double getCu() {
        if (this.cu == null)
            return 0.0;
        else
            return cu;
    }

    public void setCu(Double cu) {
        this.cu = cu;
    }

    public Double getMn() {
        if (this.mn == null)
            return 0.0;
        else
            return mn;
    }

    public void setMn(Double mn) {
        this.mn = mn;
    }

    public Double getFe() {
        if (this.fe == null)
            return 0.0;
        else
            return fe;
    }

    public void setFe(Double fe) {
        this.fe = fe;
    }

    public Double getZn() {
        if (this.zn == null)
            return 0.0;
        else
            return zn;
    }

    public void setZn(Double zn) {
        this.zn = zn;
    }

    public Double getBorax() {
        if (this.borax == null)
            return 0.0;
        else
            return borax;
    }

    public void setBorax(Double borax) {
        this.borax = borax;
    }

    public SoilTestRecommendation getSoilTestRecommendation() {
        return soilTestRecommendation;
    }

    public void setSoilTestRecommendation(SoilTestRecommendation soilTestReconmmendation) {
        this.soilTestRecommendation = soilTestReconmmendation;
    }

    public static class Constants {
        public static final String FARMER_ID = "farmerId";
    }

}
