package in.cheruvu.cheruvu.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import com.bumptech.glide.Glide;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivitySingleDayWeatherBinding;
import pojo.Day;
import pojo.ForecastdayItem;
import utility.PortraitHelper;

public class SingleDayWeather extends AppCompatActivity {

    private static final MutableLiveData<ForecastdayItem> weatherData = new MutableLiveData<>();
    private static final MutableLiveData<String> location = new MutableLiveData<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        PortraitHelper.initialize( this );
        setContentView( R.layout.activity_single_day_weather );

        ActivitySingleDayWeatherBinding activitySingleDayWeatherBinding = DataBindingUtil.setContentView( this, R.layout.activity_single_day_weather );
        weatherData.observe( this, listItem -> {
            ForecastdayItem item = weatherData.getValue();
            if (item != null) {
                Day day = item.getDay();
                activitySingleDayWeatherBinding.normalTemp.setText( String.valueOf( (int) day.getAvgtempC() + "\u00b0 C" ) );
                Glide.with( this ).load( "http:" + day.getCondition().getIcon() ).into( activitySingleDayWeatherBinding.weatherIcon );
                activitySingleDayWeatherBinding.weatherCondition.setText( day.getCondition().getText() );
                activitySingleDayWeatherBinding.normalTempValue.setText( String.valueOf( day.getAvgtempC() + "\u00b0 C" ) );
                activitySingleDayWeatherBinding.maxTempValue.setText( String.valueOf( day.getMaxtempC() + "\u00b0 C" ) );
                activitySingleDayWeatherBinding.minTempValue.setText( String.valueOf( day.getMintempC() + "\u00b0 C" ) );
                activitySingleDayWeatherBinding.windSpeed.setText( String.valueOf( day.getMaxwindKph() + "km/hour" ) );
                activitySingleDayWeatherBinding.uvAmount.setText( String.valueOf( day.getUv() ) );
                activitySingleDayWeatherBinding.humidity.setText( String.valueOf( day.getAvghumidity() ) );
                activitySingleDayWeatherBinding.sunriseTime.setText( String.valueOf( item.getAstro().getSunrise() ) );
                activitySingleDayWeatherBinding.sunsetTime.setText( String.valueOf( item.getAstro().getSunset() ) );
            }
        } );

        location.observe( this, activitySingleDayWeatherBinding.location::setText );
    }

    public static MutableLiveData<ForecastdayItem> getWeatherData() {
        return weatherData;
    }

    public static MutableLiveData<String> getLocation() {
        return location;
    }
}
