package in.cheruvu.cheruvu.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.databinding.DataBindingUtil;

import java.util.ArrayList;

import adapter.DistrictAdapter;
import adapter.StateAdapter;
import adapter.SubDistrictAdapter;
import adapter.VillageAdapter;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivitySelectItemBinding;
import network.NetworkResource;
import response.DistrictResponse;
import response.StatesResponse;
import response.SubDistrictResponse;
import response.VillageResponse;
import utility.PortraitHelper;

import static utility.ConstantUtils.DISTRICT_OPTION;
import static utility.ConstantUtils.STATE_OPTION;
import static utility.ConstantUtils.SUBDISTRICT_OPTION;
import static utility.ConstantUtils.VILLAGE_OPTION;

public class SelectItemActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    ListView listView;

    //  which option was selected? state,District,Mandal,Village??
    private String option;

    //  Adapters...
    private StateAdapter stateAdapter;
    private DistrictAdapter districtAdapter;
    private SubDistrictAdapter subDistrictAdapter;
    private VillageAdapter villageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        //  for portrait only...
        PortraitHelper.initialize( this );

        ActivitySelectItemBinding activitySelectItemBinding = DataBindingUtil.setContentView( this, R.layout.activity_select_item );
        listView = activitySelectItemBinding.selectItemListView;

        Bundle bundle = getIntent().getExtras();
        //  we will see which option is selected, based on that i will call method...
        if (bundle != null) {
            option = bundle.getString( "option" );
            assert option != null;
            switch (option) {
                case STATE_OPTION:
                    fetchAndSetStateList( activitySelectItemBinding );
                    break;
                case DISTRICT_OPTION:
                    String stateId = bundle.getString( "stateId" );
                    fetchAndSetDistrictList( stateId, activitySelectItemBinding );
                    break;
                case SUBDISTRICT_OPTION:
                    String districtId = bundle.getString( "districtId" );
                    fetchAndSetSubDistrictList( districtId, activitySelectItemBinding );
                    break;
                case VILLAGE_OPTION:
                    String subdistrictId = bundle.getString( "subdistrictid" );
                    fetchAndSetVillageList( subdistrictId, activitySelectItemBinding );
                    break;
            }
        }

    }

    //  will fetch SubVillageList and set it on ListView
    private void fetchAndSetVillageList(String subdistrictId, ActivitySelectItemBinding activitySelectItemBinding) {

        //  for changes...
        NetworkResource.getInstance().getVillagesList().observeForever( villageResponse -> setVillagesOnList( activitySelectItemBinding, villageResponse ) );

        //  fetch data
        NetworkResource.getInstance().getVillages( subdistrictId, this );
    }

    //  will fetch SubdistrictList and set it on ListView
    private void fetchAndSetSubDistrictList(String districtId, ActivitySelectItemBinding activitySelectItemBinding) {

        //  for changes...
        NetworkResource.getInstance().getSubDistrictsList().observeForever( subDistrictResponses -> setSubDistrictOnList( activitySelectItemBinding, subDistrictResponses ) );

        //  fetch data
        NetworkResource.getInstance().getSubDistricts( districtId, this );
    }

    //  will fetch Districts and set it on ListView
    private void fetchAndSetDistrictList(String stateId, ActivitySelectItemBinding activitySelectItemBinding) {

        //  for changes...
        NetworkResource.getInstance().getDistrictsList().observeForever( districtResponses -> setDistrictsOnList( activitySelectItemBinding, districtResponses ) );

        //  fetch data
        NetworkResource.getInstance().getDistricts( stateId, this );
    }

    //  will fetch StateList and set it on ListView
    private void fetchAndSetStateList(ActivitySelectItemBinding activitySelectItemBinding) {
        //  for changes...
        NetworkResource.getInstance().getStatesList().observeForever( statesResponses -> setStatesOnList( activitySelectItemBinding, statesResponses ) );
        //  fetch data
        NetworkResource.getInstance().getStates( this );
    }

    //  set state Data on List
    private void setStatesOnList(ActivitySelectItemBinding activitySelectItemBinding, ArrayList<StatesResponse> statesResponses) {
        stateAdapter = new StateAdapter( this, statesResponses );
        activitySelectItemBinding.selectItemListView.setAdapter( stateAdapter );
    }

    //  set district Data on List
    private void setDistrictsOnList(ActivitySelectItemBinding activitySelectItemBinding, ArrayList<DistrictResponse> districtResponses) {
        districtAdapter = new DistrictAdapter( this, districtResponses );
        activitySelectItemBinding.selectItemListView.setAdapter( districtAdapter );
    }

    //  set sub district Data on List
    private void setSubDistrictOnList(ActivitySelectItemBinding activitySelectItemBinding, ArrayList<SubDistrictResponse> subDistrictResponses) {
        subDistrictAdapter = new SubDistrictAdapter( this, subDistrictResponses );
        activitySelectItemBinding.selectItemListView.setAdapter( subDistrictAdapter );
    }

    //  set village data on List
    private void setVillagesOnList(ActivitySelectItemBinding activitySelectItemBinding, ArrayList<VillageResponse> villageResponses) {
        villageAdapter = new VillageAdapter( this, villageResponses );
        activitySelectItemBinding.selectItemListView.setAdapter( villageAdapter );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.search_view_menu_item, menu );
        MenuItem searchViewItem = menu.findItem( R.id.action_search );
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView( searchViewItem );
        searchViewAndroidActionBar.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        setupSearchQuery(query);
        return true;
    }

    //  which option was selected? state,District,Mandal,Village??
    //  change search according to option selected...
    void setupSearchQuery(String query){
        assert option!=null;
        switch (option){
            case STATE_OPTION:
                setupStateSearch(query);
                break;
            case DISTRICT_OPTION:
                setupDistrictSearch(query);
                break;
            case SUBDISTRICT_OPTION:
                setupMandalSearch(query);
                break;
            case VILLAGE_OPTION:
                setupVillageSearch(query);
                break;
        }
    }

    void setupStateSearch(String query){
        if (TextUtils.isEmpty(query)){
            stateAdapter.filter("");
            listView.clearTextFilter();
        }
        else {
            stateAdapter.filter(query);
        }
    }

    void setupDistrictSearch(String query){
        if (TextUtils.isEmpty(query)){
            districtAdapter.filter("");
            listView.clearTextFilter();
        }
        else {
            districtAdapter.filter(query);
        }
    }

    void setupMandalSearch(String query){
        if (TextUtils.isEmpty(query)){
            subDistrictAdapter.filter("");
            listView.clearTextFilter();
        }
        else {
            subDistrictAdapter.filter(query);
        }
    }

    void setupVillageSearch(String query){
        if (TextUtils.isEmpty(query)){
            villageAdapter.filter("");
            listView.clearTextFilter();
        }
        else {
            villageAdapter.filter(query);
        }
    }

}
