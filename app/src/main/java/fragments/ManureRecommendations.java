package fragments;


import android.os.Bundle;

import androidx.constraintlayout.widget.Group;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ManureRecommendationsBinding;
import network.NetworkResource;
import utility.ImageSetterUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManureRecommendations extends Fragment {


    public ManureRecommendations() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ManureRecommendationsBinding inflate =  DataBindingUtil.inflate(inflater,R.layout.manure_recommendations, container, false);

        NetworkResource.getInstance().getRecommendationSheetData().observe(this, recommendationSheetResponse -> {
            double zn = recommendationSheetResponse.getZincByDay().getDay0();

            ImageSetterUtils.setImageAndText(getContext(),inflate.znImg, inflate.znApplication, zn, "zn");

            if(zn == 0.0){
                inflate.dayZeroZnGroup.setVisibility( View.GONE );
            }
        } );

        return inflate.getRoot();
    }

}
