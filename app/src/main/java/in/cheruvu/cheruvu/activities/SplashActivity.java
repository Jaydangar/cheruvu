package in.cheruvu.cheruvu.activities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.crashlytics.android.answers.Answers;

import java.util.HashMap;
import java.util.Locale;

import in.cheruvu.cheruvu.R;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import pojo.CropData;
import pojo.Farmer;
import response.SoilTestDataResponse;
import utility.IntentUtils;
import utility.PortraitHelper;
import utility.QueryUtils;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private Realm realm;
    private Farmer farmer;

    //Locale
    Locale locale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        Fabric.with( this, new Answers() );

        PortraitHelper.initialize( this );
        realm = Realm.getDefaultInstance();
        setContentView( R.layout.splash_activity );

        HashMap<String, String> map = QueryUtils.getCredentials( realm );
        String farmerId = map.get( "farmerId" );
        farmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();

        if (hasAlreadySignedIn(farmer)) {
            gotoHomeActivity( this);
        }

    }

    public void onNextButtonClick(View view) {
        if (!hasAlreadySignedIn(farmer)) {
            gotoSignInActivity( view );
        }
    }

    private boolean hasAlreadySignedIn(Farmer farmer) {
        boolean isSignedIn = false;
        if (farmer != null) {
            isSignedIn = farmer.getRegistered();
        }
        return isSignedIn;
    }

    private void gotoSignInActivity(View view) {
        IntentUtils.PassIntent( view.getContext(), OTPGenerationActivity.class );
        ((Activity) view.getContext()).finish();
    }

    private void gotoHomeActivity(Context context) {
        IntentUtils.PassIntent( context, HomeScreenActivity.class );
        ((Activity) context).finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
