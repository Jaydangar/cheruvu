package response;

import java.util.ArrayList;
import java.util.List;

import application.ApplicationClass;
import in.cheruvu.cheruvu.R;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BankDetailsResponse extends RealmObject {

	private String farmerReferenceId;
	@PrimaryKey
	private String farmerId;
	private String bankAccountNumber;
	private String ifscCode;
	private String bankAccountHolderName;

	public String getFarmerReferenceId() {
		if (this.farmerReferenceId == null)
			return "";
		else
			return farmerReferenceId;
	}

	public void setFarmerReferenceId(String farmerReferenceId) {
		this.farmerReferenceId = farmerReferenceId;
	}

	public String getFarmerId() {
		if (this.farmerId == null)
			return "";
		else
			return farmerId;
	}

	public void setFarmerId(String farmerId) {
		this.farmerId = farmerId;
	}

	public String getBankAccountNumber() {
		if (this.bankAccountNumber == null)
			return "";
		else
			return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getIfscCode() {
		if (this.ifscCode == null)
			return "";
		else
			return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankAccountHolderName() {
		if (this.bankAccountHolderName == null)
			return "";
		else
			return bankAccountHolderName;
	}

	public void setBankAccountHolderName(String bankAccountHolderName) {
		this.bankAccountHolderName = bankAccountHolderName;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (this.bankAccountNumber.isEmpty()) {
			errors.add( ApplicationClass.getContext().getString(R.string.bank_account_number_cannot_be_empty));
		}
		if (this.ifscCode.isEmpty()) {
			errors.add(ApplicationClass.getContext().getString(R.string.ifsc_cannot_be_empty));
		}
		if (this.bankAccountHolderName.isEmpty()) {
			errors.add(ApplicationClass.getContext().getString(R.string.account_holder_name_cannot_be_empty));
		}
		return errors;
	}

	public static class Constants{
		public static final String FARMER_REFERENCE_ID = "farmerReferenceId";
		public static final String FARMER_ID = "farmerId";
	}
	
}
