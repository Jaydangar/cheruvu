package fragments;


import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import enums.Crop;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentComparativeYieldAnalysisBinding;
import io.realm.Realm;
import io.realm.RealmList;
import network.NetworkResource;
import pojo.CropData;
import pojo.Farmer;
import request.GetComparisonSheetRequest;
import response.YieldResponse;
import utility.QueryUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComparativeYieldAnalysisFragment extends Fragment {

    private Realm realm;

    private static final String TAG = "ComparativeYieldAnalysis";

    private FragmentComparativeYieldAnalysisBinding fragmentComparativeYieldAnalysisBinding;

    public ComparativeYieldAnalysisFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentComparativeYieldAnalysisBinding = DataBindingUtil.inflate( inflater, R.layout.fragment_comparative_yield_analysis, container, false );
        realm = Realm.getDefaultInstance();
        updateComparison();
        updateUI();
        return fragmentComparativeYieldAnalysisBinding.getRoot();
    }

    private void updateUI() {
        NetworkResource.getInstance().getYieldResponseMutableLiveData().observe( this, response -> {
            if (response != null) {
                setValueOnUi( response );
            }
        } );
    }

    private void setValueOnUi(YieldResponse getYieldComparisonResponse) {
        double averageYield = getYieldComparisonResponse.getAverageYield();
        double maxYield = getYieldComparisonResponse.getMaxYield();
        double personalYield = getYieldComparisonResponse.getPersonalYield();

        fragmentComparativeYieldAnalysisBinding.averageProgressBar.setMax( (int) maxYield );
        fragmentComparativeYieldAnalysisBinding.personalProgressBar.setMax( (int) maxYield );
        fragmentComparativeYieldAnalysisBinding.idProgressBarHigh.setMax( (int) maxYield );


        if (personalYield >= maxYield) {
            fragmentComparativeYieldAnalysisBinding.bestCard.setBackgroundColor( getResources().getColor( R.color.how_are_you_doing_selected ) );
        } else if (personalYield < averageYield) {
            fragmentComparativeYieldAnalysisBinding.improveCard.setBackgroundColor( getResources().getColor( R.color.how_are_you_doing_selected ) );
        } else {
            fragmentComparativeYieldAnalysisBinding.averageCard.setBackgroundColor( getResources().getColor( R.color.how_are_you_doing_selected ) );
        }

        if (Build.VERSION.SDK_INT > 23) {
            fragmentComparativeYieldAnalysisBinding.idProgressBarHigh.setProgress( (int) maxYield, true );
            fragmentComparativeYieldAnalysisBinding.averageProgressBar.setProgress( (int) averageYield, true );
            fragmentComparativeYieldAnalysisBinding.personalProgressBar.setProgress( (int) personalYield, true );
        } else {
            fragmentComparativeYieldAnalysisBinding.idProgressBarHigh.setProgress( (int) maxYield );
            fragmentComparativeYieldAnalysisBinding.averageProgressBar.setProgress( (int) averageYield );
            fragmentComparativeYieldAnalysisBinding.personalProgressBar.setProgress( (int) personalYield );
        }

        fragmentComparativeYieldAnalysisBinding.maxYieldValue.setText( (int) maxYield + " " + getResources().getString( R.string.quintal_per_acre ) );
        fragmentComparativeYieldAnalysisBinding.personalYield.setText( (int) personalYield + " " + getResources().getString( R.string.quintal_per_acre ) );
        fragmentComparativeYieldAnalysisBinding.averageYield.setText( (int) averageYield + " " + getResources().getString( R.string.quintal_per_acre ) );

    }

    private void updateComparison() {
        HashMap<String, String> credentials = QueryUtils.getCredentials( realm );
        String farmerId = credentials.get( "farmerId" );
        Farmer farmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();
        RealmList<CropData> farmerCropData = null;
        if (farmer != null) {
            farmerCropData = farmer.getCropData();
            CropData cropData = farmerCropData.get( 0 );
            GetComparisonSheetRequest getComparisonSheetRequest = new GetComparisonSheetRequest();
            getComparisonSheetRequest.setCrop( Crop.valueOf( cropData.getCrop() ) );
            NetworkResource.getInstance().getYieldComparisonSheet( farmer.getAuthToken(), farmer.getId(), getComparisonSheetRequest, getContext() );
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
