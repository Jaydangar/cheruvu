package in.cheruvu.cheruvu.activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.textfield.TextInputEditText;
import com.hbb20.CountryCodePicker;

import java.util.List;
import java.util.Objects;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.GenerateOtpActivityBinding;
import network.NetworkResource;
import request.SendOTPRequest;
import utility.AppSignatureHelper;
import utility.IntentUtils;
import utility.PortraitHelper;

import static com.google.android.gms.auth.api.credentials.CredentialsApi.ACTIVITY_RESULT_NO_HINTS_AVAILABLE;

public class OTPGenerationActivity extends AppCompatActivity {

    private static final String TAG = OTPGenerationActivity.class.getSimpleName();

    private String countryCode, phoneNumber;
    private AppSignatureHelper appSignatureHelper;

    private GoogleApiClient apiClient;
    private static final int RESOLVE_HINT = 5555;

    private CountryCodePicker countryCodePicker;
    private TextInputEditText phoneNumberText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PortraitHelper.initialize(this);

        GenerateOtpActivityBinding generateOtpActivityBinding = DataBindingUtil.setContentView( this, R.layout.generate_otp_activity );

        appSignatureHelper = new AppSignatureHelper(this);
        apiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.CREDENTIALS_API).enableAutoManage(this, connectionResult -> Log.e(TAG, "Client connection failed: " + connectionResult.getErrorMessage())).build();

        countryCodePicker = generateOtpActivityBinding.ccp;
        phoneNumberText = generateOtpActivityBinding.etPhone;

        countryCode = countryCodePicker.getSelectedCountryCodeWithPlus();

        //  on country changed listener
        countryCodePicker.setOnCountryChangeListener(() -> countryCode = countryCodePicker.getSelectedCountryCodeWithPlus());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void generateOTP(View view) {

        phoneNumber = Objects.requireNonNull(phoneNumberText.getText()).toString();
        SendOTPRequest sendOTPRequest = setupRequest();

        List<String> errorList = sendOTPRequest.verify(sendOTPRequest);
        if (errorList.isEmpty()) {
            NetworkResource.getInstance().getOTPCode(view.getContext(), sendOTPRequest, false);
        } else {
            Toast.makeText(view.getContext(), errorList.get(0), Toast.LENGTH_SHORT).show();
        }
        //  go to the Verification Screen with data...
        Bundle bundle = new Bundle();
        bundle.putString( "countryCode", sendOTPRequest.getCountryCode() );
        bundle.putString( "phoneNumber", sendOTPRequest.getPhoneNumber() );
        bundle.putString( "appSignature", sendOTPRequest.getAppSignature() );
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private SendOTPRequest setupRequest() {
        SendOTPRequest sendOtpRequest = new SendOTPRequest();
        sendOtpRequest.setCountryCode(countryCode);
        sendOtpRequest.setPhoneNumber(phoneNumber);
        sendOtpRequest.setAppSignature(appSignatureHelper.getAppSignatures().get(0));
        return sendOtpRequest;
    }

    //  open Phone Number choosing dialog...
    public void chooseNumber(View view) throws IntentSender.SendIntentException {

        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(apiClient, hintRequest);
        startIntentSenderForResult(intent.getIntentSender(),
                RESOLVE_HINT, null, 0, 0, 0);
    }

    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RESOLVE_HINT:
                    Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                    String phoneNumberWithCountryCode = credential.getId();
                    int length = phoneNumberWithCountryCode.length();
                    phoneNumber = phoneNumberWithCountryCode.substring(length - 10, length);
                    countryCode = phoneNumberWithCountryCode.substring(0, length - 10);
                    phoneNumberText.setText(phoneNumber);
                    countryCodePicker.setCountryForPhoneCode(Integer.parseInt(countryCode.substring(1)));
                    break;
                case ACTIVITY_RESULT_NO_HINTS_AVAILABLE:
                    phoneNumberText.setOnClickListener(null);
                    break;
            }
        }


    }
}
