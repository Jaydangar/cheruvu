package adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import in.cheruvu.cheruvu.R;
import io.realm.RealmList;
import pojo.Day;
import pojo.ForecastdayItem;

public class WeatherAdapter extends BaseAdapter {

    private static final String TAG = "WeatherAdapter";

    private RealmList<ForecastdayItem> forecastResponseArrayList;
    private Context context;
    private LayoutInflater inflater;

    static class ViewHolder {
        TextView weatherDay, weatherCondition, temperature;
        AppCompatImageView weatherIcon;
        ImageView temperatureIcon;
    }

    public WeatherAdapter(Context context, RealmList<ForecastdayItem> arrayAdapter) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.forecastResponseArrayList = arrayAdapter;
    }

    @Override
    public int getCount() {
        return forecastResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return forecastResponseArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.weather_list, parent, false);
            viewHolder.weatherDay = convertView.findViewById(R.id.weatherDay);
            viewHolder.weatherIcon = convertView.findViewById(R.id.weatherImage);
            viewHolder.temperatureIcon = convertView.findViewById(R.id.temperatureIcon);
            viewHolder.temperature = convertView.findViewById(R.id.temperature);
            viewHolder.weatherCondition = convertView.findViewById(R.id.weatherType);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //  setting text on items...
        ForecastdayItem listItem = forecastResponseArrayList.get(position);
        if (listItem != null) {
            Day day = listItem.getDay();
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat targetDateFormat = new SimpleDateFormat( "dd, MMM");

            try {
                Date date = simpleDateFormat.parse(listItem.getDate());
                String formattedString = targetDateFormat.format( date );
                calendar.setTime(date);
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                String dayName = getDay(dayOfWeek);
                if (dayName != null) {
                    viewHolder.weatherDay.setText(dayName + " - " + formattedString);
                }

                double Temperature = day.getAvgtempC();
                viewHolder.weatherCondition.setText(day.getCondition().getText());
                viewHolder.temperature.setText((int) Temperature + "\u00b0 C");
                setTemperatureIconAccordingToTemperature(Temperature, viewHolder.temperatureIcon);
                String urlIcon = day.getCondition().getIcon();
                Glide.with(context).load("http:" + urlIcon).into(viewHolder.weatherIcon);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    private String getDay(int day) {
        switch (day) {
            case 1:
                return context.getString(R.string.day_sunday);
            case 2:
                return context.getString(R.string.day_monday);
            case 3:
                return context.getString(R.string.day_tuesday);
            case 4:
                return context.getString(R.string.day_wednesday);
            case 5:
                return context.getString(R.string.day_thursday);
            case 6:
                return context.getString(R.string.day_friday);
            case 7:
                return context.getString(R.string.day_saturday);
            default:
                return null;
        }
    }

    //  sets temperature Icon according to Temperature...
    private void setTemperatureIconAccordingToTemperature(double temperatureValue, ImageView temperatureIcon) {
        if (temperatureValue >= 35) {
            temperatureIcon.setImageResource(R.drawable.ic_high_temp);
        } else {
            temperatureIcon.setImageResource(R.drawable.ic_low_temp);
        }
    }
}
