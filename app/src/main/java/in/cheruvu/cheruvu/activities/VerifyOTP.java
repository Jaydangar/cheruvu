package in.cheruvu.cheruvu.activities;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;

import java.util.List;

import broadcastreceivers.SMSReceiver;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.VerifyOtpActivityBinding;
import network.NetworkResource;
import request.SendOTPRequest;
import request.ValidateOTPRequest;
import utility.PortraitHelper;

public class VerifyOTP extends AppCompatActivity implements SMSReceiver.OTPReceiveListener {

    private static final String TAG = "VerifyOTP";

    private VerifyOtpActivityBinding verifyOtpActivityBinding;

    private TextView timerTextView;
    private TextView resendTextButton;
    public int counter;

    private SMSReceiver smsReceiver;

    private String phoneNumber, countryCode, appSignature;

    private boolean isRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        PortraitHelper.initialize( this );

        verifyOtpActivityBinding = DataBindingUtil.setContentView( this, R.layout.verify_otp_activity );

        // otp resend time set here
        timerTextView = verifyOtpActivityBinding.otpResendTimer;
        resendTextButton = verifyOtpActivityBinding.buttonResendOtp;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            phoneNumber = bundle.getString( "phoneNumber" );
            countryCode = bundle.getString( "countryCode" );

            appSignature = bundle.getString( "appSignature" );

            verifyOtpActivityBinding.textViewSentTO.setText( getResources().getString(R.string.otp_sent_to) + countryCode + " " + phoneNumber );
        }

        new CountDownTimer( 120000, 1000 ) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerTextView.setText(getResources().getString(R.string.seconds_remaining) + millisUntilFinished / 1000 );
                counter++;
            }

            @Override
            public void onFinish() {
                resendTextButton.setVisibility( View.VISIBLE );
            }
        }.start();

        startSMSListener();
    }

    private void verifyOTP() {
        String otp = verifyOtpActivityBinding.otpView.getOTP();

        ValidateOTPRequest validateOTPRequest = new ValidateOTPRequest();
        validateOTPRequest.setValue( otp );
        validateOTPRequest.setCountryCode( countryCode );
        validateOTPRequest.setPhoneNumber( phoneNumber );

        validateOTP( this, validateOTPRequest );
    }

    private void validateOTP(Context context, ValidateOTPRequest validateOTPRequest) {
        List<String> errorList = validateOTPRequest.verify();
        if (errorList.isEmpty()) {
            NetworkResource.getInstance().verifyOTP( context, validateOTPRequest );
        } else {
            Toast.makeText( context, errorList.get( 0 ), Toast.LENGTH_SHORT ).show();
        }
    }

    public void onVerifyBtnClick(View view) {
        verifyOTP();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver( smsReceiver );
    }

    private void startSMSListener() {
        try {
            smsReceiver = new SMSReceiver();
            smsReceiver.setOTPListener( this );

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction( SmsRetriever.SMS_RETRIEVED_ACTION );
            this.registerReceiver( smsReceiver, intentFilter );

            SmsRetrieverClient client = SmsRetriever.getClient( this );
            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener( aVoid -> {
                // API successfully started
            } );
            task.addOnFailureListener( e -> {
                // Fail to start API
            } );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOTPReceived(String otp) {
        verifyOtpActivityBinding.otpView.setOTP( otp );
        verifyOTP();
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onOTPReceivedError(String error) {
        Toast.makeText( this, getResources().getString( R.string.otpReceiveError ), Toast.LENGTH_SHORT ).show();
    }

    public void resendOTP(View view) {
        SendOTPRequest sendOTPRequest = new SendOTPRequest();
        sendOTPRequest.setCountryCode( countryCode );
        sendOTPRequest.setPhoneNumber( phoneNumber );
        sendOTPRequest.setAppSignature( appSignature );

        List<String> errorList = sendOTPRequest.verify( sendOTPRequest );
        if (errorList.isEmpty()) {
            NetworkResource.getInstance().getOTPCode( view.getContext(), sendOTPRequest, isRunning );
        } else {
            Toast.makeText( view.getContext(), errorList.get( 0 ), Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isRunning = false;
    }
}
