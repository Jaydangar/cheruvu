package services;

import pojo.UpdateFarmerRequest;
import request.BankAccountDetailRequest;
import request.CropDataUpdateRequest;
import request.FarmCoordinatesData;
import request.FarmerRequest;
import request.GetComparisonSheetRequest;
import request.PaymentRequest;
import request.PlantingDateRequest;
import request.SendOTPRequest;
import request.ValidateOTPRequest;
import response.BasicResponse;
import response.ForecastResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import singleton.RetrofitInstance;

public class EndPoints {

    private static final String MOBILE_AUTH = "mobileAuthToken";
    private static final String FARMER_ID = "farmerId";
    private static final String CROP_NAME = "cropName";
    private static final String LAND_SURVEY_NO = "landSurveyNo";
    private static final String GET_STATES_URL = "getStates";
    private static final String GET_DISTRICTS_BY_STATE_ID_URL = "getDistrictsByStateId";
    private static final String GET_MANDALS_BY_DISTRICT_ID_URL = "getMandalsByDistrictId";
    private static final String GET_VILLAGES_BY_MANDAL_ID_URL = "getVillagesByMandalId";
    private static final String FARMER_SIGNUP_URL = "farmer/signup";
    private static final String FARMER_PROFILE_UPDATE_URL = "farmer/updateProfile";
    private static final String SEND_OTP_URL = "farmer/login/request/otp";
    private static final String VALIDATE_OTP_URL = "farmer/login/validate/otp";
    private static final String LOGOUT_URL = "farmer/logout";
    private static final String ADD_FARM_COORDINATES_DATA_URL = "farmer/addFarmCoordinatesData";
    private static final String GET_FARM_COORDINATES_DATA_URL = "farmer/getFarmCoordinatesData";
    private static final String GET_SOIL_TEST_DATA_URL = "farmer/getSoilTestData";
    private static final String GET_RECOMMENDATION_SHEET_URL = "farmer/getRecommendationSheet";
    private static final String UPLOAD_FILE_TO_AWS_URL = "farmer/uploadCropImages";
    private static final String DOWNLOAD_FILE_FROM_AWS_URL = "farmer/getCropImages";
    private static final String ADD_NEW_CROP_URL = "farmer/updateCrop";
    private static final String PAYTM_VERIFICATION_URL = "farmer/payment/verify";
    private static final String PAYTM_LOG_URL = "farmer/payment/log";
    private static final String UPDATE_CROP_URL = "farmer/updateCrop";
    private static final String GET_YIELD_COMPARISON = "farmer/get/yield/comparison";
    private static final String GET_INCOME_COMPARISON = "farmer/get/income/comparison";
    private static final String ADD_BANK_DETAILS = "farmer/addBankDetails";
    private static final String GET_BANK_DETAILS = "farmer/getBankDetails";
    private static final String SET_PLANT_DATE = "farmer/setPlantingDate";

    private static final String TAG = EndPoints.class.getSimpleName();
    private final static Retrofit retrofitInstance = RetrofitInstance.getRetrofitInstance();
    private final static Retrofit retrofitWeatherInstance = RetrofitInstance.getRetrofitWeatherInstance();

    // get FarmCoordonates Data
    private interface GetFarmCoordinatesApi {
        @Headers("Content-Type: application/json")
        @GET(GET_FARM_COORDINATES_DATA_URL)
        Call<BasicResponse> getFarmCoordinates(@Header(MOBILE_AUTH) String authToken,
                                               @Header(FARMER_ID) String farmerId);
    }

    //  get Bank Details
    private interface GetBankDetails {
        @Headers("Content-Type: application/json")
        @GET(GET_BANK_DETAILS)
        Call<BasicResponse> getBankDetails(@Header(MOBILE_AUTH) String authToken,
                                               @Header(FARMER_ID) String farmerId);
    }

    //  for updating crops...
    private interface UpdateCropApi {
        @Headers("Content-Type: application/json")
        @PUT(UPDATE_CROP_URL)
        Call<BasicResponse> updateCrop(@Header(MOBILE_AUTH) String authToken,
                                       @Header(FARMER_ID) String farmerId, @Body CropDataUpdateRequest cropDataUpdateRequest);
    }

    //  API EndPoint will give 5 Day Forecast...
    private interface GetForecast {
        @GET("forecast.json")
        Call<ForecastResponse> getForecastWeatherDetails(@Query("key") String appID, @Query("days") String numberOfDays, @Query("q") String location);
    }

    private interface SendOtpApi {
        @POST(SEND_OTP_URL)
        Call<BasicResponse> sendOtpRequest(@Body SendOTPRequest sendOtpRequest);
    }

    private interface LogPayment {
        @Headers("Content-Type: application/json")
        @POST(PAYTM_LOG_URL)
        Call<BasicResponse> logPaymentRequest(@Header(MOBILE_AUTH) String authToken,
                                              @Header(FARMER_ID) String farmerId, @Body PaymentRequest paymentRequest);
    }

    private interface ValidateOtpApi {
        @Headers("Content-Type: application/json")
        @POST(VALIDATE_OTP_URL)
        Call<BasicResponse> validateOtp(
                @Body ValidateOTPRequest validateOTPRequest
        );
    }

    // interfaces for apis
    private interface GetStatesApi {
        @Headers("Content-Type: application/json")
        @GET(GET_STATES_URL)
        Call<BasicResponse> getAllStates();
    }

    private interface GetDistrictsByStateIdApi {
        @Headers("Content-Type: application/json")
        @GET(GET_DISTRICTS_BY_STATE_ID_URL)
        Call<BasicResponse> getDistrictsByStateId(
                @Query("stateId") String stateId
        );
    }

    private interface GetMandalsByDistrictIdApi {
        @Headers("Content-Type: application/json")
        @GET(GET_MANDALS_BY_DISTRICT_ID_URL)
        Call<BasicResponse> getMandalsByDistrictId(
                @Query("districtId") String districtId
        );
    }

    private interface GetVillagesByMandalIdApi {
        @Headers("Content-Type: application/json")
        @GET(GET_VILLAGES_BY_MANDAL_ID_URL)
        Call<BasicResponse> getVillagesByMandalId(
                @Query("mandalId") String mandalId
        );
    }

    private interface FarmerSignUpApi {
        @Headers("Content-Type: application/json")
        @POST(FARMER_SIGNUP_URL)
        Call<BasicResponse> signUp(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Body FarmerRequest farmerRequest
        );
    }

    private interface IncomeComparisonAPI {
        @Headers("Content-Type: application/json")
        @POST(GET_INCOME_COMPARISON)
        Call<BasicResponse> incomeComparisonSheet(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Body GetComparisonSheetRequest getComparisonSheetRequest
        );
    }

    private interface YieldComparisonAPI {
        @Headers("Content-Type: application/json")
        @POST(GET_YIELD_COMPARISON)
        Call<BasicResponse> yieldComparisonSheet(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Body GetComparisonSheetRequest getComparisonSheetRequest
        );
    }

    private interface FarmerPaymentVerifyApi {
        @Headers("Content-Type: application/json")
        @GET(PAYTM_VERIFICATION_URL)
        Call<BasicResponse> verifyPayment(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId
        );
    }

    private interface AddFarmCoordinatesDataApi {
        @Headers("Content-Type: application/json")
        @POST(ADD_FARM_COORDINATES_DATA_URL)
        Call<BasicResponse> addFarmCoordinatesData(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Body FarmCoordinatesData farmCoordinatesData
        );
    }

    // get FarmCoordonates Data
    private interface GetRecommendation {
        @Headers("Content-Type: application/json")
        @GET(GET_RECOMMENDATION_SHEET_URL)
        Call<BasicResponse> getRecommendationSheet(@Header(MOBILE_AUTH) String authToken,
                                                   @Header(FARMER_ID) String farmerId);
    }

    private interface FarmerProfileUpdateApi {
        @Headers("Content-Type: application/json")
        @POST(FARMER_PROFILE_UPDATE_URL)
        Call<BasicResponse> profileUpdate(@Header(MOBILE_AUTH) String authToken,
                                          @Header(FARMER_ID) String farmerId,
                                          @Body UpdateFarmerRequest updateFarmerRequest);
    }

    private interface SetPlantDateAPI {
        @Headers("Content-Type: application/json")
        @POST(SET_PLANT_DATE)
        Call<BasicResponse> setPlantDate(@Header(MOBILE_AUTH) String authToken,
                                          @Header(FARMER_ID) String farmerId,
                                          @Body PlantingDateRequest plantingDateRequest);
    }

    private interface LogoutApi {
        @Headers("Content-Type: application/json")
        @POST(LOGOUT_URL)
        Call<BasicResponse> logout(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId
        );
    }

    private interface GetSoilTestDataApi {
        @Headers("Content-Type: application/json")
        @GET(GET_SOIL_TEST_DATA_URL)
        Call<BasicResponse> getSoilTestData(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Query(LAND_SURVEY_NO) String landSurveyNo
        );
    }

    private interface AddBankDetailsAPI {
        @Headers("Content-Type: application/json")
        @POST(ADD_BANK_DETAILS)
        Call<BasicResponse> addBankDetails(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Body BankAccountDetailRequest detailRequest
        );
    }

    //  this api will add/update bank details
    public static void addBankDetails(String authToken, String farmerId, BankAccountDetailRequest bankAccountDetailRequest, Callback<BasicResponse> callback) {
        AddBankDetailsAPI addBankDetailsAPI = retrofitInstance.create( AddBankDetailsAPI.class );
        Call<BasicResponse> call = addBankDetailsAPI.addBankDetails( authToken, farmerId, bankAccountDetailRequest );
        call.enqueue( callback );
    }

    public static void getBankDetails(String authToken, String farmerId, Callback<BasicResponse> callback) {
        GetBankDetails details = retrofitInstance.create( GetBankDetails.class );
        Call<BasicResponse> call = details.getBankDetails( authToken, farmerId);
        call.enqueue( callback );
    }

    public static void getIncomeComparisonSheet(String authToken, String farmerId, GetComparisonSheetRequest getComparisonSheetRequest, Callback<BasicResponse> callback) {
        IncomeComparisonAPI incomeComparisonAPI = retrofitInstance.create( IncomeComparisonAPI.class );
        Call<BasicResponse> call = incomeComparisonAPI.incomeComparisonSheet( authToken, farmerId, getComparisonSheetRequest );
        call.enqueue( callback );
    }

    public static void getYeildComparisonSheet(String authToken, String farmerId, GetComparisonSheetRequest getComparisonSheetRequest, Callback<BasicResponse> callback) {
        YieldComparisonAPI yieldComparisonAPI = retrofitInstance.create( YieldComparisonAPI.class );
        Call<BasicResponse> call = yieldComparisonAPI.yieldComparisonSheet( authToken, farmerId, getComparisonSheetRequest );
        call.enqueue( callback );
    }

    public static void updateCrop(String authToken, String farmerId, CropDataUpdateRequest cropDataUpdateRequest, Callback<BasicResponse> callback) {
        UpdateCropApi updateCropApi = retrofitInstance.create( UpdateCropApi.class );
        Call<BasicResponse> call = updateCropApi.updateCrop( authToken, farmerId, cropDataUpdateRequest );
        call.enqueue( callback );
    }

    public static void getFarmCoordinates(String authToken, String farmerId, Callback<BasicResponse> callback) {
        GetFarmCoordinatesApi getFarmCoordinatesApi = retrofitInstance.create( GetFarmCoordinatesApi.class );
        Call<BasicResponse> call = getFarmCoordinatesApi.getFarmCoordinates( authToken, farmerId );
        call.enqueue( callback );
    }

    public static void getWeatherForecast(String appID, String numberOfDays, double latitude, double longitude, Callback<ForecastResponse> callback) {
        GetForecast getForecast = retrofitWeatherInstance.create( GetForecast.class );
        Call<ForecastResponse> call = getForecast.getForecastWeatherDetails( appID, numberOfDays,  String.valueOf( latitude ) + "," + String.valueOf( longitude ) );
        call.enqueue( callback );
    }

    public static void logPayment(String authToken, String farmerId, PaymentRequest paymentRequest, Callback<BasicResponse> callback) {
        LogPayment logPayment = retrofitInstance.create( LogPayment.class );
        Call<BasicResponse> call = logPayment.logPaymentRequest( authToken, farmerId, paymentRequest );
        call.enqueue( callback );
    }

    public static void sendOtp(SendOTPRequest sendOtpRequest, Callback<BasicResponse> callback) {
        SendOtpApi sendOtpApi = retrofitInstance.create( SendOtpApi.class );
        Call<BasicResponse> call = sendOtpApi.sendOtpRequest( sendOtpRequest );
        call.enqueue( callback );
    }

    public static void getPaymentDetails(String authToken, String farmerId, Callback<BasicResponse> callback) {
        FarmerPaymentVerifyApi farmerPaymentVerifyApi = retrofitInstance.create( FarmerPaymentVerifyApi.class );
        Call<BasicResponse> call = farmerPaymentVerifyApi.verifyPayment( authToken, farmerId );
        call.enqueue( callback );
    }

    public static void validateOtp(ValidateOTPRequest validateOTPRequest, Callback<BasicResponse> callback) {
        ValidateOtpApi validateOtpApi = retrofitInstance.create( ValidateOtpApi.class );
        Call<BasicResponse> call = validateOtpApi.validateOtp( validateOTPRequest );
        call.enqueue( callback );
    }

    // this function will give return All the States...
    public static void getStates(Callback<BasicResponse> callback) {
        GetStatesApi getStatesApi = retrofitInstance.create( GetStatesApi.class );
        Call<BasicResponse> call = getStatesApi.getAllStates();
        call.enqueue( callback );
    }

    // this function will give return All the States...
    public static void getRecommendation(String authToken, String farmerId, Callback<BasicResponse> callback) {
        GetRecommendation getRecommendation = retrofitInstance.create( GetRecommendation.class );
        Call<BasicResponse> call = getRecommendation.getRecommendationSheet( authToken, farmerId );
        call.enqueue( callback );
    }

    public static void getDistrictsByStateId(String stateId, Callback<BasicResponse> callback) {
        GetDistrictsByStateIdApi getDistrictsByStateIdApi = retrofitInstance.create( GetDistrictsByStateIdApi.class );
        Call<BasicResponse> call = getDistrictsByStateIdApi.getDistrictsByStateId( stateId );
        call.enqueue( callback );
    }

    public static void getMandalsByDistrictId(String districtId, Callback<BasicResponse> callback) {
        GetMandalsByDistrictIdApi getMandalsByDistrictId = retrofitInstance.create( GetMandalsByDistrictIdApi.class );
        Call<BasicResponse> call = getMandalsByDistrictId.getMandalsByDistrictId( districtId );
        call.enqueue( callback );
    }

    public static void getVillagesByMandalId(String mandalId, Callback<BasicResponse> callback) {
        GetVillagesByMandalIdApi getVillagesByMandalIdApi = retrofitInstance.create( GetVillagesByMandalIdApi.class );
        Call<BasicResponse> call = getVillagesByMandalIdApi.getVillagesByMandalId( mandalId );
        call.enqueue( callback );
    }

    public static void farmerSignUp(String authToken, String farmerId, FarmerRequest farmerRequest, Callback<BasicResponse> callback) {
        FarmerSignUpApi farmerSignUpApi = retrofitInstance.create( FarmerSignUpApi.class );
        Call<BasicResponse> call = farmerSignUpApi.signUp( authToken, farmerId, farmerRequest );
        call.enqueue( callback );
    }

    public static void addFarmCoordinatesData(String authToken, String farmerId, FarmCoordinatesData farmCoordinatesData, Callback<BasicResponse> callback) {
        AddFarmCoordinatesDataApi addFarmCoordinatesDataApi = retrofitInstance.create( AddFarmCoordinatesDataApi.class );
        Call<BasicResponse> call = addFarmCoordinatesDataApi.addFarmCoordinatesData( authToken, farmerId, farmCoordinatesData );
        call.enqueue( callback );
    }

    public static void farmerProfileUpdate(String authToken, String farmerId, UpdateFarmerRequest updateFarmerRequest, Callback<BasicResponse> callback) {
        FarmerProfileUpdateApi farmerProfileUpdateApi = retrofitInstance.create( FarmerProfileUpdateApi.class );
        Call<BasicResponse> call = farmerProfileUpdateApi.profileUpdate( authToken, farmerId, updateFarmerRequest );
        call.enqueue( callback );
    }

    public static void logout(String authToken, String farmerId, Callback<BasicResponse> callback) {
        LogoutApi logoutApi = retrofitInstance.create( LogoutApi.class );
        Call<BasicResponse> call = logoutApi.logout( authToken, farmerId );
        call.enqueue( callback );
    }

    public static void getSoilTestData(String authToken, String farmerId, String landSurveyNo, Callback<BasicResponse> callback) {
        GetSoilTestDataApi getSoilTestDataApi = retrofitInstance.create( GetSoilTestDataApi.class );
        Call<BasicResponse> call = getSoilTestDataApi.getSoilTestData( authToken, farmerId, landSurveyNo );
        call.enqueue( callback );
    }

    public static void setPlantDate(String authToken, String farmerId, PlantingDateRequest plantingDateRequest, Callback<BasicResponse> callback) {
        SetPlantDateAPI setPlantDateAPI = retrofitInstance.create( SetPlantDateAPI.class );
        Call<BasicResponse> call = setPlantDateAPI.setPlantDate( authToken, farmerId, plantingDateRequest);
        call.enqueue( callback );
    }


/*    public interface UploadCropImageApi {
        @POST(UPLOAD_FILE_TO_AWS_URL)
        Call<BasicResponse> uploadCropImage(
                @Header(MOBILE_AUTH) String authToken,
                @Header(FARMER_ID) String farmerId,
                @Body RequestBody requestBody);
    }

    public interface DownloadCropImageAPI {
        @Headers("Content-Type: application/json")
        @GET(DOWNLOAD_FILE_FROM_AWS_URL)
        Call<BasicResponse> downloadCropImage(@Header(MOBILE_AUTH) String authToken,
                                              @Header(FARMER_ID) String farmerId,
                                              @Header(CROP_NAME) String cropName);
    }

    public static void uploadCropImage(String authToken, String farmerId, RequestBody requestBody, Callback<BasicResponse> callback) {
        UploadCropImageApi uploadCropImageApi = retrofit.create(UploadCropImageApi.class);
        Call<BasicResponse> call = uploadCropImageApi.uploadCropImage(authToken, farmerId, requestBody);
        call.enqueue(callback);
    }

    public static void downloadCropImage(String authToken, String farmerId, String cropName, Callback<BasicResponse> callback) {
        DownloadCropImageAPI downloadCropImageAPI = retrofit.create(DownloadCropImageAPI.class);
        Call<BasicResponse> call = downloadCropImageAPI.downloadCropImage(authToken, farmerId, cropName);
        call.enqueue(callback);
    }*/
}