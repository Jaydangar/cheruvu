package utility;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import in.cheruvu.cheruvu.R;
import listeners.OnAlertButtonClickListener;

//  General class for showing Alert Dialogs and Toast Messages...
public class AlertUtils {

    private static AlertUtils alertUtilsInstance;

    //  To Get Which Alert Button is pressed...
    private OnAlertButtonClickListener onAlertButtonClickListener;

    public AlertUtils(OnAlertButtonClickListener onAlertButtonClickListener) {
        this.onAlertButtonClickListener = onAlertButtonClickListener;
    }

    //  Showing Toast Messages...
    public static void showToast(Context ctx, String message, int duration) {
        Toast.makeText( ctx, message, duration ).show();
    }

    public static synchronized AlertUtils getInstance(OnAlertButtonClickListener onAlertButtonClickListener) {
        if (alertUtilsInstance == null) {
            alertUtilsInstance = new AlertUtils( onAlertButtonClickListener );
        }
        return alertUtilsInstance;
    }

    //  Shows the Alert Dialog with one button (Only Positive Button)...
    public void showDialogWithOneBtn(Context ctx, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( new ContextWrapper( ctx ), R.style.dialogTheme );
        alertDialogBuilder.setMessage( message );
        alertDialogBuilder.setPositiveButton( ctx.getString(R.string.okay), onClickListener );
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    //  Shows the Alert Dialog with Two Buttons to show positive and negative Buttons and Returns Button Pressed Value.
    public void showDialogWithPositiveAndNegativeBtn(Context context, String Title, String Message, String positiveButtonText, String negativeButtonText) {
        AlertDialog.Builder builder = new AlertDialog.Builder( new ContextWrapper( context ), R.style.dialogTheme );
        builder.setTitle( Title );
        builder.setMessage( Message );
        builder.setPositiveButton( positiveButtonText, (dialogInterface, i) -> onAlertButtonClickListener.OnButtonClick( i ) );
        builder.setNegativeButton( negativeButtonText, (dialogInterface, i) -> {
            onAlertButtonClickListener.OnButtonClick( i );
            dialogInterface.dismiss();
        } );

        builder.show();
    }

}