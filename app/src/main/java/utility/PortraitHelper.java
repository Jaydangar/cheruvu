package utility;

import android.app.Activity;
import android.content.pm.ActivityInfo;

//  class is used for making all activities in portrait mode, just call this in onCreate on every Activity...
public class PortraitHelper {
    public static void initialize(Activity activity) {
        //  Apply Orientation for the specific activity...
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}
