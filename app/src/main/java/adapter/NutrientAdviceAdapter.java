package adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import fragments.FertilizerRecommendationDayFortyFive;
import fragments.FertilizerRecommendationDayNinety;
import fragments.FertilizerRecommendationDaySixty;
import fragments.FertilizerRecommendationDayThirty;
import fragments.ManureRecommendations;

public class NutrientAdviceAdapter extends FragmentStatePagerAdapter {

    public NutrientAdviceAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ManureRecommendations();
            case 1:
                return new FertilizerRecommendationDayThirty();
            case 2:
                return new FertilizerRecommendationDayFortyFive();
            case 3:
                return new FertilizerRecommendationDaySixty();
            case 4:
                return new FertilizerRecommendationDayNinety();
            default:
                return new ManureRecommendations();
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
