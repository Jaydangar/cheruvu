package in.cheruvu.cheruvu.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivityAgreementBinding;
import utility.ChangeLanguageUtils;
import utility.IntentUtils;
import utility.SharedPreferencesUtils;

public class AgreementActivity extends AppCompatActivity {

    private SharedPreferencesUtils sharedPreferencesUtils;
    private PDFView pdfView;

    private static final String TAG = "AgreementActivity";

    //  Update Request code
    private static final int UPDATE_REQUEST = 998;

    private AppUpdateManager appUpdateManager;
    private boolean isUpdateAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        ActivityAgreementBinding binding = DataBindingUtil.setContentView( this, R.layout.activity_agreement );
        pdfView = binding.pdfView;

        appUpdateManager = AppUpdateManagerFactory.create( this );
        checkUpdateIfAvailable();
        if (isUpdateAvailable) {
            startUpdate();
        } else {
            sharedPreferencesUtils = SharedPreferencesUtils.getInstance( this );
            String language = sharedPreferencesUtils.getLanguagePref( "language" );
            String uriPath;
            if (language.equals( "te" )) {
                ChangeLanguageUtils.setText( "te" );
                uriPath = "";
            } else {
                ChangeLanguageUtils.setText( "en" );
                uriPath = "consent_en.pdf";
            }

            if (sharedPreferencesUtils.getConcenmentPref( "concenment" )) {
                IntentUtils.PassIntent( this, SplashActivity.class );
                finish();
            } else {
                loadPDF( uriPath );
            }

            binding.agreeBtn.setText( getString( R.string.agree ) );
            binding.disagreeBtn.setText( getString( R.string.disagree ) );
        }
    }

    private void startUpdate() {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                    appUpdateManager.getAppUpdateInfo().getResult(),
                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    UPDATE_REQUEST );
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void checkUpdateIfAvailable() {
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener( appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                isUpdateAvailable = true;
            }
        } );
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  check if update is running in the background...
        checkUpdateStatus();
    }

    private void checkUpdateStatus() {
        // Checks that the update is not stalled during 'onResume()'.
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                startUpdate();
                            }
                        } );
    }

    private void loadPDF(String path) {
        pdfView.fromAsset( path )
                .pages( 0, 2, 1, 3, 3, 3 ) // all pages are displayed by default
                .enableSwipe( true ) // allows to block changing pages using swipe
                .swipeHorizontal( false )
                .enableDoubletap( true )
                .defaultPage( 0 )
                .scrollHandle( null )
                .enableAntialiasing( true ) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing( 0 )
                .load();
    }

    public void disAgreed(View view) {
        ((Activity) (view.getContext())).finish();
    }

    public void Agreed(View view) {
        sharedPreferencesUtils.setConcenmentPref( "concenment", true );
        IntentUtils.PassIntent( this, SplashActivity.class );
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == UPDATE_REQUEST) {
            if (resultCode != RESULT_OK) {
                // If the update is cancelled or fails,
                // you can request to start the update again.
                startUpdate();
            }
        }
    }
}
