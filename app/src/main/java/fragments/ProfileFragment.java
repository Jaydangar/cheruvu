package fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.HashMap;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.BankDetailsActivity;
import in.cheruvu.cheruvu.activities.CropDetails;
import in.cheruvu.cheruvu.activities.LanguagePicker;
import in.cheruvu.cheruvu.activities.PlotActivity;
import in.cheruvu.cheruvu.activities.Register;
import in.cheruvu.cheruvu.databinding.FragmentProfileBinding;
import io.realm.Realm;
import io.realm.RealmList;
import network.NetworkResource;
import pojo.CropData;
import pojo.Farmer;
import utility.CustomDialogUtils;
import utility.IntentUtils;
import utility.QueryUtils;


public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ProfileFragment";

    private Realm realm;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        realm = Realm.getDefaultInstance();

        // Inflate the layout for this fragment
        FragmentProfileBinding fragmentProfileBinding = DataBindingUtil.inflate( inflater, R.layout.fragment_profile, container, false );
        fragmentProfileBinding.personalDetails.setOnClickListener( this );
        fragmentProfileBinding.cropDetails.setOnClickListener( this );
        fragmentProfileBinding.plotDetails.setOnClickListener( this );
        fragmentProfileBinding.btnLanguage.setOnClickListener( this );
        fragmentProfileBinding.bankDetails.setOnClickListener( this );
        fragmentProfileBinding.logoutBtn.setOnClickListener( this );
        fragmentProfileBinding.editPlantDate.setOnClickListener( this );

        HashMap<String, String> credentials = QueryUtils.getCredentials( realm );
        Farmer farmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, credentials.get( "farmerId" ) ).findFirst();
        assert farmer != null;
        fragmentProfileBinding.personName.setText( farmer.getFirstName() );
        fragmentProfileBinding.personHouseNumber.setText( farmer.getAddress() );

        RealmList<CropData> cropDataRealmList = farmer.getCropData();
        CropData cropData = cropDataRealmList.get( 0 );
        if (cropData != null) {
            fragmentProfileBinding.cropName.setText( cropData.getCropName() );
            fragmentProfileBinding.acres.setText( getResources().getString( R.string.areaUnit ) + " : " + cropData.getCropAcres() );
        }

        TextDrawable userNameDrawable = TextDrawable.builder().buildRoundRect( String.valueOf( farmer.getFirstName().charAt( 0 ) ), getResources().getColor( R.color.colorPrimary ), 10 );
        fragmentProfileBinding.usernameTextImage.setImageDrawable( userNameDrawable );

        return fragmentProfileBinding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.personalDetails:
                bundle.putString( "fragmentMsg", "personalDetails" );
                IntentUtils.PassIntentWithBundle( getActivity(), Register.class, bundle );
                break;
            case R.id.cropDetails:
                bundle.putString( "fragmentMsg", "cropDetails" );
                IntentUtils.PassIntentWithBundle( getActivity(), CropDetails.class, bundle );
                break;
            case R.id.plotDetails:
                bundle.putString( "fragmentMsg", "plotDetails" );
                IntentUtils.PassIntentWithBundle( getActivity(), PlotActivity.class, bundle );
                break;
            case R.id.btnLanguage:
                IntentUtils.PassIntent( getActivity(), LanguagePicker.class );
                break;
            case R.id.bankDetails:
                IntentUtils.PassIntent( getActivity(), BankDetailsActivity.class );
                break;
            case R.id.editPlantDate:
                // Initialize a new date picker dialog fragment
                DialogFragment dFragment = new DatePickerFragment();
                dFragment.show( getFragmentManager(), "Date Picker" );
                break;
            case R.id.logoutBtn:
                HashMap<String, String> credentials = QueryUtils.getCredentials( realm );
                CustomDialogUtils customDialogUtils = new CustomDialogUtils( getContext() );
                customDialogUtils.setAlertTitle( getResources().getString( R.string.sign_out ) );
                customDialogUtils.setAlertDesciption( getResources().getString( R.string.sign_out_description ) );
                customDialogUtils.setLeftButtonText( getResources().getString( R.string.button_yes ) );
                customDialogUtils.setRightButtonText( getResources().getString( R.string.button_no ) );
                customDialogUtils.show();
                customDialogUtils.setOnActionListener( viewId -> {
                    switch (viewId.getId()) {
                        case R.id.btn_left:
                            String farmerId = credentials.get( "farmerId" );
                            String authToken = credentials.get( "authToken" );
                            NetworkResource.getInstance().logout( authToken, farmerId, v.getContext() );
                            customDialogUtils.dismiss();
                            break;
                        case R.id.btn_right:
                            customDialogUtils.dismiss();
                            break;
                    }
                } );
        }
    }


}
