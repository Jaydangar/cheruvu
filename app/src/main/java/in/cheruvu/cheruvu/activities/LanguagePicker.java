package in.cheruvu.cheruvu.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import in.cheruvu.cheruvu.R;
import utility.ChangeLanguageUtils;
import utility.IntentUtils;
import utility.PortraitHelper;
import utility.SharedPreferencesUtils;

public class LanguagePicker extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    private static final String TAG = LanguagePicker.class.getSimpleName();

    private ListView LanguageList;

    // Adapter for Languages
    private ArrayAdapter<String> languageAdapter;

    //  All Languages List...
    private String[] Languages = {"తెలుగు", "English"};

    //  TypeFace For Languages...
    private Typeface TeluguTypeface, EnglishTypeface;

    //  Select Button
    private AppCompatButton Language_Selected;

    //Get Utils
    SharedPreferencesUtils localStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_language_picker );

        localStorage = SharedPreferencesUtils.getInstance( this );

        //  For Portraint Screen Orientation...
        PortraitHelper.initialize( this );

        init();

    }

    private void init() {
        LanguageList = findViewById( R.id.Language_List );
        Language_Selected = findViewById( R.id.Language_Selected );

        languageAdapter = new ArrayAdapter<>( this, android.R.layout.select_dialog_singlechoice, Languages );
        LanguageList.setAdapter( languageAdapter );

        /*
         * Initialize the fonts.
         */
//        EnglishTypeface = Typeface.createFromAsset( getAssets(), "fonts/varelaround_regular.ttf" );
//        TeluguTypeface = Typeface.createFromAsset( getAssets(), "fonts/mandali_regular.ttf" );

        LanguageList.setOnItemClickListener( this );
        Language_Selected.setOnClickListener( this );
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                ChangeLanguageUtils.setText( "te" );
                localStorage.setLanguagePref( "language", "te" );
                break;
            case 1:
                ChangeLanguageUtils.setText( "en" );
                localStorage.setLanguagePref( "language", "en" );
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Language_Selected:
                IntentUtils.PassIntent( getApplicationContext(), HomeScreenActivity.class );
                finish();
                break;
        }
    }
}
