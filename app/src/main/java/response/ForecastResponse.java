package response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pojo.Current;
import pojo.Forecast;
import pojo.Location;
import utility.ConstantUtils;

public class ForecastResponse extends RealmObject {

	@PrimaryKey
	private String forecastWeatherId;

	public ForecastResponse() {
		forecastWeatherId = ConstantUtils.getForecastId();
	}

	@SerializedName("current")
	private Current current;

	@SerializedName("location")
	private Location location;

	@SerializedName("forecast")
	private Forecast forecast;

	public void setCurrent(Current current){
		this.current = current;
	}

	public Current getCurrent(){
		return current;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setForecast(Forecast forecast){
		this.forecast = forecast;
	}

	public Forecast getForecast(){
		return forecast;
	}

	@Override
 	public String toString(){
		return 
			"ForecastResponse{" + 
			"current = '" + current + '\'' + 
			",location = '" + location + '\'' + 
			",forecast = '" + forecast + '\'' + 
			"}";
		}

	public static class Constants {
		public static final String ID = "forecastWeatherId";
	}
}