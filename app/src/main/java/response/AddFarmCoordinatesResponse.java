package response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pojo.LatLng;
import utility.ConstantUtils;

public class AddFarmCoordinatesResponse extends RealmObject {

    @PrimaryKey
    private String farmCoordinateId;

    public AddFarmCoordinatesResponse() {
        farmCoordinateId = ConstantUtils.getFarmCoordinatesId();
    }

    @SerializedName("path")
    private RealmList<LatLng> path;

    @SerializedName("lastUpdated")
    private long lastUpdated;

    private String farmerId;

    @SerializedName("farmerReferenceId")
    private String farmerReferenceId;

    @SerializedName("creationTime")
    private long creationTime;

    @SerializedName("state")
    private String state;

    private boolean isPlotAdded;

    public RealmList<LatLng> getPath() {
        return path;
    }

    public void setPath(RealmList<LatLng> path) {
        this.path = path;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getFarmerId() {
        if (this.farmerId == null)
            return "";
        else
            return farmerId;

    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFarmerReferenceId() {
        if (this.farmerReferenceId == null)
            return "";
        else
            return farmerReferenceId;

    }

    public void setFarmerReferenceId(String farmerReferenceId) {
        this.farmerReferenceId = farmerReferenceId;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public String getState() {
        if (this.state == null)
            return "";
        else
            return state;

    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isPlotAdded() {
        return isPlotAdded;
    }

    public void setPlotAdded(boolean plotAdded) {
        isPlotAdded = plotAdded;
    }

    public static class Constants {
        public static final String FARMER_ID = "farmerId";
        public static final String FARM_COORDINATES = "path";
        public static final String FARMCOORDINATES_ID = "farmCoordinateId";
    }
}