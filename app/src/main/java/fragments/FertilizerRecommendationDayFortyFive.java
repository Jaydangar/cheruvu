package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentFertilizerRecommendationDayFortyFiveBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class FertilizerRecommendationDayFortyFive extends Fragment {


    public FertilizerRecommendationDayFortyFive() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentFertilizerRecommendationDayFortyFiveBinding recommendationDayFortyFiveBinding = DataBindingUtil.inflate( inflater, R.layout.fragment_fertilizer_recommendation_day_forty_five, container, false );
        return recommendationDayFortyFiveBinding.getRoot();
    }

}
