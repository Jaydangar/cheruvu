package response;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pojo.Farmer;
import utility.ConstantUtils;

public class ValidateOTPResponse extends RealmObject {

    @PrimaryKey
    private String id;
    private Farmer farmer;
    private String userImage;

    public ValidateOTPResponse(){
        id = ConstantUtils.getValidationId();
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public static class Constants {
        public static final String ID = "id";
    }
}
