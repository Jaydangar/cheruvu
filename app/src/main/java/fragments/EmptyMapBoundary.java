package fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import in.cheruvu.cheruvu.R;

public class EmptyMapBoundary extends Fragment {


    public EmptyMapBoundary() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_empty_map_boundary, container, false);
        Button plotBoundaryButton=(Button)view.findViewById(R.id.plotMap);
        plotBoundaryButton.setOnClickListener( v -> replaceFragment(new WeatherFragment()) );
        return view;
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.main_fragment_placeholder, fragment);
        ft.commit();
    }

}
