package in.cheruvu.cheruvu.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import in.cheruvu.cheruvu.R;
import utility.PortraitHelper;

public class NetworkErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        PortraitHelper.initialize( this );

        DataBindingUtil.setContentView(this, R.layout.activity_network_error);
    }

    public void onRetryButtonClick(View view) {
        ((Activity) (view.getContext())).finish();
    }
}
