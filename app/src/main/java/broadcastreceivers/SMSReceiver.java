package broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.cheruvu.cheruvu.R;

public class SMSReceiver extends BroadcastReceiver {

    private static final String TAG = "SMSReceiver";

    private OTPReceiveListener otpListener;

    /**
     * @param otpListener
     */
    public void setOTPListener(OTPReceiveListener otpListener) {
        this.otpListener = otpListener;
    }

    /**
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals( intent.getAction() )) {
            Bundle extras = intent.getExtras();
            Status status = null;
            if (extras != null) {
                status = (Status) extras.get( SmsRetriever.EXTRA_STATUS );
                if (status != null) {
                    switch (status.getStatusCode()) {
                        case CommonStatusCodes.SUCCESS:

                            //This is the full message
                            String message = (String) extras.get( SmsRetriever.EXTRA_SMS_MESSAGE );

                            //Extract the OTP code and send to the listener
                            if (otpListener != null) {
                                String otp = extractOTP( message );
                                if(!otp.isEmpty()){
                                    otpListener.onOTPReceived( otp );
                                }
                            }
                            break;
                        case CommonStatusCodes.TIMEOUT:
                            // Waiting for SMS timed out (5 minutes)
                            if (otpListener != null) {
                                otpListener.onOTPTimeOut();
                            }
                            break;
                        case CommonStatusCodes.API_NOT_CONNECTED:
                            if (otpListener != null) {
                                otpListener.onOTPReceivedError( context.getString(R.string.api_not_connected) );
                            }
                            break;
                        case CommonStatusCodes.NETWORK_ERROR:
                            if (otpListener != null) {
                                otpListener.onOTPReceivedError( context.getString(R.string.network_error) );
                            }
                            break;
                        case CommonStatusCodes.ERROR:
                            if (otpListener != null) {
                                otpListener.onOTPReceivedError( context.getString(R.string.something_went_wrong) );
                            }
                            break;

                    }
                }
            }
        }
    }

    private String extractOTP(String message) {
        Pattern p = Pattern.compile( "(\\d{6})" );
        Matcher m = p.matcher( message );
        if (m.find()) {
            return m.group( 0 );
        } else {
            return "";
        }
    }

    public interface OTPReceiveListener {
        void onOTPReceived(String otp);

        void onOTPTimeOut();

        void onOTPReceivedError(String error);
    }
}