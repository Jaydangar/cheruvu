package fragments;


import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import enums.Crop;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentComparativeIncomeAnalysisBinding;
import io.realm.Realm;
import io.realm.RealmList;
import network.NetworkResource;
import pojo.CropData;
import pojo.Farmer;
import request.GetComparisonSheetRequest;
import response.IncomeResponse;
import utility.QueryUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComparativeIncomeAnalysisFragment extends Fragment {

    private static final String TAG = "ComparativeIncomeAnalys";

    private FragmentComparativeIncomeAnalysisBinding fragmentComparativeIncomeAnalysisBinding;

    private Realm realm;

    public ComparativeIncomeAnalysisFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        realm = Realm.getDefaultInstance();
        // Inflate the layout for this fragment
        fragmentComparativeIncomeAnalysisBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_comparative_income_analysis, container, false);
        updateComparison();
        updateUI();
        return fragmentComparativeIncomeAnalysisBinding.getRoot();
    }

    private void updateUI() {
        NetworkResource.getInstance().getIncomeResponseMutableLiveData().observe(this, response -> {
            if (response != null) {
                setupUI(response);
            }
        });
    }

    private void updateComparison() {
        HashMap<String, String> credentials = QueryUtils.getCredentials(realm);
        String farmerId = credentials.get("farmerId");
        Farmer farmer = realm.where(Farmer.class).equalTo(Farmer.Constants.FARMER_ID, farmerId).findFirst();
        RealmList<CropData> farmerCropData = farmer.getCropData();
        CropData cropData = farmerCropData.get(0);
        GetComparisonSheetRequest getComparisonSheetRequest = new GetComparisonSheetRequest();
        getComparisonSheetRequest.setCrop(Crop.valueOf(cropData.getCrop()));
        NetworkResource.getInstance().getIncomeComparisonSheet(farmer.getAuthToken(), farmer.getId(), getComparisonSheetRequest, getContext());
    }

    private void setupUI(IncomeResponse incomeResponse) {
        double averageIncome = incomeResponse.getAverageIncome();
        double maxIncome = incomeResponse.getMaxIncome();
        double personalIncome = incomeResponse.getPersonalIncome();

        fragmentComparativeIncomeAnalysisBinding.averageProgressBar.setMax((int) maxIncome);
        fragmentComparativeIncomeAnalysisBinding.personalProgressBar.setMax((int) maxIncome);
        fragmentComparativeIncomeAnalysisBinding.idProgressBarHigh.setMax((int) maxIncome);

        if (personalIncome >= maxIncome) {
            fragmentComparativeIncomeAnalysisBinding.bestCard.setBackgroundColor(getResources().getColor(R.color.how_are_you_doing_selected));
        } else if (personalIncome < averageIncome) {
            fragmentComparativeIncomeAnalysisBinding.improveCard.setBackgroundColor(getResources().getColor(R.color.how_are_you_doing_selected));
        } else {
            fragmentComparativeIncomeAnalysisBinding.averageCard.setBackgroundColor(getResources().getColor(R.color.how_are_you_doing_selected));
        }

        if (Build.VERSION.SDK_INT > 23) {
            fragmentComparativeIncomeAnalysisBinding.idProgressBarHigh.setProgress((int) maxIncome, true);
            fragmentComparativeIncomeAnalysisBinding.averageProgressBar.setProgress((int) averageIncome, true);
            fragmentComparativeIncomeAnalysisBinding.personalProgressBar.setProgress((int) personalIncome, true);
        } else {
            fragmentComparativeIncomeAnalysisBinding.idProgressBarHigh.setProgress((int) maxIncome);
            fragmentComparativeIncomeAnalysisBinding.averageProgressBar.setProgress((int) averageIncome);
            fragmentComparativeIncomeAnalysisBinding.personalProgressBar.setProgress((int) personalIncome);
        }

        fragmentComparativeIncomeAnalysisBinding.maxIncomeValue.setText((int) maxIncome + " " + getResources().getString(R.string.income_per_acre));
        fragmentComparativeIncomeAnalysisBinding.personalIncome.setText((int) personalIncome + " " + getResources().getString(R.string.income_per_acre));
        fragmentComparativeIncomeAnalysisBinding.averageIncome.setText((int) averageIncome + " " + getResources().getString(R.string.income_per_acre));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
