package pojo;

import com.google.gson.annotations.SerializedName;

import java.time.temporal.Temporal;

import io.realm.RealmObject;

public class Condition extends RealmObject {

	@SerializedName("code")
	private int code;

	@SerializedName("icon")
	private String icon;

	@SerializedName("text")
	private String text;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setIcon(String icon){
		this.icon = icon;
	}

	public String getIcon(){
		if (this.icon == null)
			return "";
		else
			return icon;

	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		if (this.text == null)
			return "";
		else
			return text;

	}

	@Override
 	public String toString(){
		return 
			"Condition{" + 
			"code = '" + code + '\'' + 
			",icon = '" + icon + '\'' + 
			",text = '" + text + '\'' + 
			"}";
		}
}