package utility;

import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import pojo.Farmer;
import pojo.LatLng;
import response.AddFarmCoordinatesResponse;
import response.ValidateOTPResponse;

public class QueryUtils {

    //  returns authToken,farmerId,countryCode and phoneNumber..
    public static HashMap<String, String> getCredentials(Realm realm) {
        ValidateOTPResponse validateOTPResponse = realm.where( ValidateOTPResponse.class ).findFirst();
        HashMap<String, String> hashMap = new HashMap<>();
        if (validateOTPResponse != null) {
            Farmer farmer = validateOTPResponse.getFarmer();
            hashMap.put( "authToken", farmer.getAuthToken() );
            hashMap.put( "farmerId", farmer.getId() );
            hashMap.put( "farmerReferenceId", farmer.getFarmerReferenceId() );
            hashMap.put( "phoneNumber", farmer.getPhoneNumber() );
            hashMap.put( "countryCode", farmer.getCountryCode() );
            hashMap.put( "cropId", farmer.getCropId() );
        }
        return hashMap;
    }

    //  returns saved lat and long for weather forecast...
    static LatLng getLocation(Realm realm) {
        LatLng latLng = null;
        AddFarmCoordinatesResponse addFarmCoordinatesResponse = realm.where( AddFarmCoordinatesResponse.class ).findFirst();
        if (addFarmCoordinatesResponse != null) {
            List<LatLng> path = addFarmCoordinatesResponse.getPath();
            if (!path.isEmpty()) {
                latLng = path.get( 0 );
            }
        }
        return latLng;
    }

}
