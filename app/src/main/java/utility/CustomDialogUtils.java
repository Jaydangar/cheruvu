package utility;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.cheruvu.cheruvu.R;

public class CustomDialogUtils extends AlertDialog {

    private TextView tvTitle, tvDesc, tvEnglish, tvTelugu;
    private Button btnLeft, btnRight;

    private LinearLayout layoutTwoButtons;

    private ImageView alertIcon;
    private DialogActionListener dialogActionListener;

    public CustomDialogUtils(Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_alert_dialog, null);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvEnglish = view.findViewById(R.id.tv_english);
        tvTelugu = view.findViewById(R.id.tv_telugu);
        tvTitle = view.findViewById(R.id.tv_title);
        tvDesc = view.findViewById(R.id.tv_desc);
        alertIcon = view.findViewById(R.id.alert_icon);

        btnLeft = view.findViewById(R.id.btn_left);
        btnRight = view.findViewById(R.id.btn_right);
        layoutTwoButtons = view.findViewById(R.id.layout_two_button);

        setClickListener(btnLeft, btnRight, tvEnglish, tvTelugu);
        setView(view);
    }

    private void setClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(mOnClickListener);
        }
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialogActionListener.onAction(v);
        }
    };

    public void setOnActionListener(DialogActionListener dialogActionListener) {
        this.dialogActionListener = dialogActionListener;
    }

    public void setAlertTitle(String title) {
        if (title != null) {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
        }
    }

    public void setAlertDesciption(String desciption) {
        tvDesc.setVisibility(View.VISIBLE);
        tvDesc.setText(desciption);
    }

    public void setRightButtonText(String text) {
        layoutTwoButtons.setVisibility(View.VISIBLE);
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setText(text);
    }

    public void setLeftButtonText(String text) {
        layoutTwoButtons.setVisibility(View.VISIBLE);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setText(text);
    }

    public void setEnglishTv(String text) {
        tvEnglish.setVisibility(View.VISIBLE);
        tvEnglish.setText(text);
    }

    public void setTeluguTv(String text) {
        tvTelugu.setVisibility(View.VISIBLE);
        tvTelugu.setText(text);
    }


    public interface DialogActionListener {
        void onAction(View viewId);
    }

}