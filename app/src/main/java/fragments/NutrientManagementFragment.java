package fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import adapter.NutrientAdviceAdapter;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentNutrientManagementBinding;
import io.realm.Realm;
import network.NetworkResource;
import pojo.Farmer;
import utility.QueryUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class NutrientManagementFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private static final String TAG = "NutrientManagementFragm";

    private View dayZeroLayout, dayThirtyLayout, dayFortyFive, daySixty, dayNinety;

    //main view pager
    private ViewPager viewPager;

    // pager adapter
    private NutrientAdviceAdapter mAnalysisPagerViewAdapter;

    private Realm realm;

    public NutrientManagementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach( context );
        mAnalysisPagerViewAdapter = new NutrientAdviceAdapter( getFragmentManager() );
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentNutrientManagementBinding nutrientManagementDataBinging = DataBindingUtil.inflate( inflater, R.layout.fragment_nutrient_management, container, false );

        realm = Realm.getDefaultInstance();
        HashMap<String, String> credentials = QueryUtils.getCredentials( realm );
        String farmerId = credentials.get( "farmerId" );
        String authToken = credentials.get( "authToken" );
        Farmer farmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();
        assert farmer != null;

        NetworkResource.getInstance().getRecommendation( authToken, farmerId, getContext() );

        if (farmer.getPlantingDate() == null) {
            Fragment fragment = new SetPlantingDateFragment();
            replaceFragment( fragment );
        } else {
            String plantingDate = farmer.getPlantingDate();
            try {
                Date currentDate = new SimpleDateFormat( "MMM dd,yyyy" ).parse( plantingDate );
                String originalDate = new SimpleDateFormat( "dd MMM" ).format( currentDate );

                Calendar calendar = Calendar.getInstance();
                calendar.setTime( currentDate );

                calendar.add( Calendar.DAY_OF_YEAR, 30 );
                Date after30Days = calendar.getTime();
                String after30DaysDate = new SimpleDateFormat( "dd MMM" ).format( after30Days );

                calendar.add( Calendar.DAY_OF_YEAR, 15 );
                Date after45Days = calendar.getTime();
                String after45DaysDate = new SimpleDateFormat( "dd MMM" ).format( after45Days );

                calendar.add( Calendar.DAY_OF_YEAR, 15 );
                Date after60Days = calendar.getTime();
                String after60DaysDate = new SimpleDateFormat( "dd MMM" ).format( after60Days );

                calendar.add( Calendar.DAY_OF_YEAR, 30 );
                Date after90Days = calendar.getTime();
                String after90DaysDate = new SimpleDateFormat( "dd MMM" ).format( after90Days );

                nutrientManagementDataBinging.dayOneDate.setText( originalDate );
                nutrientManagementDataBinging.dayThirtyDate.setText( after30DaysDate );
                nutrientManagementDataBinging.dayFourtyFiveDate.setText( after45DaysDate );
                nutrientManagementDataBinging.daySixtyDate.setText( after60DaysDate );
                nutrientManagementDataBinging.dayNintyDate.setText( after90DaysDate );

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        dayZeroLayout = nutrientManagementDataBinging.dayZero;
        dayThirtyLayout = nutrientManagementDataBinging.dayThirty;
        dayFortyFive = nutrientManagementDataBinging.dayFortyFive;
        daySixty = nutrientManagementDataBinging.daySixty;
        dayNinety = nutrientManagementDataBinging.dayNinety;

        viewPager = nutrientManagementDataBinging.recommendationViewPager;
        viewPager.setAdapter( mAnalysisPagerViewAdapter );
        viewPager.setOffscreenPageLimit( 5 );
        viewPager.addOnPageChangeListener( this );
        viewPager.setCurrentItem( 0, true );

        //viewPager label listener
        dayZeroLayout.setOnClickListener( v -> viewPager.setCurrentItem( 0 ) );
        dayThirtyLayout.setOnClickListener( v -> viewPager.setCurrentItem( 1 ) );
        dayFortyFive.setOnClickListener( v -> viewPager.setCurrentItem( 2 ) );
        daySixty.setOnClickListener( v -> viewPager.setCurrentItem( 3 ) );
        dayNinety.setOnClickListener( v -> viewPager.setCurrentItem( 4 ) );

        dayZeroLayout.setBackgroundResource( R.drawable.ic_date_progress );

        return nutrientManagementDataBinging.getRoot();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        viewPager.setCurrentItem( position );
        if (position == 0) {
            dayZeroLayout.setBackgroundResource( R.drawable.ic_date_progress );
            dayThirtyLayout.setBackgroundResource( 0 );
            dayFortyFive.setBackgroundResource( 0 );
            daySixty.setBackgroundResource( 0 );
            dayNinety.setBackgroundResource( 0 );
        }

        if (position == 1) {
            dayZeroLayout.setBackgroundResource( 0 );
            dayThirtyLayout.setBackgroundResource( R.drawable.ic_date_progress );
            dayFortyFive.setBackgroundResource( 0 );
            daySixty.setBackgroundResource( 0 );
            dayNinety.setBackgroundResource( 0 );
        }
        if (position == 2) {
            dayZeroLayout.setBackgroundResource( 0 );
            dayThirtyLayout.setBackgroundResource( 0 );
            dayFortyFive.setBackgroundResource( R.drawable.ic_date_progress );
            daySixty.setBackgroundResource( 0 );
            dayNinety.setBackgroundResource( 0 );
        }
        if (position == 3) {
            dayZeroLayout.setBackgroundResource( 0 );
            dayThirtyLayout.setBackgroundResource( 0 );
            dayFortyFive.setBackgroundResource( 0 );
            daySixty.setBackgroundResource( R.drawable.ic_date_progress );
            dayNinety.setBackgroundResource( 0 );
        }
        if (position == 4) {
            dayZeroLayout.setBackgroundResource( 0 );
            dayThirtyLayout.setBackgroundResource( 0 );
            dayFortyFive.setBackgroundResource( 0 );
            daySixty.setBackgroundResource( 0 );
            dayNinety.setBackgroundResource( R.drawable.ic_date_progress );
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations( R.anim.slide_in_left, R.anim.slide_out_right );
        ft.replace( R.id.main_fragment_placeholder, fragment );
        ft.commit();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
