package pojo;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Astro extends RealmObject {

	@SerializedName("moonset")
	private String moonset;

	@SerializedName("sunrise")
	private String sunrise;

	@SerializedName("sunset")
	private String sunset;

	@SerializedName("moonrise")
	private String moonrise;

	public void setMoonset(String moonset){
		this.moonset = moonset;
	}

	public String getMoonset(){
		if (this.moonset == null)
			return "";
		else
			return moonset;

	}

	public void setSunrise(String sunrise){
		this.sunrise = sunrise;
	}

	public String getSunrise(){
		if (this.sunrise== null)
			return "";
		else
			return sunrise;

	}

	public void setSunset(String sunset){
		this.sunset = sunset;
	}

	public String getSunset(){
		if (this.sunset == null)
			return "";
		else
			return sunset;

	}

	public void setMoonrise(String moonrise){
		this.moonrise = moonrise;
	}

	public String getMoonrise(){
		if (this.moonrise == null)
			return "";
		else
			return moonset;

	}

	@Override
 	public String toString(){
		return 
			"Astro{" + 
			"moonset = '" + moonset + '\'' + 
			",sunrise = '" + sunrise + '\'' + 
			",sunset = '" + sunset + '\'' + 
			",moonrise = '" + moonrise + '\'' + 
			"}";
		}
}