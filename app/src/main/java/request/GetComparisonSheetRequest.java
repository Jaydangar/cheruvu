package request;

import java.util.ArrayList;
import java.util.List;

import enums.Crop;
import in.cheruvu.cheruvu.R;

public class GetComparisonSheetRequest {

    private Crop crop;

    public Crop getCrop() {
        return crop;
    }

    public void setCrop(Crop crop) {
        this.crop = crop;
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<String>();
        if(crop==null) {
            errors.add(String.valueOf(R.string.crop_required));
        }
        return errors;
    }
}
