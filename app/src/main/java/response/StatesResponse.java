package response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StatesResponse {

    @SerializedName("lastUpdated")
    private long lastUpdated;

    @SerializedName("creationTime")
    private long creationTime;

    @SerializedName("stateName")
    private String stateName;

    @SerializedName("createdBy")
    private String createdBy;

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @SerializedName("state")
    private String state;

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateName() {
        if (this.stateName == null)
            return "";
        else
            return stateName;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        if (this.createdBy == null)
            return "";
        else
            return createdBy;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        if (this.id == null)
            return "";
        else
            return id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        if (this.state == null)
            return "";
        else
            return state;
    }

    @Override
    public String toString() {
        return
                "StatesResponse{" +
                        "lastUpdated = '" + lastUpdated + '\'' +
                        ",creationTime = '" + creationTime + '\'' +
                        ",stateName = '" + stateName + '\'' +
                        ",createdBy = '" + createdBy + '\'' +
                        ",id = '" + id + '\'' +
                        ",state = '" + state + '\'' +
                        "}";
    }
}