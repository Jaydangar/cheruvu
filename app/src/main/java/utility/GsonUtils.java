package utility;

import com.google.gson.Gson;

import java.lang.reflect.Type;

//  This Class is useful for mapping Json into Java Objects and vice versa.
public class GsonUtils {

    private static final Gson gson = new Gson();

    //  This will Convert Java Objects into JSON String...
    public static String toGson(Object object) {
        return gson.toJson( object );
    }

    //  Gives Java Objects from JSON
    public static <T> T fromGson(String json, Class<T> type) {
        return gson.fromJson( json, type );
    }

    //  get Array...
    public static Object jsonToArray(String json, Type type) {
        return gson.fromJson( json, type );
    }
}
