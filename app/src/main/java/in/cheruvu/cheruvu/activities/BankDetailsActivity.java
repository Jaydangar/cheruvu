package in.cheruvu.cheruvu.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.room.Database;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Objects;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivityBankDetailsBinding;
import io.realm.Realm;
import network.NetworkResource;
import pojo.Farmer;
import request.BankAccountDetailRequest;
import response.BankDetailsResponse;
import utility.ConstantUtils;
import utility.QueryUtils;

public class BankDetailsActivity extends AppCompatActivity {

    private ActivityBankDetailsBinding activityBankDetailsBinding;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBankDetailsBinding = DataBindingUtil.setContentView( this,R.layout.activity_bank_details);
        realm = Realm.getDefaultInstance();

        BankDetailsResponse bankDetailsResponse = realm.where( BankDetailsResponse.class ).findFirst();
        if(bankDetailsResponse!=null){

            activityBankDetailsBinding.confirmBankAccountNumber.setText( bankDetailsResponse.getBankAccountNumber() );
            activityBankDetailsBinding.bankAccountNumber.setText( bankDetailsResponse.getBankAccountNumber() );
            activityBankDetailsBinding.accountHolderName.setText( bankDetailsResponse.getBankAccountHolderName() );
            activityBankDetailsBinding.ifscCode.setText( bankDetailsResponse.getIfscCode() );

            activityBankDetailsBinding.submitBankDetails.setText(getResources().getString(R.string.update_personal_details));
        }
        else {
            activityBankDetailsBinding.submitBankDetails.setText(getResources().getString(R.string.submit));
        }
    }

    public void onSubmit(View view) {

        HashMap<String,String> map = QueryUtils.getCredentials( realm );
        String authToken = map.get("authToken");
        String farmerId = map.get("farmerId");

        String bankAccountNumber = Objects.requireNonNull( activityBankDetailsBinding.bankAccountNumber.getText() ).toString();
        String accountHolderName = Objects.requireNonNull( activityBankDetailsBinding.accountHolderName.getText() ).toString();
        String confirmationNumber = Objects.requireNonNull( activityBankDetailsBinding.confirmBankAccountNumber.getText() ).toString();
        String ifscCode = Objects.requireNonNull( activityBankDetailsBinding.ifscCode.getText() ).toString();

        BankAccountDetailRequest bankAccountDetailRequest = new BankAccountDetailRequest();
        bankAccountDetailRequest.setBankAccountHolderName( accountHolderName );
        bankAccountDetailRequest.setBankAccountNumber( bankAccountNumber );
        bankAccountDetailRequest.setIfscCode( ifscCode );

        if(bankAccountNumber.equals( confirmationNumber )){
            NetworkResource.getInstance().addBankDetails(authToken,farmerId,bankAccountDetailRequest,this);
        }
        else {
            Toast.makeText( this, getResources().getString(R.string.mismatch_error), Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
