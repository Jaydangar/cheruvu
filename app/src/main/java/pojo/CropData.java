package pojo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import utility.ConstantUtils;

public class CropData extends RealmObject {

    @PrimaryKey
    private String id;

    public CropData() {
        id = ConstantUtils.getCropId();
    }

    private String cropName;
    private String crop;
    private Float cropAcres;
    private Float cropYield;
    private Float cropPrice;

    public String getCropName() {
        if (this.cropName == null)
            return "";
        else
            return cropName;

    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getCrop() {
        if (this.crop == null)
            return "";
        else
            return crop;

    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public Float getCropAcres() {
        if (this.cropAcres == null)
            return 0.0F;
        else
            return cropAcres;

    }

    public void setCropAcres(Float cropAcres) {
        this.cropAcres = cropAcres;
    }

    public Float getCropYield() {
        if (this.cropYield == null)
            return 0.0F;
        else
            return cropYield;
    }

    public void setCropYield(Float cropYield) {
        this.cropYield = cropYield;
    }

    public Float getCropPrice() {
        if (this.cropPrice == null)
            return 0.0F;
        else
            return cropPrice;
    }

    public void setCropPrice(Float cropPrice) {
        this.cropPrice = cropPrice;
    }

    public String getId() {
        if (this.id == null)
            return "";
        else
            return id;
    }

    public static class Constants {
        public static final String ID = "id";
    }
}
