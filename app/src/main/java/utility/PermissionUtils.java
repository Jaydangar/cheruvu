package utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionUtils {

    private static final String TAG = PermissionUtils.class.getSimpleName();

    private Context context;

    public PermissionUtils(Context context) {
        this.context = context;
    }

    //  Check Required Permissions...
    public boolean checkRequestedPermission(Context context, String CheckRequiredPermission) {
        return ContextCompat.checkSelfPermission( context, CheckRequiredPermission ) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(final Context context, final String RequestedPermission, final int PermissionCode) {
        if (!checkRequestedPermission( context, RequestedPermission )) {
            ActivityCompat.requestPermissions( (Activity) context, new String[]{RequestedPermission},
                    PermissionCode );
        }
    }

    public void RequestReadSMSPermission(int READ_SMS_REQUEST_CODE) {
        requestPermission( context, Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_SMS_REQUEST_CODE );
    }
}