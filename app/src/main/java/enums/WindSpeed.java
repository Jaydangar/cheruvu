package enums;

//  Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
public enum WindSpeed {
    Metric,Imperial
}
