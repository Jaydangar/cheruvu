package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.Register;
import response.DistrictResponse;
import response.SubDistrictResponse;
import response.VillageResponse;

public class DistrictAdapter extends BaseAdapter {

    private List<DistrictResponse> searchList;
    private ArrayList<DistrictResponse> districtResponseArrayList;
    private Context context;
    private LayoutInflater inflater;
    private MutableLiveData<DistrictResponse> districtResponseMutableLiveData;

    static class ViewHolder {
        TextView eachItem;
    }

    public DistrictAdapter(Context context, ArrayList<DistrictResponse> arrayAdapter) {
        this.context = context;
        inflater = LayoutInflater.from( context );
        this.districtResponseArrayList = arrayAdapter;
        districtResponseMutableLiveData = Register.getDistrictResponse();
        searchList = new ArrayList<>();
        searchList.addAll( districtResponseArrayList );
    }

    @Override
    public int getCount() {
        return districtResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return districtResponseArrayList.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate( R.layout.item_selection, null );
            viewHolder.eachItem = convertView.findViewById( R.id.listItem );
            convertView.setTag( viewHolder );
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //  setting text on items...
        viewHolder.eachItem.setText( districtResponseArrayList.get( position ).getDistrictName() );

        //  setting onclicklistener...
        View finalConvertView = convertView;
        convertView.setOnClickListener( v -> {
            DistrictResponse districtResponse = districtResponseArrayList.get( position );
            districtResponseMutableLiveData.postValue( districtResponse );
            updateSV( finalConvertView );
            ((Activity) context).finish();
        } );

        return convertView;
    }

    //  we need to update SubDistrict and Village to default one, whenever state changes..
    private void updateSV(View view) {
        Context context = view.getContext();
        SubDistrictResponse subDistrictResponse = new SubDistrictResponse();
        subDistrictResponse.setMandalName( context.getString( R.string.select_sub_district ) );
        subDistrictResponse.setId( "0" );
        Register.getSubDistrictResponse().setValue( subDistrictResponse );

        VillageResponse villageResponse = new VillageResponse();
        villageResponse.setVillageName( context.getString( R.string.select_village ) );
        villageResponse.setId( "0" );
        Register.getVillageResponse().setValue( villageResponse );
    }

    //  filter
    public void filter(String charText) {
        charText = charText.toLowerCase( Locale.getDefault() );
        districtResponseArrayList.clear();
        if (charText.length() == 0) {
            districtResponseArrayList.addAll( searchList );
        } else {
            for (DistrictResponse model : searchList) {
                if (model.getDistrictName().toLowerCase( Locale.getDefault() )
                        .contains( charText )) {
                    districtResponseArrayList.add( model );
                }
            }
        }
        notifyDataSetChanged();
    }
}
