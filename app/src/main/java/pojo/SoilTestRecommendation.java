package pojo;

public class SoilTestRecommendation {

    private Double nRec;
    private Double p2o5Rec;
    private Double k2oRec;
    private Double znRec;
    private Double bRec;
    private Double ureaRec;
    private Double dapRec;
    private Double potashRec;

    public Double getnRec() {
        if (this.nRec == null)
            return 0.0;
        else
            return nRec;
    }

    public void setnRec(Double nRec) {
        this.nRec = nRec;
    }

    public Double getP2o5Rec() {
        if (this.getP2o5Rec() == null)
            return 0.0;
        else
            return p2o5Rec;
    }

    public void setP2o5Rec(Double p2o5Rec) {
        this.p2o5Rec = p2o5Rec;
    }

    public Double getK2oRec() {
        if (this.k2oRec == null)
            return 0.0;
        else
            return k2oRec;
    }

    public void setK2oRec(Double k2oRec) {
        this.k2oRec = k2oRec;
    }

    public Double getZnRec() {
        if (this.znRec == null)
            return 0.0;
        else
            return znRec;
    }

    public void setZnRec(Double znRec) {
        this.znRec = znRec;
    }

    public Double getbRec() {
        if (this.bRec == null)
            return 0.0;
        else
            return bRec;
    }

    public void setbRec(Double bRec) {
        this.bRec = bRec;
    }

    public Double getUreaRec() {
        if (this.ureaRec == null)
            return 0.0;
        else
            return ureaRec;
    }

    public void setUreaRec(Double ureaRec) {
        this.ureaRec = ureaRec;
    }

    public Double getDapRec() {
        if (this.dapRec == null)
            return 0.0;
        else
            return dapRec;
    }

    public void setDapRec(Double dapRec) {
        this.dapRec = dapRec;
    }

    public Double getPotashRec() {
        if (this.potashRec == null)
            return 0.0;
        else
            return potashRec;
    }

    public void setPotashRec(Double potashRec) {
        this.potashRec = potashRec;
    }
}
