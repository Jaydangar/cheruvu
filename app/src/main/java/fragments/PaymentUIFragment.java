package fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentPaymentUiBinding;
import utility.ConstantUtils;
import utility.PermissionUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentUIFragment extends Fragment {

    private boolean isReadSMSPermissionGranted = false;
    private boolean isRecieveSMSPermissionGranted = false;

    private final int READ_SMS_CODE = ConstantUtils.getReadSmsRequest();
    private final int RECEIVE_SMS_CODE = ConstantUtils.getReceiveSmsRequest();

    private PermissionUtils permissionUtils;

    public PaymentUIFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //  just to initialize to observate and store data in Sqlite...
        //  InjectionUtils.paymentRepository(this.requireContext());

        FragmentPaymentUiBinding fragmentPaymentUiBinding = DataBindingUtil.inflate( inflater, R.layout.fragment_payment_ui, container, false );
        fragmentPaymentUiBinding.payBtn.setOnClickListener( this::startPayment );

        permissionUtils = new PermissionUtils( this.requireContext() );
        isReadSMSPermissionGranted = permissionUtils.checkRequestedPermission( this.requireContext(), Manifest.permission.READ_SMS );

        return fragmentPaymentUiBinding.getRoot();
    }

    private void startPayment(View view) {

        if (!isReadSMSPermissionGranted) {
            permissionUtils.RequestReadSMSPermission( READ_SMS_CODE );
        }

        if (!isRecieveSMSPermissionGranted) {
            permissionUtils.RequestReadSMSPermission( RECEIVE_SMS_CODE );
        }

        //  NetworkResource.getInstance().paymentVerification( InjectionUtils.getAuthToken( this.requireContext() ), InjectionUtils.getFarmerId( this.requireContext() ), this.requireContext() );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
        if (requestCode == READ_SMS_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isReadSMSPermissionGranted = true;
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText( this.requireContext(), getResources().getString(R.string.toas_read_sms_permission_not_granted), Toast.LENGTH_SHORT ).show();
            }
        }

        if (requestCode == RECEIVE_SMS_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isRecieveSMSPermissionGranted = true;
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText( this.requireContext(), getResources().getString(R.string.toas_read_sms_permission_not_granted), Toast.LENGTH_SHORT ).show();
            }
        }
    }
}
