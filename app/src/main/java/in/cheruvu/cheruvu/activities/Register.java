package in.cheruvu.cheruvu.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivityRegisterBinding;
import io.realm.Realm;
import network.NetworkResource;
import pojo.Farmer;
import pojo.UpdateFarmerRequest;
import request.FarmerRequest;
import response.DistrictResponse;
import response.StatesResponse;
import response.SubDistrictResponse;
import response.VillageResponse;
import utility.ConstantUtils;
import utility.IntentUtils;
import utility.PortraitHelper;
import utility.QueryUtils;

public class Register extends AppCompatActivity {

    private static final String TAG = Register.class.getSimpleName();

    private ActivityRegisterBinding activityRegisterBinding;

    private String firstName, stateId, districtId, subDistrictId, villageId, phoneNumber, countryCode, address, authToken, farmerId;
    private String stateName, districtName, subDistrictName, villageName;

    //  these are used for observing the response...
    private static final MutableLiveData<StatesResponse> statesResponse = new MutableLiveData<>();
    private static final MutableLiveData<DistrictResponse> districtResponse = new MutableLiveData<>();
    private static final MutableLiveData<SubDistrictResponse> subDistrictResponse = new MutableLiveData<>();
    private static final MutableLiveData<VillageResponse> villageResponse = new MutableLiveData<>();

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PortraitHelper.initialize(this);

        realm = Realm.getDefaultInstance();

        //  inializing strings..
        stateName = getResources().getString(R.string.select_state);
        districtName = getResources().getString(R.string.select_district);
        subDistrictName = getResources().getString(R.string.select_sub_district);
        villageName = getResources().getString(R.string.select_village);

        //  portrait mode
        PortraitHelper.initialize(this);

        activityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        //  check and see if you got msg from profile Fragment, if yes then fetch and load data from db onto the view...
        fetchAndSetData(activityRegisterBinding);

        statesResponse.observe(this, statesResponse -> {
            stateId = statesResponse.getId();
            stateName = statesResponse.getStateName();
            activityRegisterBinding.stateSelection.setText(statesResponse.getStateName());
        });

        districtResponse.observe(this, districtResponse -> {
            districtId = districtResponse.getId();
            districtName = districtResponse.getDistrictName();
            activityRegisterBinding.districtSelection.setText(districtResponse.getDistrictName());
        });

        subDistrictResponse.observe(this, subDistrictResponse -> {
            subDistrictId = subDistrictResponse.getId();
            subDistrictName = subDistrictResponse.getMandalName();
            activityRegisterBinding.subDistrictSelection.setText(subDistrictResponse.getMandalName());
        });

        villageResponse.observe(this, villageResponse -> {
            villageId = villageResponse.getId();
            villageName = villageResponse.getVillageName();
            activityRegisterBinding.villageSelection.setText(villageResponse.getVillageName());
        });

    }

    private void fetchAndSetData(ActivityRegisterBinding activityRegisterBinding) {

        //  if there's update/View data Request then fetch and load data on view...
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String msg = bundle.getString("fragmentMsg");
            assert msg != null;
            if (msg.equals("personalDetails")) {
                HashMap<String,String> credentials = QueryUtils.getCredentials( realm );
                String farmerId = credentials.get( "farmerId" );
                Farmer farmer = realm.where(Farmer.class).equalTo( Farmer.Constants.FARMER_ID,farmerId ).findFirst();
                if (farmer != null) {

                    activityRegisterBinding.IdFullName.setText(farmer.getFirstName());
                    activityRegisterBinding.idAddress.setText(farmer.getAddress());
                    activityRegisterBinding.stateSelection.setText(farmer.getStateName());
                    activityRegisterBinding.districtSelection.setText(farmer.getCity());
                    activityRegisterBinding.subDistrictSelection.setText(farmer.getMandalName());
                    activityRegisterBinding.villageSelection.setText(farmer.getVillageName());

                    //  this will reset the value, if not updated...
                    resetValues(farmer);

                    activityRegisterBinding.buttonContinue.setText(getResources().getString(R.string.update_personal_details));
                }
            }
        }
    }

    private void resetValues(Farmer farmer) {
        StatesResponse sResponse = new StatesResponse();
        sResponse.setId(farmer.getStateId());
        sResponse.setStateName(farmer.getStateName());

        DistrictResponse dResponse = new DistrictResponse();
        dResponse.setId(farmer.getDistrictId());
        dResponse.setDistrictName(farmer.getCity());
        dResponse.setStateId(farmer.getStateId());

        SubDistrictResponse subDResponse = new SubDistrictResponse();
        subDResponse.setId(farmer.getMandalId());
        subDResponse.setMandalName(farmer.getMandalName());
        subDResponse.setDistrictId(farmer.getDistrictId());

        VillageResponse vResponse = new VillageResponse();
        vResponse.setId(farmer.getVillageId());
        vResponse.setVillageName(farmer.getVillageName());
        vResponse.setMandalId(farmer.getMandalId());

        statesResponse.postValue(sResponse);
        districtResponse.postValue(dResponse);
        subDistrictResponse.postValue(subDResponse);
        villageResponse.postValue(vResponse);
    }

    public static MutableLiveData<StatesResponse> getStatesResponse() {
        return statesResponse;
    }

    public static MutableLiveData<DistrictResponse> getDistrictResponse() {
        return districtResponse;
    }

    public static MutableLiveData<SubDistrictResponse> getSubDistrictResponse() {
        return subDistrictResponse;
    }

    public static MutableLiveData<VillageResponse> getVillageResponse() {
        return villageResponse;
    }

    public void onSubmit(View view) {

        //  intialize all strings...
        initializeData();

        List<String> errorList = verify(view);
        if (errorList.isEmpty()) {
            //  no errors, send API Request...
            Farmer farmer = new Farmer();
            farmer.setAddress(address);
            farmer.setFirstName(firstName);
            farmer.setStateId(stateId);
            farmer.setDistrictId(districtId);
            farmer.setMandalId(subDistrictId);
            farmer.setVillageId(villageId);
            farmer.setPhoneNumber(phoneNumber);
            farmer.setCountryCode(countryCode);
            FarmerRequest farmerRequest = new FarmerRequest();
            farmerRequest.setFarmer(farmer);

            AppCompatButton appCompatButton = activityRegisterBinding.buttonContinue;
            String txt = appCompatButton.getText().toString();
            if (txt.equals(getResources().getString(R.string.update_personal_details))) {
                UpdateFarmerRequest updateFarmerRequest = new UpdateFarmerRequest();
                updateFarmerRequest.setFarmer(farmer);
                NetworkResource.getInstance().farmerProfileUpdate(authToken, farmerId, updateFarmerRequest, this);
            } else {
                NetworkResource.getInstance().farmerRegistration(authToken, farmerId, farmerRequest, this);
            }
        } else {
            //  Some error Occurred...
            Toast.makeText(view.getContext(), errorList.get(0), Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeData() {
        address = Objects.requireNonNull( activityRegisterBinding.idAddress.getText() ).toString();
        firstName = Objects.requireNonNull( activityRegisterBinding.IdFullName.getText() ).toString();

        HashMap<String, String> map = QueryUtils.getCredentials(realm);
        authToken = map.get("authToken");
        farmerId = map.get("farmerId");
        phoneNumber = map.get("phoneNumber");
        countryCode = map.get("countryCode");
    }

    private List<String> verify(View view) {
        List<String> Errors = new ArrayList<>();
        if (firstName.length() == 0) {
            Errors.add(getResources().getString(R.string.empty_name_error));
        }
        if (address.length() == 0) {
            Errors.add(getResources().getString(R.string.empty_address_error));
        }
        if (stateName.equals(getResources().getString(R.string.select_state))) {
            Errors.add(getResources().getString(R.string.empty_state_error));
        }
        if (districtName.equals(view.getResources().getString(R.string.select_district))) {
            Errors.add(getResources().getString(R.string.empty_district_error));
        }
        if (subDistrictName.equals(view.getResources().getString(R.string.select_sub_district))) {
            Errors.add(getResources().getString(R.string.empty_subdistrict_error));
        }
        if (villageName.equals(view.getResources().getString(R.string.select_village))) {
            Errors.add(getResources().getString(R.string.empty_village_error));
        }
        return Errors;
    }

    public void selectState(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("option", ConstantUtils.getStateOption());
        IntentUtils.PassIntentWithBundle(view.getContext(), SelectItemActivity.class, bundle);
    }

    public void selectDistrict(View view) {
        if (statesResponse.getValue() != null) {
            if (statesResponse.getValue().getId().equals("0")) {
                Toast.makeText(view.getContext(), getResources().getString(R.string.select_state_first_toast), Toast.LENGTH_SHORT).show();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("stateId", Objects.requireNonNull(statesResponse.getValue()).getId());
                bundle.putString("option", ConstantUtils.getDistrictOption());
                IntentUtils.PassIntentWithBundle(view.getContext(), SelectItemActivity.class, bundle);
            }
        } else {
            Toast.makeText(view.getContext(), getResources().getString(R.string.select_state_first_toast), Toast.LENGTH_SHORT).show();
        }
    }

    public void selectSubDistrict(View view) {
        if (districtResponse.getValue() != null) {
            if (districtResponse.getValue().getId().equals("0")) {
                Toast.makeText(view.getContext(), getResources().getString(R.string.select_district_first_toast), Toast.LENGTH_SHORT).show();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("districtId", Objects.requireNonNull(districtResponse.getValue()).getId());
                bundle.putString("option", ConstantUtils.getSubdistrictOption());
                IntentUtils.PassIntentWithBundle(view.getContext(), SelectItemActivity.class, bundle);
            }
        } else {
            Toast.makeText(view.getContext(), getResources().getString(R.string.select_district_first_toast), Toast.LENGTH_SHORT).show();
        }
    }

    public void selectVillage(View view) {
        if (subDistrictResponse.getValue() != null) {
            if (subDistrictResponse.getValue().getId().equals("0")) {
                Toast.makeText(view.getContext(), getResources().getString(R.string.select_subdistrict_first), Toast.LENGTH_SHORT).show();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("subdistrictid", Objects.requireNonNull(subDistrictResponse.getValue()).getId());
                bundle.putString("option", ConstantUtils.getVillageOption());
                IntentUtils.PassIntentWithBundle(view.getContext(), SelectItemActivity.class, bundle);
            }
        } else {
            Toast.makeText(view.getContext(), getResources().getString(R.string.select_subdistrict_first), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
