package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentSoilTestBinding;
import response.SoilTestDataResponse;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShowSoilTestResult#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShowSoilTestResult extends Fragment {

    private SoilTestDataResponse soilTestDataResponse;

    public ShowSoilTestResult(SoilTestDataResponse soilTestDataResponse) {
        // Required empty public constructor
        this.soilTestDataResponse = soilTestDataResponse;
    }

    public static ShowSoilTestResult newInstance(SoilTestDataResponse soilTestDataResponse) {
        return new ShowSoilTestResult( soilTestDataResponse );
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSoilTestBinding inflate = DataBindingUtil.inflate( inflater, R.layout.fragment_soil_test, container, false );

        String ph = soilTestDataResponse.getPh().toString();
        String ec = soilTestDataResponse.getEc().toString();
        String clay = soilTestDataResponse.getClay().toString();
        String silt = soilTestDataResponse.getSilt().toString();
        String sand = soilTestDataResponse.getSand().toString();
        String texture = soilTestDataResponse.getTextureClass();
        String hue = soilTestDataResponse.getHue().toString();
        String chroma = soilTestDataResponse.getChroma().toString();
        String oc = soilTestDataResponse.getOc();
        String n = soilTestDataResponse.getN().toString();
        String p2o5 = soilTestDataResponse.getP2o5().toString();
        String k2o = soilTestDataResponse.getK2o().toString();
        String cu = soilTestDataResponse.getCu().toString();
        String mn = soilTestDataResponse.getMn().toString();
        String fe = soilTestDataResponse.getFe().toString();
        String zn = soilTestDataResponse.getZn().toString();
        String borax = soilTestDataResponse.getBorax().toString();

        if(ph.equals( "0.0" )){
            inflate.phValue.setText("-");
        }
        else{
            inflate.phValue.setText( ph );
        }

        if(ec.equals( "0.0" )){
            inflate.ecValue.setText("-");
        }
        else{
            inflate.ecValue.setText( ph );
        }

        if(clay.equals( "0.0" )){
            inflate.clayValue.setText("-");
        }
        else{
            inflate.clayValue.setText( ph );
        }

        if(silt.equals( "0.0" )){
            inflate.siltValue.setText("-");
        }
        else{
            inflate.siltValue.setText( ph );
        }

        if(sand.equals( "0.0" )){
            inflate.sandValue.setText("-");
        }
        else{
            inflate.sandValue.setText( ph );
        }

        if(texture.equals( "0.0" )){
            inflate.textureValue.setText("-");
        }
        else{
            inflate.textureValue.setText( ph );
        }

        if(hue.equals( "0.0" )){
            inflate.hueValue.setText("-");
        }
        else{
            inflate.hueValue.setText( ph );
        }

        if(chroma.equals( "0.0" )){
            inflate.chromaValue.setText("-");
        }
        else{
            inflate.chromaValue.setText( ph );
        }

        if(oc.equals( "0.0" )){
            inflate.ocValue.setText("-");
        }
        else{
            inflate.ocValue.setText( ph );
        }

        if(n.equals( "0.0" )){
            inflate.nValue.setText("-");
        }
        else{
            inflate.nValue.setText( ph );
        }

        if(p2o5.equals( "0.0" )){
            inflate.p2O5Value.setText("-");
        }
        else{
            inflate.p2O5Value.setText( ph );
        }

        if(k2o.equals( "0.0" )){
            inflate.k2oValue.setText("-");
        }
        else{
            inflate.k2oValue.setText( ph );
        }

        if(cu.equals( "0.0" )){
            inflate.cuValue.setText("-");
        }
        else{
            inflate.cuValue.setText( ph );
        }

        if(mn.equals( "0.0" )){
            inflate.mnValue.setText("-");
        }
        else{
            inflate.mnValue.setText( ph );
        }

        if(fe.equals( "0.0" )){
            inflate.feValue.setText("-");
        }
        else{
            inflate.feValue.setText( ph );
        }

        if(zn.equals( "0.0" )){
            inflate.znValue.setText("-");
        }
        else{
            inflate.znValue.setText( ph );
        }

        if(borax.equals( "0.0" )){
            inflate.boraxValue.setText("-");
        }
        else{
            inflate.boraxValue.setText( ph );
        }

        return inflate.getRoot();
    }

}
