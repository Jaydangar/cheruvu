package network;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.MutableLiveData;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.reflect.TypeToken;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import application.ApplicationClass;
import enums.ErrorCode;
import exception.NoConnectivityException;
import fragments.NutrientManagementFragment;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.activities.CropDetails;
import in.cheruvu.cheruvu.activities.HomeScreenActivity;
import in.cheruvu.cheruvu.activities.NetworkErrorActivity;
import in.cheruvu.cheruvu.activities.Register;
import in.cheruvu.cheruvu.activities.SplashActivity;
import in.cheruvu.cheruvu.activities.VerifyOTP;
import io.realm.Realm;
import io.realm.RealmList;
import pojo.CropData;
import pojo.Farmer;
import pojo.PaymentLogResponse;
import pojo.UpdateFarmerRequest;
import request.BankAccountDetailRequest;
import request.CropDataUpdateRequest;
import request.FarmCoordinatesData;
import request.FarmerRequest;
import request.GetComparisonSheetRequest;
import request.PaymentRequest;
import request.PlantingDateRequest;
import request.SendOTPRequest;
import request.ValidateOTPRequest;
import response.AddFarmCoordinatesResponse;
import response.BankDetailsResponse;
import response.BasicResponse;
import response.DistrictResponse;
import response.ForecastResponse;
import response.IncomeResponse;
import response.PaymentResponse;
import response.RecommendationSheetResponse;
import response.SoilTestDataResponse;
import response.StatesResponse;
import response.SubDistrictResponse;
import response.UpdateCropResponse;
import response.UpdateProfileReponse;
import response.ValidateOTPResponse;
import response.VillageResponse;
import response.YieldResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.EndPoints;
import utility.AlertUtils;
import utility.ConstantUtils;
import utility.GsonUtils;
import utility.IntentUtils;
import utility.NetworkErrorHandlingUtils;
import utility.PeriodicWork;

//  this is the class from which we are going to call all apis and
public class NetworkResource {

    private ProgressDialog dialog;
    private static NetworkResource sInstance;
    private static final Object LOCK = new Object();
    private static final String TAG = NetworkResource.class.getSimpleName();

    private final MutableLiveData<ArrayList<StatesResponse>> stateData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<DistrictResponse>> districtData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<SubDistrictResponse>> subDistrictData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<VillageResponse>> villageData = new MutableLiveData<>();

    private final MutableLiveData<IncomeResponse> incomeResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<YieldResponse> yieldResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<RecommendationSheetResponse> recommendationSheetData = new MutableLiveData<>();
    private final MutableLiveData<SoilTestDataResponse> soilTestresult = new MutableLiveData<>();
    //  Interval is for a DAY...
    private static final int INTERVAL = 1;
    private static final int BACKOFF = 15;

    /**
     * Get the singleton for this class
     */
    public static NetworkResource getInstance() {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new NetworkResource();
            }
        }
        return sInstance;
    }

    public void getOTPCode(Context context, SendOTPRequest sendOTPRequest, boolean isRunningVerificationActivity) {
        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.generating ) );

        EndPoints.sendOtp( sendOTPRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {

                        //  go to the Verification Screen with data...
                        Bundle bundle = new Bundle();
                        bundle.putString( "countryCode", sendOTPRequest.getCountryCode() );
                        bundle.putString( "phoneNumber", sendOTPRequest.getPhoneNumber() );
                        bundle.putString( "appSignature", sendOTPRequest.getAppSignature() );

                        if (!isRunningVerificationActivity) {
                            IntentUtils.PassIntentWithBundle( context, VerifyOTP.class, bundle );
                            ((Activity) context).finish();
                        }
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, errorList.get( 0 ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_generateOTP ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_generateOTP ) );
                }
            }
        } );
    }

    public void verifyOTP(Context context, ValidateOTPRequest validateOTPRequest) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.verifying ) );

        EndPoints.validateOtp( validateOTPRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        ValidateOTPResponse validateOTPResponse = GsonUtils.fromGson( basicResponse.getResponse(), ValidateOTPResponse.class );
                        boolean registered = validateOTPResponse.getFarmer().getRegistered();
                        try (Realm realm = Realm.getDefaultInstance()) {
                            realm.executeTransactionAsync( realm1 -> realm1.insertOrUpdate( validateOTPResponse ) );
                        }
                        if (registered) {
                            Farmer farmer = validateOTPResponse.getFarmer();

                            String authToken = farmer.getAuthToken();
                            String farmerId = farmer.getId();

                            getBankDetails( context, farmerId, authToken );
                        } else {
                            IntentUtils.PassIntent( context, Register.class );
                            ((Activity) (context)).finish();
                        }
                    } else {
                        dismissDialog();
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, errorList.get( 0 ) );
                    }
                } else {
                    dismissDialog();
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_verifyOTP ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_verifyOTP ) );
                }
            }
        } );
    }

    void getBankDetails(Context context, String farmerId, String authToken) {
        EndPoints.getBankDetails( authToken, farmerId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        BankDetailsResponse bankDetailsResponse = GsonUtils.fromGson( basicResponse.getResponse(), BankDetailsResponse.class );
                        if(bankDetailsResponse!=null){
                            try (Realm realm = Realm.getDefaultInstance()) {
                                realm.executeTransactionAsync( realm1 -> realm1.copyToRealmOrUpdate( bankDetailsResponse ), () -> {
                                    //  fetch all data...
                                    getFarmCoordinates( authToken, farmerId, context );
                                } );
                            }
                        }
                        else{
                            //  fetch all data...
                            getFarmCoordinates( authToken, farmerId, context );
                        }
                    } else {
                        dismissDialog();
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, errorList.get( 0 ) );
                    }
                } else {
                    dismissDialog();
                    showAlertDialogWithOneButton( context, context.getString( R.string.bankDetailsError ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.bankDetailsError ) );
                }
            }
        } );
    }

    public void getStates(Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.loading ) );

        EndPoints.getStates( new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    List<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        Type type = new TypeToken<List<StatesResponse>>() {
                        }.getType();
                        ArrayList<StatesResponse> stateList = (ArrayList<StatesResponse>) GsonUtils.jsonToArray( basicResponse.getResponse(), type );
                        stateData.postValue( stateList );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_stateList ) );
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_stateList ) );
                }
            }
        } );
    }

    public void getDistricts(String stateId, Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.loading ) );

        EndPoints.getDistrictsByStateId( stateId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        Type type = new TypeToken<List<DistrictResponse>>() {
                        }.getType();
                        ArrayList<DistrictResponse> districtList = (ArrayList<DistrictResponse>) GsonUtils.jsonToArray( basicResponse.getResponse(), type );
                        districtData.postValue( districtList );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_districtList ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_districtList ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_districtList ) );
                }
            }
        } );
    }

    public void getSubDistricts(String districtId, Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.loading ) );

        EndPoints.getMandalsByDistrictId( districtId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        Type type = new TypeToken<List<SubDistrictResponse>>() {
                        }.getType();
                        ArrayList<SubDistrictResponse> subdistrictList = (ArrayList<SubDistrictResponse>) GsonUtils.jsonToArray( basicResponse.getResponse(), type );
                        subDistrictData.postValue( subdistrictList );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_mandalList ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_mandalList ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_mandalList ) );
                }
            }
        } );
    }

    public void getVillages(String subDistrictId, Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.loading ) );

        EndPoints.getVillagesByMandalId( subDistrictId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        Type type = new TypeToken<List<VillageResponse>>() {
                        }.getType();
                        ArrayList<VillageResponse> villageList = (ArrayList<VillageResponse>) GsonUtils.jsonToArray( basicResponse.getResponse(), type );
                        villageData.postValue( villageList );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_villagelist ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_villagelist ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_villagelist ) );
                }
            }
        } );
    }

    public void farmerRegistration(String authToken, String farmerId, FarmerRequest farmerRequest, Context context) {

        showProgressDialog( context, context.getResources().getString( R.string.signing_up ) );

        EndPoints.farmerSignUp( authToken, farmerId, farmerRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NotNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        Farmer farmer = GsonUtils.fromGson( basicResponse.getResponse(), Farmer.class );
                        try (Realm realm = Realm.getDefaultInstance()) {
                            //  update verification Response...
                            realm.executeTransactionAsync( realm1 -> realm1.copyToRealmOrUpdate( farmer ), () -> {
                                //  goto Crop Details Activity...
                                IntentUtils.PassIntent( context, CropDetails.class );
                                ((Activity) (context)).finish();
                            } );
                        }
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_signup ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_signup ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_signup ) );
                }
            }
        } );
    }

    public void updateCrop(String authToken, String farmerId, CropDataUpdateRequest cropDataUpdateRequest, Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.updating ) );

        EndPoints.updateCrop( authToken, farmerId, cropDataUpdateRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NotNull Response<BasicResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        UpdateCropResponse updateCropResponse = GsonUtils.fromGson( basicResponse.getResponse(), UpdateCropResponse.class );
                        List<CropData> cropData = updateCropResponse.getUpdatedCrop();
                        try (Realm realm = Realm.getDefaultInstance()) {
                            realm.executeTransactionAsync( realm1 -> {
                                Farmer farmer = realm1.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();
                                RealmList<CropData> cropDataRealmList = farmer.getCropData();
                                cropDataRealmList.deleteLastFromRealm();
                                cropDataRealmList.addAll( cropData );
                                realm1.copyToRealmOrUpdate( farmer );
                            }, () -> IntentUtils.PassIntent( context, HomeScreenActivity.class ) );
                        }
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.toast_error_in_crop_updating ) );
                    }

                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.toast_error_in_crop_updating ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.toast_error_in_crop_updating ) );
                }
            }
        } );
    }

    public void addFarmCoordinatesData(String authToken, String farmerId, FarmCoordinatesData farmCoordinatesData, Context context) {

        EndPoints.addFarmCoordinatesData( authToken, farmerId, farmCoordinatesData, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NotNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        AddFarmCoordinatesResponse addFarmCoordinatesResponse = GsonUtils.fromGson( basicResponse.getResponse(), AddFarmCoordinatesResponse.class );
                        try (Realm realm = Realm.getDefaultInstance()) {
                            //  update verification Response...
                            realm.executeTransactionAsync( realm1 -> realm1.copyToRealmOrUpdate( addFarmCoordinatesResponse ), () -> {
                                startWeatherForeCastJob();
                            } );
                        }
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_add_farm_coordinates ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_add_farm_coordinates ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_add_farm_coordinates ) );
                }
            }
        } );
    }

    private void getFarmCoordinates(String authToken, String farmerId, Context context) {
        EndPoints.getFarmCoordinates( authToken, farmerId, new Callback<BasicResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                        if (response.isSuccessful()) {
                            BasicResponse basicResponse = response.body();
                            assert basicResponse != null;
                            ErrorCode errorCode = basicResponse.getErrorCode();
                            ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                            if (errorList.isEmpty()) {
                                try (Realm realm = Realm.getDefaultInstance()) {
                                    AddFarmCoordinatesResponse farmCoordinatesResponse = GsonUtils.fromGson( basicResponse.getResponse(), AddFarmCoordinatesResponse.class );
                                    realm.executeTransactionAsync( realm1 -> realm1.copyToRealmOrUpdate( farmCoordinatesResponse ), () -> {
                                        if (farmCoordinatesResponse.isPlotAdded()) {
                                            startWeatherForeCastJob();
                                        } else {
                                            dismissDialog();
                                            //  goto home Activity
                                            IntentUtils.PassIntent( context, HomeScreenActivity.class );
                                        }
                                    } );
                                }
                            } else {
                                dismissDialog();
                                //  show error dialog here..., the error could be from one of the Error Code...
                                showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_farm_coordinates ) );
                            }
                        } else {
                            dismissDialog();
                            showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_farm_coordinates ) );
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {
                        dismissDialog();
                        //  show UI for API Failure...
                        if (t instanceof NoConnectivityException) {
                            //  This is where it throws No Internet connectivity error...
                            //  show some UI here, sefu...
                            IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                        }
                        //  there's some other error, not network connectivity issue...
                        else {
                            showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_farm_coordinates ) );
                        }
                    }
                }
        );
    }

    public void paymentVerification(String authToken, String farmerId, Context context) {
        EndPoints.getPaymentDetails( authToken, farmerId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NotNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    if (!NetworkErrorHandlingUtils.ErrorCheck( errorCode ).isEmpty()) {
                        //  update farmerMutableLiveData Details...
                        PaymentLogResponse paymentLogResponse = GsonUtils.fromGson( basicResponse.getResponse(), PaymentLogResponse.class );

                        //  for production environment...
                        PaytmPGService Service = PaytmPGService.getStagingService();
                        HashMap<String, String> paramMap = new HashMap<>();
                        paramMap.put( "MID", paymentLogResponse.getMerchantId() );
                        // Key in your staging and production MID available in your dashboard
                        paramMap.put( "ORDER_ID", paymentLogResponse.getOrderId() );
                        paramMap.put( "CUST_ID", paymentLogResponse.getCustomerId() );
                        paramMap.put( "CHANNEL_ID", paymentLogResponse.getChannelId().name() );
                        paramMap.put( "TXN_AMOUNT", paymentLogResponse.getAmountPaid() );
                        paramMap.put( "WEBSITE", paymentLogResponse.getWebsite().name() );
                        // This is the staging value. Production value is available in your dashboard
                        paramMap.put( "INDUSTRY_TYPE_ID", paymentLogResponse.getIndustry_type().name() );
                        // This is the staging value. Production value is available in your dashboard
                        paramMap.put( "CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + paymentLogResponse.getOrderId() );
                        paramMap.put( "CHECKSUMHASH", paymentLogResponse.getChecksumHash() );
                        PaytmOrder Order = new PaytmOrder( paramMap );
                        Service.initialize( Order, null );
                        Service.startPaymentTransaction( context, true, true, new PaytmPaymentTransactionCallback() {
                            /*Call Backs*/
                            public void someUIErrorOccurred(String inErrorMessage) {

                            }

                            public void onTransactionResponse(Bundle inResponse) {
                                String bankname = inResponse.getString( "BANKNAME" );
                                String orderId = inResponse.getString( "ORDERID" );
                                String amountTransferred = inResponse.getString( "TXNAMOUNT" );
                                String transferDate = inResponse.getString( "TXNDATE" );
                                String merchantId = inResponse.getString( "MID" );
                                String transferId = inResponse.getString( "TXNID" );
                                String paymentMode = inResponse.getString( "PAYMENTMODE" );
                                String bankId = inResponse.getString( "BANKTXNID" );
                                String currency = inResponse.getString( "CURRENCY" );
                                String responseMsg = inResponse.getString( "RESPMSG" );

                                PaymentRequest paymentRequest = new PaymentRequest();
                                paymentRequest.setAmountTransferred( amountTransferred );
                                paymentRequest.setBankId( bankId );
                                paymentRequest.setBankname( bankname );
                                paymentRequest.setCurrency( currency );
                                paymentRequest.setMerchantId( merchantId );
                                paymentRequest.setOrderId( orderId );
                                paymentRequest.setTransferDate( transferDate );
                                paymentRequest.setResponseMsg( responseMsg );
                                paymentRequest.setTransferId( transferId );
                                paymentRequest.setPaymentMode( paymentMode );

                                //  log Payment...
                                EndPoints.logPayment( authToken, farmerId, paymentRequest, new Callback<BasicResponse>() {
                                    @Override
                                    public void onResponse(@NonNull Call<BasicResponse> call, @NotNull Response<BasicResponse> response) {
                                        if (response.isSuccessful()) {
                                            BasicResponse basicResponse = response.body();
                                            assert basicResponse != null;
                                            ErrorCode errorCode = basicResponse.getErrorCode();
                                            if (!NetworkErrorHandlingUtils.ErrorCheck( errorCode ).isEmpty()) {
                                                PaymentResponse paymentResponse = GsonUtils.fromGson( basicResponse.getResponse(), PaymentResponse.class );

                                            } else {
                                                //  show error dialog here..., the error could be from one of the Error Code...

                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NonNull Call<BasicResponse> call, @NotNull Throwable t) {
                                        //  show UI for API Failure...
                                        if (t instanceof NoConnectivityException) {
                                            //  This is where it throws No Internet connectivity error...
                                            //  show some UI here, sefu...
                                            IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                                        }
                                        //  there's some other error, not network connectivity issue...
                                        else {

                                        }
                                    }
                                } );
                            }

                            public void networkNotAvailable() {

                            }

                            public void clientAuthenticationFailed(String inErrorMessage) {

                            }

                            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {

                            }

                            public void onBackPressedCancelTransaction() {

                            }

                            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {

                            }
                        } );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NotNull Throwable t) {
                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {

                }
            }
        } );
    }

    public void getWeatherForecast(String appID, String numberOfDays, double latitude, double longitude, Context context) {
        EndPoints.getWeatherForecast( appID, numberOfDays, latitude, longitude, new Callback<ForecastResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForecastResponse> call, @NotNull Response<ForecastResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    ForecastResponse forecastResponse = response.body();
                    if (forecastResponse != null) {
                        try (Realm realm = Realm.getDefaultInstance()) {
                            //  update verification Response...
                            realm.executeTransactionAsync( realm1 -> realm1.copyToRealmOrUpdate( forecastResponse ), () -> {
                                //  goto home Activity
                                IntentUtils.PassIntent( context, HomeScreenActivity.class );
                            } );
                        }
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_in_fetching_forecast ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForecastResponse> call, @NotNull Throwable t) {
                dismissDialog();
                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_in_fetching_forecast ) );
                }
            }
        } );
    }

    public void logout(String authToken, String farmerId, Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.logout ) );

        EndPoints.logout( authToken, farmerId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        try (Realm realm = Realm.getDefaultInstance()) {
                            realm.executeTransactionAsync( realm1 -> {
                                realm1.deleteAll();
                                //  remove all works
                                WorkManager.getInstance().cancelAllWork();
                            }, () -> {
                                IntentUtils.PassIntent( context, SplashActivity.class );
                                ((Activity) context).finish();
                            } );
                        }
                    } else {
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_logout ) );
                    }
                } else {
                    //  show error dialog here..., the error could be from one of the Error Code...
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_logout ) );
                }
            }


            @Override
            public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_logout ) );
                }
            }
        } );
    }

    public void farmerProfileUpdate(String authToken, String farmerId, UpdateFarmerRequest updateFarmerRequest, Context context) {

        showProgressDialog( context, ApplicationClass.getContext().getResources().getString( R.string.updating ) );

        EndPoints.farmerProfileUpdate( authToken, farmerId, updateFarmerRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {

                dismissDialog();

                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        try (Realm realm = Realm.getDefaultInstance()) {
                            UpdateProfileReponse updateProfileReponse = GsonUtils.fromGson( basicResponse.getResponse(), UpdateProfileReponse.class );
                            //  update profile...
                            realm.executeTransactionAsync( realm1 -> realm1.insertOrUpdate( updateProfileReponse.getFarmer() ) );

                            IntentUtils.PassIntent( context, HomeScreenActivity.class );
                            ((Activity) context).finish();
                        }
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_update_profile ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_update_profile ) );
                }
            }

            @Override
            public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_update_profile ) );
                }
            }
        } );
    }

    public void getIncomeComparisonSheet(String authToken, String farmerId, GetComparisonSheetRequest getComparisonSheetRequest, Context context) {
        EndPoints.getIncomeComparisonSheet( authToken, farmerId, getComparisonSheetRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    List<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        IncomeResponse incomeResponse = GsonUtils.fromGson( basicResponse.getResponse(), IncomeResponse.class );
                        incomeResponseMutableLiveData.postValue( incomeResponse );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_income_comparison_sheet ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_income_comparison_sheet ) );
                }
            }

            @Override
            public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {
                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_income_comparison_sheet ) );
                }
            }
        } );
    }

    public void getRecommendation(String authToken, String farmerId, Context context) {
        EndPoints.getRecommendation( authToken, farmerId, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        RecommendationSheetResponse sheetResponse = GsonUtils.fromGson( basicResponse.getResponse(), RecommendationSheetResponse.class );
                        recommendationSheetData.postValue( sheetResponse );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_recommendation_sheet ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_recommendation_sheet ) );
                }
            }

            @Override
            public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_recommendation_sheet ) );
                }
            }
        } );
    }

    public void getYieldComparisonSheet(String authToken, String farmerId, GetComparisonSheetRequest getComparisonSheetRequest, Context context) {
        EndPoints.getYeildComparisonSheet( authToken, farmerId, getComparisonSheetRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    List<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        YieldResponse yieldResponse = GsonUtils.fromGson( basicResponse.getResponse(), YieldResponse.class );
                        yieldResponseMutableLiveData.postValue( yieldResponse );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_comparision_sheet ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_comparision_sheet ) );
                }
            }

            @Override
            public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {
                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_comparision_sheet ) );
                }
            }
        } );
    }

    public void getSoilTestResult(String authToken, String farmerId, String landSurveyNumber, Context context) {
        EndPoints.getSoilTestData( authToken, farmerId, landSurveyNumber, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        SoilTestDataResponse soilTestDataResponse = GsonUtils.fromGson( basicResponse.getResponse(), SoilTestDataResponse.class );
                        soilTestresult.postValue( soilTestDataResponse );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_soil_test_result ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_soil_test_result ) );
                }
            }

            @Override
            public void onFailure(@NotNull Call<BasicResponse> call, @NotNull Throwable t) {

                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_cannot_fetch_soil_test_result ) );
                }
            }
        } );
    }

    public void addBankDetails(String authToken, String farmerId, BankAccountDetailRequest bankAccountDetailRequest, Context context) {
        showProgressDialog( context, context.getResources().getString( R.string.updating ) );
        EndPoints.addBankDetails( authToken, farmerId, bankAccountDetailRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        try (Realm realm = Realm.getDefaultInstance()) {
                            BankDetailsResponse bankDetailsResponse = GsonUtils.fromGson( basicResponse.getResponse(), BankDetailsResponse.class );
                            realm.executeTransactionAsync( realm1 -> realm1.copyToRealmOrUpdate( bankDetailsResponse ), () -> IntentUtils.PassIntent( context, HomeScreenActivity.class ) );
                        }
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_bank_details ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_bank_details ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_bank_details ) );
                }
            }
        } );
    }

    public void setPlantingDate(String authToken, String farmerId, PlantingDateRequest plantingDateRequest, Context context) {
        showProgressDialog( context, context.getResources().getString( R.string.updating ) );
        EndPoints.setPlantDate( authToken, farmerId, plantingDateRequest, new Callback<BasicResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasicResponse> call, @NonNull Response<BasicResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {
                    BasicResponse basicResponse = response.body();
                    assert basicResponse != null;
                    ErrorCode errorCode = basicResponse.getErrorCode();
                    ArrayList<String> errorList = NetworkErrorHandlingUtils.ErrorCheck( errorCode );
                    if (errorList.isEmpty()) {
                        Farmer farmer = GsonUtils.fromGson( basicResponse.getResponse(), Farmer.class );
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        Farmer dbFarmer = realm.where( Farmer.class ).equalTo( Farmer.Constants.FARMER_ID, farmerId ).findFirst();
                        dbFarmer.setPlantingDate( farmer.getPlantingDate() );
                        realm.commitTransaction();
                        realm.close();
                        replaceFragment( context );
                    } else {
                        //  show error dialog here..., the error could be from one of the Error Code...
                        showAlertDialogWithOneButton( context, context.getString( R.string.error_updating_planting_date ) );
                    }
                } else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_updating_planting_date ) );
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasicResponse> call, @NonNull Throwable t) {
                dismissDialog();

                //  show UI for API Failure...
                if (t instanceof NoConnectivityException) {
                    //  This is where it throws No Internet connectivity error...
                    //  show some UI here, sefu...
                    IntentUtils.PassIntent( context, NetworkErrorActivity.class );
                }
                //  there's some other error, not network connectivity issue...
                else {
                    showAlertDialogWithOneButton( context, context.getString( R.string.error_updating_planting_date ) );
                }
            }
        } );
    }

    //  run this method only if the coordinates are Added/Updated...
    private void startWeatherForeCastJob() {

        //  check if periodic work is enqueued or not, and if it isn't then start periodic job...
        WorkManager workManager = WorkManager.getInstance();

        //  initialize periodic workRequest...
        PeriodicWorkRequest periodicWorkRequest = setupPeriodicTask();

        //  enqueue new periodic job...
        workManager.enqueueUniquePeriodicWork( ConstantUtils.getPeriodicWorkName(), ExistingPeriodicWorkPolicy.REPLACE, periodicWorkRequest );

    }

    private PeriodicWorkRequest setupPeriodicTask() {

        Constraints.Builder constraintBuilder = new Constraints.Builder();
        constraintBuilder.setRequiredNetworkType( NetworkType.CONNECTED );
        Constraints constraints = constraintBuilder.build();

        //  Periodic Work Request...
        PeriodicWorkRequest.Builder builder = new PeriodicWorkRequest.Builder( PeriodicWork.class, INTERVAL, TimeUnit.DAYS ).setConstraints( constraints ).addTag( ConstantUtils.getPeriodicWorkName() );
        builder.setBackoffCriteria( BackoffPolicy.LINEAR, BACKOFF, TimeUnit.MINUTES );
        return builder.build();
    }

    public MutableLiveData<ArrayList<StatesResponse>> getStatesList() {
        return stateData;
    }

    public MutableLiveData<ArrayList<DistrictResponse>> getDistrictsList() {
        return districtData;
    }

    public MutableLiveData<ArrayList<SubDistrictResponse>> getSubDistrictsList() {
        return subDistrictData;
    }

    public MutableLiveData<IncomeResponse> getIncomeResponseMutableLiveData() {
        return incomeResponseMutableLiveData;
    }

    public MutableLiveData<YieldResponse> getYieldResponseMutableLiveData() {
        return yieldResponseMutableLiveData;
    }

    public MutableLiveData<ArrayList<VillageResponse>> getVillagesList() {
        return villageData;
    }

    public MutableLiveData<RecommendationSheetResponse> getRecommendationSheetData() {
        return recommendationSheetData;
    }

    public MutableLiveData<SoilTestDataResponse> getSoilTestresult() {
        return soilTestresult;
    }

    //  this will start Showing Progress Dialog...
    private void showProgressDialog(Context context, String message) {
        dialog = new ProgressDialog( context );
        dialog.setMessage( message );
        dialog.show();
    }

    //  this will stop Showing Progress Dialog...
    private void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    //  show AlertDialog
    private void showAlertDialogWithOneButton(Context context, String message) {
        DialogInterface.OnClickListener onClickListener = (dialog, which) -> dialog.dismiss();
        AlertUtils alertUtils = AlertUtils.getInstance( null );
        alertUtils.showDialogWithOneBtn( context, message, onClickListener );
    }


    private void replaceFragment(Context context) {
        Fragment fragment = new NutrientManagementFragment();
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations( R.anim.slide_in_left, R.anim.slide_out_right );
        ft.replace( R.id.main_fragment_placeholder, fragment );
        ft.commit();
    }

}
