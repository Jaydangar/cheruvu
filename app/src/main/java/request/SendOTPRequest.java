package request;

import java.util.ArrayList;
import java.util.List;

import application.ApplicationClass;
import enums.TokenType;
import in.cheruvu.cheruvu.R;

public class SendOTPRequest {

    private static final String TAG = SendOTPRequest.class.getSimpleName();

    private String userId;
    private String countryCode;
    private String phoneNumber;
    private String appSignature;
    private TokenType type = TokenType.OTP;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public TokenType getType() {
        return type;
    }

    public String getAppSignature() {
        return appSignature;
    }

    public void setAppSignature(String appSignature) {
        this.appSignature = appSignature;
    }

    //  verification
    public List<String> verify(SendOTPRequest sendOTPRequest) {
        List<String> errors = new ArrayList<>();
        if (sendOTPRequest.getType() == null) {
            errors.add( ApplicationClass.getContext().getString(R.string.invalid_type));
        }
        if (sendOTPRequest.countryCode == null || sendOTPRequest.getCountryCode().isEmpty()) {
            errors.add( ApplicationClass.getContext().getString(R.string.invalid_country_code));
        }
        if (sendOTPRequest.appSignature == null || sendOTPRequest.getAppSignature().isEmpty()) {
            errors.add( ApplicationClass.getContext().getString(R.string.invalid_signature) );
        }
        if (sendOTPRequest.phoneNumber == null || sendOTPRequest.getPhoneNumber().isEmpty()) {

            errors.add( ApplicationClass.getContext().getString(R.string.invalid_phone_number) );
        }
        return errors;
    }

}