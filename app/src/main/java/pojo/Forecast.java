package pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Forecast extends RealmObject {

	@SerializedName("forecastday")
	private RealmList<ForecastdayItem> forecastday;

	public void setForecastday(RealmList<ForecastdayItem> forecastday){
		this.forecastday = forecastday;
	}

	public RealmList<ForecastdayItem> getForecastday(){
		return forecastday;
	}

	@Override
 	public String toString(){
		return 
			"Forecast{" + 
			"forecastday = '" + forecastday + '\'' + 
			"}";
		}
}