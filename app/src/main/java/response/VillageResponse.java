package response;

import com.google.gson.annotations.SerializedName;

public class VillageResponse {

    @SerializedName("mandalId")
    private String mandalId;

    @SerializedName("lastUpdated")
    private long lastUpdated;

    @SerializedName("creationTime")
    private long creationTime;

    @SerializedName("createdBy")
    private String createdBy;

    @SerializedName("id")
    private String id;

    @SerializedName("state")
    private String state;

    @SerializedName("villageName")
    private String villageName;

    public void setMandalId(String mandalId) {
        this.mandalId = mandalId;
    }

    public String getMandalId() {
        return mandalId;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        if (this.createdBy == null)
            return "";
        else
            return createdBy;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        if (this.id == null)
            return "";
        else
            return id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        if (this.state == null)
            return "";
        else
            return state;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getVillageName() {
        if (this.villageName == null)
            return "";
        else
            return villageName;
    }

    @Override
    public String toString() {
        return
                "VillageResponse{" +
                        "mandalId = '" + mandalId + '\'' +
                        ",lastUpdated = '" + lastUpdated + '\'' +
                        ",creationTime = '" + creationTime + '\'' +
                        ",createdBy = '" + createdBy + '\'' +
                        ",id = '" + id + '\'' +
                        ",state = '" + state + '\'' +
                        ",villageName = '" + villageName + '\'' +
                        "}";
    }
}