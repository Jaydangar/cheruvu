package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentFertilizerRecommendationDaySixtyBinding;
import io.realm.Realm;
import network.NetworkResource;
import response.RecommendationSheetResponse;
import utility.ImageSetterUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FertilizerRecommendationDaySixty extends Fragment {

    public FertilizerRecommendationDaySixty() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentFertilizerRecommendationDaySixtyBinding inflate = DataBindingUtil.inflate( inflater, R.layout.fragment_fertilizer_recommendation_day_sixty, container, false );

        NetworkResource.getInstance().getRecommendationSheetData().observe( this, recommendationSheetResponse -> {
            double mop = recommendationSheetResponse.getMopByDay().getDay60();
            double ssp = recommendationSheetResponse.getSspByDay().getDay60();
            double urea = recommendationSheetResponse.getUreaByDay().getDay60();

            ImageSetterUtils.setImageAndText(getContext(),inflate.dapBagsImg, inflate.dapUsage, mop, "mop" );
            ImageSetterUtils.setImageAndText( getContext(),inflate.superBagsImg, inflate.superUsage, ssp, "ssp" );
            ImageSetterUtils.setImageAndText( getContext(),inflate.ureaBagsImg, inflate.ureaUsage, urea, "urea" );

            if(mop == 0.0){
                inflate.daySixtyPotashGroup.setVisibility( View.GONE );
            }
            if(ssp == 0.0){
                inflate.daySixtySuperGroup.setVisibility( View.GONE );
            }
            if(urea == 0.0){
                inflate.daySixtyUreaGroup.setVisibility( View.GONE );
            }
        } );

        return inflate.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
