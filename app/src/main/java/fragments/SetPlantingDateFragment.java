package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentSetPlantingDateBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetPlantingDateFragment extends Fragment {

    private static final String TAG = "SetPlantingDateFragment";

    public SetPlantingDateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSetPlantingDateBinding inflate = DataBindingUtil.inflate( inflater, R.layout.fragment_set_planting_date, container, false );
        Button setPlantingDateButton = inflate.idSetPlantingDate;

        // Initialize a new date picker dialog fragment
        DialogFragment dFragment = new DatePickerFragment();

        setPlantingDateButton.setOnClickListener( v -> {
            // Show the date picker dialog fragment
            dFragment.show( getFragmentManager(), "Date Picker" );
        } );

        return inflate.getRoot();
    }
}
