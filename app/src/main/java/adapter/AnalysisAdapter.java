package adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import fragments.ComparativeIncomeAnalysisFragment;
import fragments.ComparativeYieldAnalysisFragment;

public class AnalysisAdapter extends FragmentStatePagerAdapter {

    public AnalysisAdapter(FragmentManager fm) {
        super( fm );
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ComparativeYieldAnalysisFragment();
            case 1:
                return new ComparativeIncomeAnalysisFragment();
            default:
                return new ComparativeYieldAnalysisFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
