package utility;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import enums.Units;
import enums.WindSpeed;
import in.cheruvu.cheruvu.R;
import io.realm.Realm;
import network.NetworkResource;
import pojo.LatLng;

public class PeriodicWork extends Worker {

    private static final String TAG = "PeriodicWork";

    private static final String numberOfDays = "7";

    private Context context;

    public PeriodicWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super( context, workerParams );
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        String weatherId = context.getString( R.string.weather_app_id );
        try (Realm realm = Realm.getDefaultInstance()) {
            //  update verification Response...
            LatLng latLng = QueryUtils.getLocation( realm );
            if (latLng != null) {
                NetworkResource.getInstance().getWeatherForecast( weatherId,numberOfDays,latLng.getLatitude(),latLng.getLongitude(),context );
            }
        }
        return Result.success();
    }
}
