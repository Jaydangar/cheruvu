package fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import adapter.AnalysisAdapter;
import in.cheruvu.cheruvu.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ViewPager.OnPageChangeListener {

    //menu text view
    private TextView yieldAnalysisTextView, incomeAnalysisTextView;

    //main view pager
    private ViewPager viewPager;

    // pager adapter
    private AnalysisAdapter mAnalysisPagerViewAdapter;

    private Context context;

    public HomeFragment() {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        mAnalysisPagerViewAdapter = new AnalysisAdapter(getFragmentManager());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        yieldAnalysisTextView = (TextView) view.findViewById(R.id.idComparativeYieldAnalysis);
        incomeAnalysisTextView = (TextView) view.findViewById(R.id.idComparativeIncomeAnalysis);
        yieldAnalysisTextView.setTextSize(16);

        incomeAnalysisTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGrey));

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        viewPager.setAdapter(mAnalysisPagerViewAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(0, true);

        //viewPager label listener
        yieldAnalysisTextView.setOnClickListener(v -> viewPager.setCurrentItem(0));
        incomeAnalysisTextView.setOnClickListener(v -> viewPager.setCurrentItem(1));

        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        viewPager.setCurrentItem(position);
        if (position == 0) {
            yieldAnalysisTextView.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
            yieldAnalysisTextView.setTextSize(17);
            incomeAnalysisTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGrey));
            incomeAnalysisTextView.setTextSize(16);
        }

        if (position == 1) {
            incomeAnalysisTextView.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
            incomeAnalysisTextView.setTextSize(17);
            yieldAnalysisTextView.setTextColor(ContextCompat.getColor(context, R.color.colorGrey));
            yieldAnalysisTextView.setTextSize(16);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
