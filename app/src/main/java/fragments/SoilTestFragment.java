package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentEmptySoilTestBinding;
import io.realm.Realm;
import network.NetworkResource;
import response.SoilTestDataResponse;
import utility.QueryUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SoilTestFragment extends Fragment {

    private Realm realm;

    private static final String TAG = "SoilTestFragment";
    private View view;

    Fragment fragment = null;


    public SoilTestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        realm = Realm.getDefaultInstance();

        // Inflate the layout for this fragment
        HashMap<String, String> credentials = QueryUtils.getCredentials(realm);
        String farmerId = credentials.get("farmerId");
        String authToken = credentials.get("authToken");

        NetworkResource.getInstance().getSoilTestResult(authToken, farmerId, "", getContext());

        MutableLiveData<SoilTestDataResponse> soilTestresult = NetworkResource.getInstance().getSoilTestresult();

        FragmentEmptySoilTestBinding originalInflate = DataBindingUtil.inflate(inflater, R.layout.fragment_empty_soil_test, container, false);
        view = originalInflate.getRoot();

        soilTestresult.observe(this, soilTestDataResponse -> {
            if (soilTestDataResponse.getSoilTestRecommendation() != null ) {
                //  change fragment to SoilTestResult
                fragment = ShowSoilTestResult.newInstance(soilTestDataResponse);
                replaceFragment(fragment);
            }
        });

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.main_fragment_placeholder, fragment);
        ft.commit();
    }
}
