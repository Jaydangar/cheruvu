package fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.Group;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.FragmentFertilizerRecommendationDayNinetyBinding;
import network.NetworkResource;
import utility.ImageSetterUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FertilizerRecommendationDayNinety extends Fragment {

    Group ureaGroup, sspGroup, potashGroup;
    public static double mop, ssp, urea;

    public FertilizerRecommendationDayNinety() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentFertilizerRecommendationDayNinetyBinding inflate = DataBindingUtil.inflate(inflater, R.layout.fragment_fertilizer_recommendation_day_ninety, container, false);

        ureaGroup = inflate.dayNinetyUreaGroup;
        sspGroup = inflate.dayNinetySuperGroup;
        potashGroup = inflate.dayNinetyPotashGroup;

        NetworkResource.getInstance().getRecommendationSheetData().observe(this, recommendationSheetResponse -> {
            mop = recommendationSheetResponse.getMopByDay().getDay90();
            ssp = recommendationSheetResponse.getSspByDay().getDay90();
            urea = 1.0;

            ImageSetterUtils.setImageAndText(getContext(),inflate.dapBagsImg, inflate.dapUsage, mop, "mop");
            ImageSetterUtils.setImageAndText(getContext(),inflate.superBagsImg, inflate.superUsage, ssp, "ssp");
            ImageSetterUtils.setImageAndText(getContext(),inflate.ureaBagsImg, inflate.ureaUsage, urea, "urea");

            if(mop == 0.0){
                potashGroup.setVisibility( View.GONE );
            }
            if(ssp == 0.0){
                sspGroup.setVisibility( View.GONE );
            }
            if(urea == 0.0){
                ureaGroup.setVisibility( View.GONE );
            }
        } );

        return inflate.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
