package response;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;


public class MopByDay{

	@SerializedName("day_0")
	private double day0;

	@SerializedName("day_30")
	private double day30;

	@SerializedName("day_90")
	private double day90;

	@SerializedName("day_60")
	private double day60;

	public void setDay0(double day0){
		this.day0 = day0;
	}

	public double getDay0(){
		return day0;
	}

	public void setDay30(double day30){
		this.day30 = day30;
	}

	public double getDay30(){
		return day30;
	}

	public void setDay90(double day90){
		this.day90 = day90;
	}

	public double getDay90(){
		return day90;
	}

	public void setDay60(double day60){
		this.day60 = day60;
	}

	public double getDay60(){
		return day60;
	}

	@Override
 	public String toString(){
		return 
			"MopByDay{" + 
			"day_0 = '" + day0 + '\'' + 
			",day_30 = '" + day30 + '\'' + 
			",day_90 = '" + day90 + '\'' + 
			",day_60 = '" + day60 + '\'' + 
			"}";
		}
}