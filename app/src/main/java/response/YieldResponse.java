package response;

import com.google.gson.annotations.SerializedName;

public class YieldResponse{

    @SerializedName("maxYield")
    private double maxYield;

    @SerializedName("averageYield")
    private double averageYield;

    @SerializedName("personalYield")
    private double personalYield;

    public void setMaxYield(double maxYield) {
        this.maxYield = maxYield;
    }

    public double getMaxYield() {
        return maxYield;
    }

    public void setAverageYield(double averageYield) {
        this.averageYield = averageYield;
    }

    public double getAverageYield() {
        return averageYield;
    }

    public void setPersonalYield(double personalYield) {
        this.personalYield = personalYield;
    }

    public double getPersonalYield() {
        return personalYield;
    }

    @Override
    public String toString() {
        return
                "YieldResponse{" +
                        "maxYield = '" + maxYield + '\'' +
                        ",averageYield = '" + averageYield + '\'' +
                        ",personalYield = '" + personalYield + '\'' +
                        "}";
    }

    public static class Constants {
        public static final String ID = "id";
    }
}