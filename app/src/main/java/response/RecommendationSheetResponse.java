package response;

import com.google.gson.annotations.SerializedName;

import io.realm.annotations.PrimaryKey;

public class RecommendationSheetResponse {

    @PrimaryKey
    private String id;

    @SerializedName("k2o")
    private double k2o;

    @SerializedName("sspByDay")
    private SspByDay sspByDay;

    @SerializedName("p2o5")
    private double p2o5;

    @SerializedName("mopByDay")
    private MopByDay mopByDay;

    @SerializedName("state")
    private String state;

    @SerializedName("n")
    private double N;

    @SerializedName("ureaByDay")
    private UreaByDay ureaByDay;

    private ZincByDay zincByDay;
    private BoraxByDay boraxByDay;

    public void setK2o(double k2o) {
        this.k2o = k2o;
    }

    public double getK2o() {
        return k2o;
    }

    public void setSspByDay(SspByDay sspByDay) {
        this.sspByDay = sspByDay;
    }

    public SspByDay getSspByDay() {
        return sspByDay;
    }

    public void setP2o5(double p2o5) {
        this.p2o5 = p2o5;
    }

    public double getP2o5() {
        return p2o5;
    }

    public void setMopByDay(MopByDay mopByDay) {
        this.mopByDay = mopByDay;
    }

    public MopByDay getMopByDay() {
        return mopByDay;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        if (this.state == null)
            return "";
        else
            return state;
    }

    public void setN(double N) {
        this.N = N;
    }

    public double getN() {
        return N;
    }

    public void setUreaByDay(UreaByDay ureaByDay) {
        this.ureaByDay = ureaByDay;
    }

    public UreaByDay getUreaByDay() {
        return ureaByDay;
    }

    public ZincByDay getZincByDay() {
        return zincByDay;
    }

    public void setZincByDay(ZincByDay zincByDay) {
        this.zincByDay = zincByDay;
    }

    public BoraxByDay getBoraxByDay() {
        return boraxByDay;
    }

    public void setBoraxByDay(BoraxByDay boraxByDay) {
        this.boraxByDay = boraxByDay;
    }

    @Override
    public String toString() {
        return
                "RecommendationSheetResponse{" +
                        "k2o = '" + k2o + '\'' +
                        ",sspByDay = '" + sspByDay + '\'' +
                        ",p2o5 = '" + p2o5 + '\'' +
                        ",mopByDay = '" + mopByDay + '\'' +
                        ",state = '" + state + '\'' +
                        ",n = '" + N + '\'' +
                        ",ureaByDay = '" + ureaByDay + '\'' +
                        "}";
    }

    public static class Constants {
        public static final String ID = "id";
    }
}