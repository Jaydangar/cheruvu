package pojo;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Farmer extends RealmObject {

    @PrimaryKey
    private String id;
    private String farmerReferenceId;
    private String cropId;
    private String nomineeName;
    private String stateName;
    private String city;
    private String villageName;
    private String address;
    private String mandalName;
    private Boolean smartPhoneUser;
    private String aadharNumber;
    private String phoneNumber;
    private String stateId;
    private String firstName;
    private String districtId;
    private String mandalId;
    private String villageId;
    private Boolean registered = false;
    private Boolean oneTimeDataEntered = false;
    private String countryCode;
    private String authToken;
    private String plantingDate;
    private RealmList<CropData> cropData;

    public String getPlantingDate() {
        if (this.plantingDate == null)
            return "";
        else
            return plantingDate;
    }

    public void setPlantingDate(String plantingDate) {
        this.plantingDate = plantingDate;
    }

    public String getMandalName() {
        if (this.mandalName == null)
            return "";
        else
            return mandalName;
    }

    public void setMandalName(String mandalName) {
        this.mandalName = mandalName;
    }

    public String getId() {
        if (this.id == null)
            return "";
        else
            return id;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthToken() {
        if (this.authToken == null)
            return "";
        else
            return authToken;

    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getCountryCode() {
        if (this.countryCode == null)
            return "";
        else
            return countryCode;

    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        if (this.phoneNumber == null)
            return "";
        else
            return phoneNumber;

    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        if (this.firstName == null)
            return "";
        else
            return firstName;

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Boolean getRegistered() {
        if (this.registered == null)
            return false;
        else
            return registered;

    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }

    public String getFarmerReferenceId() {
        if (this.farmerReferenceId == null)
            return "";
        else
            return farmerReferenceId;

    }

    public void setFarmerReferenceId(String farmerReferenceId) {
        this.farmerReferenceId = farmerReferenceId;
    }

    public String getCropId() {
        if (this.cropId == null)
            return "";
        else
            return cropId;

    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getNomineeName() {
        if (this.nomineeName == null)
            return "";
        else
            return nomineeName;

    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getStateName() {
        if (this.stateName == null)
            return "";
        else
            return stateName;

    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCity() {
        if (this.city == null)
            return "";
        else
            return city;

    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVillageName() {
        if (this.villageName == null)
            return "";
        else
            return villageName;

    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getAddress() {
        if (this.address == null)
            return "";
        else
            return address;

    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getSmartPhoneUser() {
        if (this.smartPhoneUser == null)
            return false;
        else
            return smartPhoneUser;

    }

    public void setSmartPhoneUser(Boolean smartPhoneUser) {
        this.smartPhoneUser = smartPhoneUser;
    }

    public String getAadharNumber() {
        if (this.aadharNumber == null)
            return "";
        else
            return aadharNumber;

    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getStateId() {
        if (this.stateId == null)
            return "";
        else
            return stateId;

    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getDistrictId() {
        if (this.districtId == null)
            return "";
        else
            return districtId;

    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getMandalId() {
        if (this.mandalId == null)
            return "";
        else
            return mandalId;

    }

    public void setMandalId(String mandalId) {
        this.mandalId = mandalId;
    }

    public String getVillageId() {
        if (this.villageId == null)
            return "";
        else
            return villageId;

    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public Boolean getOneTimeDataEntered() {
        if (this.oneTimeDataEntered == null)
            return false;
        else
            return oneTimeDataEntered;

    }

    public void setOneTimeDataEntered(Boolean oneTimeDataEntered) {
        this.oneTimeDataEntered = oneTimeDataEntered;
    }

    public RealmList<CropData> getCropData() {
        return cropData;
    }

    public void setCropData(RealmList<CropData> cropData) {
        this.cropData = cropData;
    }

    public static class Constants {
        public static final String FARMER_ID = "id";
    }
}
