package response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import pojo.CropData;

public class UpdateCropResponse {

    @SerializedName("updatedCrop")
    private List<CropData> updatedCrop;

    public void setUpdatedCrop(List<CropData> updatedCrop) {
        this.updatedCrop = updatedCrop;
    }

    public List<CropData> getUpdatedCrop() {
        return updatedCrop;
    }

    @Override
    public String toString() {
        return
                "UpdateCropResponse{" +
                        "updatedCrop = '" + updatedCrop + '\'' +
                        "}";
    }
}