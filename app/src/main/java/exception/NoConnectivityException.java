package exception;

import java.io.IOException;

import application.ApplicationClass;
import in.cheruvu.cheruvu.R;

//  class used for no Connectivity
public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return ApplicationClass.getContext().getString( R.string.you_are_offline );
    }

}
