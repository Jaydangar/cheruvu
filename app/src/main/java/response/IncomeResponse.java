package response;

public class IncomeResponse{

    private double maxIncome;
    private double averageIncome;
    private double personalIncome;

    public void setMaxIncome(double maxIncome) {
        this.maxIncome = maxIncome;
    }

    public double getMaxIncome() {
        return maxIncome;
    }

    public void setAverageIncome(double averageIncome) {
        this.averageIncome = averageIncome;
    }

    public double getAverageIncome() {
        return averageIncome;
    }

    public void setPersonalIncome(double personalIncome) {
        this.personalIncome = personalIncome;
    }

    public double getPersonalIncome() {
        return personalIncome;
    }

    @Override
    public String toString() {
        return
                "IncomeResponse{" +
                        "maxIncome = '" + maxIncome + '\'' +
                        ",averageIncome = '" + averageIncome + '\'' +
                        ",personalIncome = '" + personalIncome + '\'' +
                        "}";
    }

    public static class Constants {
        public static final String ID = "id";
    }
}