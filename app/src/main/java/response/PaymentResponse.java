package response;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

public class PaymentResponse{

	@SerializedName("creationTime")
	private long creationTime;

	@SerializedName("orderId")
	private String orderId;

	@SerializedName("paymentMode")
	private String paymentMode;

	@SerializedName("transferDate")
	private String transferDate;

	@SerializedName("transferId")
	private String transferId;

	@SerializedName("lastUpdated")
	private long lastUpdated;

	@SerializedName("bankId")
	private String bankId;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("currency")
	private String currency;

	@SerializedName("bankname")
	private String bankname;

	@SerializedName("id")
	private String id;

	@SerializedName("state")
	private String state;

	@SerializedName("amountTransferred")
	private String amountTransferred;

	@PrimaryKey
	@SerializedName("customer_Id")
	private String customerId;

	@SerializedName("responseMsg")
	private String responseMsg;

	public void setCreationTime(long creationTime){
		this.creationTime = creationTime;
	}

	public long getCreationTime(){
		return creationTime;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		if (this.orderId== null)
			return "";
		else
			return orderId;
	}

	public void setPaymentMode(String paymentMode){
		this.paymentMode = paymentMode;
	}

	public String getPaymentMode(){
		if (this.paymentMode == null)
			return "";
		else
			return paymentMode;
	}

	public void setTransferDate(String transferDate){
		this.transferDate = transferDate;
	}

	public String getTransferDate(){
		if (this.transferDate == null)
			return "";
		else
			return transferDate;
	}

	public void setTransferId(String transferId){
		this.transferId = transferId;
	}

	public String getTransferId(){
		if (this.transferId == null)
			return "";
		else
			return transferId;
	}

	public void setLastUpdated(long lastUpdated){
		this.lastUpdated = lastUpdated;
	}

	public long getLastUpdated(){
		return lastUpdated;
	}

	public void setBankId(String bankId){
		this.bankId = bankId;
	}

	public String getBankId(){
		if (this.bankId == null)
			return "";
		else
			return bankId;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		if (this.merchantId == null)
			return "";
		else
			return merchantId;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		if (this.currency == null)
			return "";
		else
			return currency;
	}

	public void setBankname(String bankname){
		this.bankname = bankname;
	}

	public String getBankname(){
		if (this.bankname == null)
			return "";
		else
			return bankname;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		if (this.id == null)
			return "";
		else
			return id;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		if (this.state == null)
			return "";
		else
			return state;
	}

	public void setAmountTransferred(String amountTransferred){
		this.amountTransferred = amountTransferred;
	}

	public String getAmountTransferred(){
		if (this.amountTransferred== null)
			return "";
		else
			return amountTransferred;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		if (this.customerId == null)
			return "";
		else
			return customerId;
	}

	public void setResponseMsg(String responseMsg){
		this.responseMsg = responseMsg;
	}

	public String getResponseMsg(){
		if (this.responseMsg == null)
			return "";
		else
			return responseMsg;
	}

	@Override
 	public String toString(){
		return 
			"PaymentResponse{" + 
			"creationTime = '" + creationTime + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",paymentMode = '" + paymentMode + '\'' + 
			",transferDate = '" + transferDate + '\'' + 
			",transferId = '" + transferId + '\'' + 
			",lastUpdated = '" + lastUpdated + '\'' + 
			",bankId = '" + bankId + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",currency = '" + currency + '\'' + 
			",bankname = '" + bankname + '\'' + 
			",id = '" + id + '\'' + 
			",state = '" + state + '\'' + 
			",amountTransferred = '" + amountTransferred + '\'' + 
			",customer_Id = '" + customerId + '\'' + 
			",responseMsg = '" + responseMsg + '\'' + 
			"}";
		}
}