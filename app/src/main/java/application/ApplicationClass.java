package application;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ApplicationClass extends Application {

    // i need application class...
    private static ApplicationClass mContext;

    //  Database Name...
    private static final String DB_NAME = "Cheruvu.realm";

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        //  configure Realm..
        configureRealm();

        Fabric.with(this, new Crashlytics());
    }

    private void configureRealm() {

        //  initializing it...
        Realm.init( this );

        RealmInspectorModulesProvider realmInspectorModulesProvider = RealmInspectorModulesProvider.builder( this )
                .withDeleteIfMigrationNeeded( true ).build();

        //  Initialize stetho...
        Stetho.initialize(
                Stetho.newInitializerBuilder( this )
                        .enableDumpapp( Stetho.defaultDumperPluginsProvider( this ) )
                        .enableWebKitInspector( realmInspectorModulesProvider )
                        .build() );

        RealmConfiguration config = new RealmConfiguration.Builder()
                .name( DB_NAME )
                .schemaVersion( 1 )
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration( config );
    }

    public synchronized static ApplicationClass getContext() {
        return mContext;
    }

    //  to add multidex support...
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext( base );
        MultiDex.install( this );
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Realm realm = Realm.getDefaultInstance();
        if (!realm.isClosed()) {
            Realm.getDefaultInstance().close();
        }
    }
}
