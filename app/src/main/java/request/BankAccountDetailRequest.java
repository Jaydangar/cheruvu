package request;

import java.util.ArrayList;
import java.util.List;

import application.ApplicationClass;
import in.cheruvu.cheruvu.R;

public class BankAccountDetailRequest {

	private String bankAccountNumber;
	private String ifscCode;
	private String bankAccountHolderName;

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankAccountHolderName() {
		return bankAccountHolderName;
	}

	public void setBankAccountHolderName(String bankAccountHolderName) {
		this.bankAccountHolderName = bankAccountHolderName;
	}
	
	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (this.bankAccountHolderName.isEmpty()) {
			errors.add( ApplicationClass.getContext().getString( R.string.bank_account_name ));
		}
		if (this.ifscCode.isEmpty()) {
			errors.add(ApplicationClass.getContext().getString(R.string.ifsc_code));
		}
		if (this.bankAccountNumber.isEmpty()) {
			errors.add(ApplicationClass.getContext().getString(R.string.bank_account_number));
		}
		return errors;
	}
	
}
