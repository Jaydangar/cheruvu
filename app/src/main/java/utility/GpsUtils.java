package utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;

import application.ApplicationClass;

public class GpsUtils {

    private static Intent getLocationSettingIntent() {
        Intent intent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        return intent;
    }

    public static boolean isGPSOpen() {
        LocationManager lm = (LocationManager) ApplicationClass.getContext().getSystemService( Context.LOCATION_SERVICE );
        return lm.isProviderEnabled( LocationManager.GPS_PROVIDER );
    }

    public static void setGPS(Activity context, int requestCode) {
        try {
            context.startActivityForResult( getLocationSettingIntent(), requestCode );
        } catch (Exception e) {
            // TODO: handle exception
            Log.e( "", "setGPS  " + e.toString() );
        }
    }
}