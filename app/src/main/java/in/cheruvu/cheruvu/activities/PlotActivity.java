package in.cheruvu.cheruvu.activities;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.style.layers.CircleLayer;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.takusemba.spotlight.OnSpotlightStateChangedListener;
import com.takusemba.spotlight.Spotlight;
import com.takusemba.spotlight.shape.Circle;
import com.takusemba.spotlight.target.CustomTarget;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import application.ApplicationClass;
import in.cheruvu.cheruvu.R;
import in.cheruvu.cheruvu.databinding.ActivityPlotBinding;
import io.realm.Realm;
import io.realm.RealmList;
import network.NetworkResource;
import pojo.LatLng;
import request.FarmCoordinatesData;
import response.AddFarmCoordinatesResponse;
import utility.GpsUtils;
import utility.PolygonUtils;
import utility.PortraitHelper;
import utility.QueryUtils;
import utility.SharedPreferencesUtils;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

/**
 * Use the LocationComponent to easily add a device location "puck" to a Mapbox map.
 */
public class PlotActivity extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener {

    private static final String TAG = "PlotActivity";

    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private MapView mapView;

    private static final String authToken_map = ApplicationClass.getContext().getResources().getString(R.string.mapbox_token);

    private static final int GPS_REQUEST = 58880;

    //  Update Interval 1 seconds...
    private static final int LOCATION_UPDATE_INTERVAL = 1000;

    private static final String CIRCLE_SOURCE_ID = "circle-source-id";
    private static final String FILL_SOURCE_ID = "fill-source-id";
    private static final String LINE_SOURCE_ID = "line-source-id";
    private static final String CIRCLE_LAYER_ID = "circle-layer-id";
    private static final String FILL_LAYER_ID = "fill-layer-polygon-id";
    private static final String LINE_LAYER_ID = "line-layer-id";

    private List<Point> fillLayerPointList = new ArrayList<>();
    private List<Point> lineLayerPointList = new ArrayList<>();
    private List<Feature> circleLayerFeatureList = new ArrayList<>();
    private List<List<Point>> listOfList;

    private GeoJsonSource circleSource;
    private GeoJsonSource fillSource;
    private GeoJsonSource lineSource;

    private Point firstPointOfPolygon;

    //  polygonPoints...
    private List<List<Point>> polygonPoints = new ArrayList<>();
    private List<Point> OUTER_POINTS = new ArrayList<>();

    //  View for spolight
    View gps, plot, cancel, upload, showMe;

    // shared prefs utitlity file
    SharedPreferencesUtils localStorage;

    TextView farmAreaTextView;

    //  format for Double value
    private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

    private Spotlight spotlight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PortraitHelper.initialize(this);

        listOfList = new ArrayList<>();

        setupMapBox(savedInstanceState);

        localStorage = SharedPreferencesUtils.getInstance(PlotActivity.this);

        if (!localStorage.getTutorialSeenStatus("status")) {
            showSpotLight();
        }
    }

    private void setupMapBox(Bundle savedInstanceState) {
        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(this, authToken_map);

        // This contains the MapView in XML and needs to be called after the access token is configured.
        ActivityPlotBinding activityPlotBinding = DataBindingUtil.setContentView(this, R.layout.activity_plot);
        gps = activityPlotBinding.gpsBtn;
        plot = activityPlotBinding.plotBtn;
        cancel = activityPlotBinding.undoButton;
        upload = activityPlotBinding.saveBtn;
        showMe = activityPlotBinding.showMe;

        //  farm area textview
        farmAreaTextView = activityPlotBinding.textViewFarmArea;

        mapView = activityPlotBinding.mapView;
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        loadPolygon();
    }

    private void requestLocationPermission() {
        permissionsManager = new PermissionsManager(this);
        permissionsManager.requestLocationPermissions(this);
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        if (isLocationPermissionGranted()) {
            enableLocationComponent();
        } else if (!isLocationPermissionGranted()) {
            requestLocationPermission();
        }
    }

    private void loadPolygon() {
        //  check and see if you got data from fragment....
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String updatePlotRequest = bundle.getString("fragmentMsg");
            if (Objects.requireNonNull(updatePlotRequest).equals("plotDetails")) {
                try (Realm realm = Realm.getDefaultInstance()) {
                    HashMap<String, String> credentials = QueryUtils.getCredentials(realm);
                    String farmerId = credentials.get("farmerId");
                    AddFarmCoordinatesResponse addFarmCoordinatesResponse = realm.where(AddFarmCoordinatesResponse.class).equalTo(AddFarmCoordinatesResponse.Constants.FARMER_ID, farmerId).findFirst();
                    if (addFarmCoordinatesResponse != null && addFarmCoordinatesResponse.isValid()) {
                        RealmList<LatLng> path = new RealmList<>();
                        path.addAll(addFarmCoordinatesResponse.getPath());
                        for (LatLng latLng : path) {
                            OUTER_POINTS.add(Point.fromLngLat(latLng.getLongitude(), latLng.getLatitude()));
                        }
                        polygonPoints.add(OUTER_POINTS);
                        String computedArea = computeArea(OUTER_POINTS);
                        farmAreaTextView.setText(getResources().getString(R.string.farmArea) + " " + computedArea + " " + getResources().getString(R.string.areaUnit));
                    }
                }
            }
        }
    }

    private void enableLocationComponent() {

        Style.Builder builder = new Style.Builder();
        builder.fromUrl(Style.SATELLITE_STREETS);

        builder.withSource(new GeoJsonSource("source-id", Polygon.fromLngLats(polygonPoints)));
        builder.withLayer(new FillLayer("layer-id", "source-id").withProperties(
                fillColor(Color.parseColor("#32CD32")))
        );

        // Add a marker on the map's center/"target" for UI...
        ImageView hoveringMarker = new ImageView(PlotActivity.this);
        hoveringMarker.setImageResource(R.drawable.ic_add_marker);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        hoveringMarker.setLayoutParams(params);
        mapView.addView(hoveringMarker);

        mapboxMap.setStyle(builder, style -> {

            LocationEngineRequest locationEngineRequest = setupLocationEngineRequest();

            // Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

            // Activate with a built LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(LocationComponentActivationOptions.builder(PlotActivity.this, style).build());
            locationComponent.setLocationEngineRequest(locationEngineRequest);

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

            // Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);

            CameraPosition.Builder cameraPositionBuilder = new CameraPosition.Builder();
            cameraPositionBuilder.zoom(18);
            cameraPositionBuilder.tilt(30);
            mapboxMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPositionBuilder.build()), 5000);

            UiSettings uiSettings = mapboxMap.getUiSettings();
            uiSettings.setAllGesturesEnabled(true);
            uiSettings.setLogoEnabled(false);

            // Add sources to the map
            circleSource = initCircleSource(style);
            fillSource = initFillSource(style);
            lineSource = initLineSource(style);

            // Add layers to the map
            initCircleLayer(style);
            initLineLayer(style);
            initFillLayer(style);
        });
    }

    private LocationEngineRequest setupLocationEngineRequest() {
        LocationEngineRequest.Builder builder = new LocationEngineRequest.Builder(LOCATION_UPDATE_INTERVAL);
        builder.setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY);
        builder.setMaxWaitTime(LOCATION_UPDATE_INTERVAL);
        return builder.build();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GPS_REQUEST:
                enableLocationComponent();
                break;
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, getResources().getString(R.string.needed_permission), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent();
        } else {
            Toast.makeText(this, getResources().getString(R.string.permission_not_allowed), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void onGpsClick(View view) {
        if (isLocationPermissionGranted() && GpsUtils.isGPSOpen()) {
            enableLocationComponent();
        } else if (!GpsUtils.isGPSOpen()) {
            GpsUtils.setGPS(this, GPS_REQUEST);
        } else {
            requestLocationPermission();
        }
    }

    /**
     * Set up the CircleLayer source for showing map click points
     */
    private GeoJsonSource initCircleSource(@NonNull Style loadedMapStyle) {
        FeatureCollection circleFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource circleGeoJsonSource = new GeoJsonSource(CIRCLE_SOURCE_ID, circleFeatureCollection);
        loadedMapStyle.addSource(circleGeoJsonSource);
        return circleGeoJsonSource;
    }

    /**
     * Set up the CircleLayer for showing polygon click points
     */
    private void initCircleLayer(@NonNull Style loadedMapStyle) {
        CircleLayer circleLayer = new CircleLayer(CIRCLE_LAYER_ID,
                CIRCLE_SOURCE_ID);
        circleLayer.setProperties(
                circleRadius(7f),
                circleColor(Color.parseColor("#d004d3"))
        );
        loadedMapStyle.addLayer(circleLayer);
    }

    /**
     * Set up the FillLayer source for showing map click points
     */
    private GeoJsonSource initFillSource(@NonNull Style loadedMapStyle) {
        FeatureCollection fillFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource fillGeoJsonSource = new GeoJsonSource(FILL_SOURCE_ID, fillFeatureCollection);
        loadedMapStyle.addSource(fillGeoJsonSource);
        return fillGeoJsonSource;
    }

    /**
     * Set up the FillLayer for showing the set boundaries' polygons
     */
    private void initFillLayer(@NonNull Style loadedMapStyle) {
        FillLayer fillLayer = new FillLayer(FILL_LAYER_ID,
                FILL_SOURCE_ID);
        fillLayer.setProperties(
                fillOpacity(.6f),
                fillColor(Color.parseColor("#32CD32"))
        );
        loadedMapStyle.addLayerBelow(fillLayer, LINE_LAYER_ID);
    }

    /**
     * Set up the LineLayer source for showing map click points
     */
    private GeoJsonSource initLineSource(@NonNull Style loadedMapStyle) {
        FeatureCollection lineFeatureCollection = FeatureCollection.fromFeatures(new Feature[]{});
        GeoJsonSource lineGeoJsonSource = new GeoJsonSource(LINE_SOURCE_ID, lineFeatureCollection);
        loadedMapStyle.addSource(lineGeoJsonSource);
        return lineGeoJsonSource;
    }

    /**
     * Set up the LineLayer for showing the set boundaries' polygons
     */
    private void initLineLayer(@NonNull Style loadedMapStyle) {
        LineLayer lineLayer = new LineLayer(LINE_LAYER_ID,
                LINE_SOURCE_ID);
        lineLayer.setProperties(
                lineColor(Color.WHITE),
                lineWidth(5f)
        );
        loadedMapStyle.addLayerBelow(lineLayer, CIRCLE_LAYER_ID);
    }

    /**
     * Remove the drawn area from the map by resetting the FeatureCollections used by the layers' sources
     */
    private void clearEntireMap() {
        fillLayerPointList = new ArrayList<>();
        circleLayerFeatureList = new ArrayList<>();
        lineLayerPointList = new ArrayList<>();
        if (circleSource != null) {
            circleSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }
        if (lineSource != null) {
            lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }
        if (fillSource != null) {
            fillSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{}));
        }
        //  clear the list
        listOfList.clear();
    }

    @SuppressLint("SetTextI18n")
    public void onPlot(View view) {

        // Use the map click location to create a Point object
        Point mapTargetPoint = Point.fromLngLat(mapboxMap.getCameraPosition().target.getLongitude(),
                mapboxMap.getCameraPosition().target.getLatitude());

        // Make note of the first map click location so that it can be used to create a closed polygon later on
        if (circleLayerFeatureList.size() == 0) {
            firstPointOfPolygon = mapTargetPoint;
        }

        // Add the click point to the circle layer and update the display of the circle layer data
        circleLayerFeatureList.add(Feature.fromGeometry(mapTargetPoint));
        if (circleSource != null) {
            circleSource.setGeoJson(FeatureCollection.fromFeatures(circleLayerFeatureList));
        }

        // Add the click point to the line layer and update the display of the line layer data
        if (circleLayerFeatureList.size() < 3) {
            lineLayerPointList.add(mapTargetPoint);
        } else if (circleLayerFeatureList.size() == 3) {
            lineLayerPointList.add(mapTargetPoint);
            lineLayerPointList.add(firstPointOfPolygon);
        } else if (circleLayerFeatureList.size() >= 4) {
            lineLayerPointList.remove(circleLayerFeatureList.size() - 1);
            lineLayerPointList.add(mapTargetPoint);
            lineLayerPointList.add(firstPointOfPolygon);
        }
        if (lineSource != null) {
            lineSource.setGeoJson(FeatureCollection.fromFeatures(new Feature[]
                    {Feature.fromGeometry(LineString.fromLngLats(lineLayerPointList))}));
        }

        // Add the click point to the fill layer and update the display of the fill layer data
        if (circleLayerFeatureList.size() < 3) {
            fillLayerPointList.add(mapTargetPoint);
        } else if (circleLayerFeatureList.size() == 3) {
            fillLayerPointList.add(mapTargetPoint);
            fillLayerPointList.add(firstPointOfPolygon);
        } else if (circleLayerFeatureList.size() >= 4) {
            fillLayerPointList.remove(fillLayerPointList.size() - 1);
            fillLayerPointList.add(mapTargetPoint);
            fillLayerPointList.add(firstPointOfPolygon);
        }
        listOfList = new ArrayList<>();
        listOfList.add(fillLayerPointList);
        List<Feature> finalFeatureList = new ArrayList<>();
        finalFeatureList.add(Feature.fromGeometry(Polygon.fromLngLats(listOfList)));
        FeatureCollection newFeatureCollection = FeatureCollection.fromFeatures(finalFeatureList);
        if (fillSource != null) {
            fillSource.setGeoJson(newFeatureCollection);
        }
        String computedArea = computeArea(fillLayerPointList);
        farmAreaTextView.setText(getResources().getString(R.string.your_farm_area_is) + " " + computedArea + getResources().getString(R.string.areaUnit));
    }

    //  calculates area
    private String computeArea(List<Point> fillLayerPointList) {
        List<LatLng> latLngs = new ArrayList<>();
        for (Point point : fillLayerPointList) {
            latLngs.add(new LatLng(point.latitude(), point.longitude()));
        }
        return decimalFormat.format(PolygonUtils.computeArea(latLngs) / 4046.856);
    }

    public void onUndoClick(View view) {
        clearEntireMap();
    }

    public void onUploadClick(View view) {
        if (!listOfList.isEmpty()) {

            List<LatLng> latLngList = new ArrayList<>();
            for (List<Point> pointList : listOfList) {
                for (Point point : pointList) {
                    latLngList.add(new LatLng(point.latitude(), point.longitude()));
                }
            }

            try (Realm realm = Realm.getDefaultInstance()) {
                HashMap<String, String> map = QueryUtils.getCredentials(realm);
                String farmerId = map.get("farmerId");
                String referenceId = map.get("farmerReferenceId");
                String authToken = map.get("authToken");
                FarmCoordinatesData farmCoordinatesData = new FarmCoordinatesData();
                farmCoordinatesData.setFarmerId(farmerId);
                farmCoordinatesData.setFarmerReferenceId(referenceId);
                farmCoordinatesData.setPath(latLngList);

                NetworkResource.getInstance().addFarmCoordinatesData(authToken, farmerId, farmCoordinatesData, this);
            }
        }
    }

    public void showSpotLight() {

        // Layout inflater to cast target
        LayoutInflater inflater = LayoutInflater.from(PlotActivity.this);

        // show spotlight here
        //gps target layout
        View gpsLayout = inflater.inflate(R.layout.target_gps_layout, null);
        final CustomTarget gpsTarget =
                new CustomTarget.Builder(PlotActivity.this).setPoint(findViewById(R.id.gpsBtn))
                        .setShape(new Circle(100f))
                        .setOverlay(gpsLayout)
                        .build();

        // plot target layout
        View plotLayout = inflater.inflate(R.layout.target_plot_layout, null);
        final CustomTarget plotTarget =
                new CustomTarget.Builder(PlotActivity.this).setPoint(findViewById(R.id.plotBtn))
                        .setShape(new Circle(100f))
                        .setOverlay(plotLayout)
                        .build();

        // cancel target layout
        View cancelLayout = inflater.inflate(R.layout.target_cancel_layout, null);
        final CustomTarget cancelTarget =
                new CustomTarget.Builder(PlotActivity.this).setPoint(findViewById(R.id.undoButton))
                        .setShape(new Circle(100f))
                        .setOverlay(cancelLayout)
                        .build();

        // plot target layout
        View uploadLayout = inflater.inflate(R.layout.target_upload_layout, null);
        final CustomTarget uploadTarget =
                new CustomTarget.Builder(PlotActivity.this).setPoint(findViewById(R.id.saveBtn))
                        .setShape(new Circle(100f))
                        .setOverlay(uploadLayout)
                        .build();


        spotlight = Spotlight.with(PlotActivity.this)
                .setOverlayColor(R.color.tutorial_backgrund)
                .setDuration(10L)
                .setAnimation(new DecelerateInterpolator(2f))
                .setTargets(gpsTarget, plotTarget, cancelTarget, uploadTarget)
                .setClosedOnTouchedOutside(true)
                .setOnSpotlightStateListener(new OnSpotlightStateChangedListener() {
                    @Override
                    public void onStarted() {

                    }

                    @Override
                    public void onEnded() {
                        localStorage.setTutorialSeenStatus("status", true);
                    }
                });

        spotlight.start();

    }

    private boolean isLocationPermissionGranted() {
        boolean isGranted = false;
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            isGranted = true;
        }
        return isGranted;
    }

    public void showMe(View view) {
        showSpotLight();
    }
}